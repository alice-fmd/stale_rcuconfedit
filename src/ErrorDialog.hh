//
// 
//
#ifndef RCUCONFEDIT_ErrorDialog
#define RCUCONFEDIT_ErrorDialog
#include <gtkmm/dialog.h>

// Forward decsl
namespace Gtk
{
  class Label;
  class Label;
  class HBox;
  class VBox;
  class Image;
  class Button;
}
namespace RcuConfEdit 
{
  /** @class ErrorDialog 
      @brief A generic error dialog 
      @ingroup dialog */
  class ErrorDialog : public Gtk::Dialog
  {  
  public:
    /** Destrcutor */
    virtual ~ErrorDialog();
    /** Show the error dialog 
	@param title 
	@param description */
    static void run_it(const std::string& title, 
		       const std::string& description);
  protected:
    /** Constructor */
    ErrorDialog();
    /** Title label  */
    Gtk::Label* _title_label;
    /** Message label */
    Gtk::Label* _message_label;
    /** Horizontal box */ 
    Gtk::HBox* _hbox;
    /** Vertical box */
    Gtk::VBox* _vbox;
    /** Image */ 
    Gtk::Image* _image;
    /** OK button */
    Gtk::Button* _ok_button;
    /** Static instance */ 
    static ErrorDialog* _instance;
  };
}
#endif
