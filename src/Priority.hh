#ifndef RCUCONFEDIT_Priority
#define RCUCONFEDIT_Priority
#include "ScrolledView.hh"
#include "EditDialog.hh"
#include "Handler.hh"

#include <vector>
#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/textview.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/paned.h>

// Forward declarations 
namespace Gtk 
{
  class Frame;
}

namespace RcuConf
{
  class Priority;
  class Parameter;
}


namespace RcuConfEdit 
{
  // Forward decl 
  class PriorityView;
  class PriorityHandler;
  class ParameterHandler;
  class ParameterProxy;
  
  // Convinience typedef 
  typedef std::vector<ParameterProxy*> ParameterList;

  /** @defgroup priority_stuff Priority stuff */
  //__________________________________________________________________
  /** @class PriorityProxy
      @brief Proxy for priorities
      @ingroup proxies
      @ingroup priority_stuff */ 
  class PriorityProxy 
  {
  public:
    /** Cosntructor from user data 
	@param id 
	@param desc 
	@param pp */
    PriorityProxy(int id, std::string& desc, ParameterList& pp)
      : _id(id), _description(desc), _params(pp.begin(),pp.end()), _obj(0)
    {}
    /** Constructor from DB data 
	@param o DB object 
	@param pp */
    PriorityProxy(RcuConf::Priority* o, ParameterList& pp) 
      : _obj(o), _params(pp.begin(),pp.end())
    {}
    /** Destructor */
    virtual ~PriorityProxy();
    /** @return identifier */
    int id() const;
    /** @return identifier */
    int my_id() const { return _id; }
    /** @return description */
    const std::string& description() const;
    /** @return Parameter list */
    const ParameterList& params() const { return _params; }
    /** @param id Identifer */
    void set_id(int id) { _id = id; }
    /** @return DB object (possibly made from user data) */
    RcuConf::Priority* obj();
  protected:
    /** (user) identifier */
    int _id;
    /** (user) description */
    std::string _description;
    /** Parameters */
    ParameterList _params;
    /** DB object */
    RcuConf::Priority* _obj;
  };

  //__________________________________________________________________
  /** List of priority proxies 
      @ingroup proxies */
  typedef std::vector<PriorityProxy*> PriorityList;
  
  //__________________________________________________________________
  /** @class PriorityColumns PriorityView.h 
      @brief Structure that defines the columns in a priority view
      @ingroup priority_stuff 
      @ingroup view  */ 
  struct PriorityColumns : public ViewColumns
  {
    /** Constructor */
    PriorityColumns() { add(_name); add(_obj); add(_sub_obj); }
    /** Name column */
    Gtk::TreeModelColumn<Glib::ustring> _name;
    /** Pointer to object */
    Gtk::TreeModelColumn<PriorityProxy*> _obj;
    /** Pointer to sub-object */
    Gtk::TreeModelColumn<ParameterProxy*> _sub_obj;
  };

  //__________________________________________________________________
  /** @class PriorityView 
      @brief View of priority available 
      @ingroup priority_stuff 
      @ingroup view */
  class PriorityView : public ScrolledTreeView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    PriorityView();
    /** Called when the selection is changed */
    virtual void on_selection();
    /** Member function to add an entry to the view 
	@param p  The parameter references 
	@param pp Array of parameters */
    void add_entry(PriorityProxy* p);
    /** Get the DB object and parameters corresponding to the current
	object. 
	@param p  On return, the selected object, or null if no row
	was selected. 
	@param pp List of pararameter corresponding to @a p */
    void get_entry(PriorityProxy*& p) const;
    /** Find an entry that has ID field @a id, and return a pointer to
	the DB object in @a p 
	@param id Identifier to look for 
	@param  p On exit a pointer to the DB object, or null if the
	identifier wasn't found in the list. */
    void find_entry(int id, PriorityProxy*& p) const;
    /** Copy the entries from @a other into this view.  This is a
	shallow copy. 
	@param other View to topy from */
    void copy(const PriorityView& other);
    /** Get all entries from the table. 
	@param l On return, contains the entries */
    void get_all(std::vector<PriorityProxy*>& l) const;
    /** Get all new entries from the table. 
	@param l On return, contains the new entries */
    void get_new(std::vector<PriorityProxy*>& l) const;
    /** Get the name */
    const char* name() const { return "Priority"; }
  protected:
    /** Delete user data associated with a row 
	@param row Row to delete user data for */
    void delete_entry(Gtk::TreeModel::Row& row);
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    PriorityColumns _columns;
  };

  //__________________________________________________________________
  /** @class ParameterEntries
      @brief Structure that defines the columns in a parameter view
      @ingroup priority_stuff 
      @ingroup editors  */ 
  struct ParameterEntries : public ViewColumns
  {
  public:
    /** Constructor */
    ParameterEntries()
    { 
      add(_name); 
      add(_obj);
    }
    /** Name column */
    Gtk::TreeModelColumn<Glib::ustring>	_name;
    /** Reference to object */ 
    Gtk::TreeModelColumn<ParameterProxy*> _obj;
  };

  // Forward declarator 
  class PriorityDialog;

  //__________________________________________________________________
  /** @class PriorityView 
      @brief View of priority available 
      @ingroup priority_stuff 
      @ingroup editors */
  class ParameterTable : public ScrolledListView
  {  
    /** Constructor 
	@param gmm_data Data */
    ParameterTable(PriorityDialog& p);
    /** Called when the selection is changed */
    void on_selection();
    /** Member function to add an entry to the view 
	@param p Parameter to add */
    void add_entry(ParameterProxy* p);
    /** Set the entries 
	@param pp List of parameters to add */
    void set_entries(const ParameterList& pp);
    /** Get the entries 
	@param pp on Return the parameters */
    void get_entries(ParameterList& pp) const;
    /** Remove the currently selected row */
    void remove_selected();
    /** Move the currently selected object up. */
    void move_up();
    /** Move the currently selected object down. */
    void move_down();
    /** Insert a new entry after current selection 
	@param p Parameter to insert */
    void insert_entry(ParameterProxy* p);
    /** Get the name */ 
    const char* name() const { return "Parameter"; }
  protected:
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Parent dialog is a friend */
    friend class PriorityDialog;
    /** Parent dialog */
    PriorityDialog& _parent;
    /** Columns in this view */
    ParameterEntries _columns;
  };
    

  //__________________________________________________________________
  /** @brief Priority edit dialog 
      @ingroup priority_stuff 
      @ingroup editors */
  class PriorityDialog : public EditDialog
  {  
  public:
    /** Member function to pop-up a dialog, and then remove it when
	the user hits OK or cancel 
	@param h  Handler
	@param p  Priority to edit
	@param pp Paramters
	@return @c true if the values where changed and the user
	pressed OK, @c false otherwise */
    static bool create(PriorityHandler& h, PriorityProxy*& p);
    /** Called when adding an entry */
    void on_add();
    /** Called when removing an entry */
    void on_remove();
    /** Called when an entry is moved up */
    void on_up();
    /** Called when an entry is moved down */
    void on_down();
    /** Called when an entry is selected */
    void on_select(bool ok);
    /** Destructor */
    ~PriorityDialog() {}
  private:
    /** Constructor */
    PriorityDialog(PriorityHandler& h);
    /** Handler */
    PriorityHandler& _handler;
    /** Static instance */
    static PriorityDialog* _instance;

    Gtk::VPaned _paned;
    
    /** @{ 
	@name Description stuff */
    /** View of description */
    Gtk::TextView       _description_view;
    /** Scrolled view of description */
    Gtk::ScrolledWindow _description_scroll;
    /** Frame for the description */
    Gtk::Frame*         _description_frame;
    /** @} */

    /** @{
	@name Parameter list stuff */
    /** Add button */
    Gtk::Button         _add_button;
    /** Remove button */
    Gtk::Button         _remove_button;
    /** Up button */
    Gtk::Button         _up_button;
    /** Down button */
    Gtk::Button         _down_button;
    /** Vertical box of button */
    Gtk::VButtonBox     _param_buttons;
    /** Horizontal box */
    Gtk::HBox           _param_hbox;
    /** List of parameters */
    ParameterTable       _param_list;
    /** Frame for the list */
    Gtk::Frame*         _param_frame;
    /** @} */
  };

  //__________________________________________________________________
  /** @class PriorityHandler 
      @brief Handles Priority table 
      @ingroup priority_stuff 
      @ingroup handler */
  class PriorityHandler : public Handler 
  {
  public:
    /** Constructor */
    PriorityHandler(ParameterHandler& ph) 
      : _parameter_handler(ph) 
    {}
    /** Destructor */
    ~PriorityHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit();
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update();
    /** Create an edit dialog 
	@param edit If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return @c true on edit */
    bool edit(bool edit);
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    bool pick(int& id) const;
    /** @} */

    /** @{ 
	@name Specific member functions */
    /** Pick a parameter 
	@param ret on return, the chosen ID 
	@return true if a parameter was selected */
    bool pick_parameter(ParameterProxy*& p) const;
    /** Find a priority object 
	@param id Id of the object */ 
    PriorityProxy* find(int id) const;
    /** @} */
  protected:
    /** Our view. */
    PriorityView _view;
    /** Reference to the parameter handler */
    ParameterHandler& _parameter_handler;
  };

}

#endif
