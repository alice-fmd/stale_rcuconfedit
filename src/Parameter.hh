//
//
//
#ifndef RCUCONFEDIT_Parameter
#define RCUCONFEDIT_Parameter
#include "ScrolledView.hh"
#include "EditDialog.hh"
#include "Handler.hh"
#include "Util.hh"
#include <gtkmm/label.h>
#include <gtkmm/entry.h>
#ifndef GTKMM_2_0_0
# include <gtkmm/combobox.h>
# include <gtkmm/comboboxtext.h>
#else
# include <gtkmm/combo.h>
#endif
#include <gtkmm/radiobutton.h>
#include <gtkmm/box.h>
#include <gtkmm/table.h>

// Forward declarations 
namespace Gtk 
{
  class Label;
  class Entry;
  class VBox;
  class RadioButton;
  class Table;
  class HexSpinButton;
}
namespace RcuConf
{
  class Parameter;
}

namespace RcuConfEdit 
{
  /** @defgroup parameter_stuff Parameters */
  class ParameterView;
  class ParameterHandler;

  //__________________________________________________________________
  /** Where parameter type 
      @ingroup parameter_stuff */
  enum Where { 
    /** For the RCU */
    Rcu, 
    /** For the Board controller */
    Bc, 
    /** For the ALTRO */
    Altro
  };
  
  //__________________________________________________________________
  /** @class ParameterProxy 
      @brief Proxy of parameters 
      @ingroup proxies
      @ingroup parameter_stuff */ 
  class ParameterProxy 
  {
  public:
    /** Constructor from user data 
	@param id 
	@param name 
	@param w Destination 
	@param blob Is a blob?
	@param mask Value mask */
    ParameterProxy(int id, const std::string& name, Where w, 
		   bool blob, unsigned int mask)
      : _id(id), _name(name), _where(w), _mask(mask), _blob(blob), _obj(0)
    {}
    /** DB constructor */
    ParameterProxy(RcuConf::Parameter* p);
    /** Destructor */
    virtual ~ParameterProxy();
    /** @return Identifier */
    int id() const;
    /** @return Identifier */
    int my_id() const { return _id; }
    /** @return name */
    const std::string& name() const;
    /** @return destination  */
    Where where() const;
    /** @return Is a blob */
    bool blob() const;
    /** @return value mask */
    unsigned int mask() const;
    /** @return (newly created from user data) DB object */
    RcuConf::Parameter*  obj();
    /** @param new user id */
    void set_id(int id) { _id = id; }
  protected:
    /** (user) Identifier */
    int _id;
    /** (user) name */
    std::string _name;
    /** (user) destination  */
    Where _where;
    /** @return Is a blob */
    bool _blob;
    /** Value mask */
    unsigned int _mask;
    /** DB object */
    RcuConf::Parameter* _obj;
  };

  //__________________________________________________________________
  /** Type def of a list of parameter proxies 
      @ingroup proxies */
  typedef std::vector<ParameterProxy*> ParameterList;

  //__________________________________________________________________
  /** @class ParameterColumns 
      @brief Structure that defines the columns in a parameter view
      @ingroup parameter_stuff 
      @ingroup view */
  struct ParameterColumns : public ViewColumns
  {
  public:
    /** Constructor */
    ParameterColumns()
    { 
      add(_name); 
      add(_where); 
      add(_blob); 
      add(_mask); 
      add(_obj);
    }
    /** Name column */
    Gtk::TreeModelColumn<Glib::ustring>		_name;
    /** Destination column */
    Gtk::TreeModelColumn<Glib::ustring>		_where;
    /** BLOB flag column */
    Gtk::TreeModelColumn<bool>			_blob;
    /** Mask column */
    Gtk::TreeModelColumn<unsigned int>		_mask;
    /** Pointer to object (hidden column) */
    Gtk::TreeModelColumn<ParameterProxy*>	_obj;
  };

  //__________________________________________________________________
  /** @class ParameterView 
      @brief View of parameter available 
      @ingroup parameter_stuff 
      @ingroup view */
  class ParameterView : public ScrolledListView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    ParameterView();
    /** Called when the selection is changed */
    void on_selection();
    /** Member function to add an entry to the view 
	@param p Configuration DB object to add */
    void add_entry(ParameterProxy* p);
    /** Get the DB object and priority corresponding to the current
	object. 
	@param p  On return, the selected object, or null if no row
	was selected. 
	@param p The priority corresponding to @a p */
    void get_entry(ParameterProxy*& p) const;
    /** Find an entry that has ID field @a id, and return a pointer to
	the DB object in @a p 
	@param id Identifier to look for 
	@param  p On exit a pointer to the DB object, or null if the
	identifier wasn't found in the list. */
    void find_entry(int id, ParameterProxy*& p) const;
    /** Copy the entries from @a other into this view.  This is a
	shallow copy. 
	@param other View to topy from */
    void copy(const ParameterView& other);
    /** Get all entries from the table. 
	@param l On return, contains the entries */
    void get_all(ParameterList& l) const;
    /** Get all new entries from the table. 
	@param l On return, contains the new entries */
    void get_new(ParameterList& l) const;
    /** Get the name */ 
    const char* name() const { return "Parameter"; }
  protected:
    /** Delete user data associated with a row 
	@param row Row to delete user data for */
    void delete_entry(Gtk::TreeModel::Row& row);
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    ParameterColumns _columns;
  };

  //__________________________________________________________________
  /** @brief Parameter edit dialog 
      @ingroup parameter_stuff 
      @ingroup editors */
  class ParameterDialog : public EditDialog
  {  
  public:
    /** Member function to pop-up a dialog, and then remove it when
	the user hits OK or cancel 
	@param h  Handler
	@param p  Object to edit
	@return @c true if the values where changed and the user
	pressed OK, @c false otherwise */
    static bool create(ParameterHandler& h, ParameterProxy*& p);
    /** Set the destination 
	@param w Where */
    void set_destination(Where w);
    /** Get the destination 
	@param where */
    Where get_destination() const;
    /** Validate the input */
    bool validate();
    /** Destructor */
    virtual ~ParameterDialog() {}
  private:
    /** Constructor */
    ParameterDialog(ParameterHandler& h);
    /** Static instance */
    static ParameterDialog* _instance;
    /** Parameter handler */
    ParameterHandler& _handler;
    
    /** @{ 
	@name */
    Gtk::Label      		_name_label;
    Gtk::Entry	     		_name_entry;
    Gtk::Label      		_where_label;
    // #ifndef GTKMM_2_0_0
    Gtk::ComboBoxText   	_where_combo;
    // #else
    // Gtk::Combo   		_where_combo;
    // #endif
    Gtk::Label      		_type_label;
    Gtk::VBox			_type_vbox;
    Gtk::RadioButton::Group 	_type_group;
    Gtk::RadioButton		_single_button;
    Gtk::RadioButton		_array_button;
    Gtk::Label			_mask_label;
    Gtk::HexSpinButton		_mask_value;
    Gtk::Table                  _table;
    /** @} */
  };

  //__________________________________________________________________
  /** @class ParameterHandler 
      @brief Handles Parameter table 
      @ingroup parameter_stuff 
      @ingroup handler */
  class ParameterHandler : public Handler 
  {
  public:
    /** Constructor */
    ParameterHandler() {}
    /** Destructor */
    ~ParameterHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit();
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update();
    /** Create an edit dialog 
	@param copy If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return @c true on edit */
    bool edit(bool copy);
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    bool pick(int& id) const;
    /** @} */

    /** @{ 
	@name Specific member functions */
    /** Find a parameter object 
	@param id Id of the object */ 
    virtual ParameterProxy* find(int id) const;
    /** Turn a where into a string */ 
    static std::string where_to_string(Where where);
    /** Turn a string into a where */
    static Where string_to_where(const std::string& s);
    /** @} */
  protected:
    /** Our view */
    ParameterView _view;
  };



}
#endif
//
// EOF
//
