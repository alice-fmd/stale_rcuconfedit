#ifndef RCUCONFEDIT_Widgets
#define RCUCONFEDIT_Widgets
#include "Util.hh"
#include <gtkmm/label.h>
#include <gtkmm/table.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/frame.h>
#include <gtkmm/alignment.h>

namespace RcuConfEdit
{
  // Forward decl
  class ParameterProxy;
  
  /** @defgroup widgets Widgets for registers, commands and memories
   */
 
  //__________________________________________________________________
  /** @class ValueWidget 
      @brief This defines a pure interface that a parameter specific
      widget needs to implement 
      @ingroup values_stuff 
      @ingroup widgets
      @ingroup editors */
  class ValueWidget : public Gtk::VBox 
  {
  public: 
    /** Constructor */
    ValueWidget() : Gtk::VBox() {}
    /** Destructor */ 
    virtual ~ValueWidget() {}
    /** Set the widgets single value.  Derived code should over load
	this member function to do more compliated stuff. 
	@param v Value to set. */
    virtual void set_single_value(unsigned int v) {}
    /** Set the widgets multiple value. Derived code should over load
	this member function to do more compliated stuff. 
	@param v Values to set. */
    virtual void set_multiple_value(const std::vector<unsigned int>& v) {}
    /** Get the widgets single value 
	@return the widgets single value */ 
    virtual unsigned int get_single_value() const { return 0; }
    /** Get the widgets multiple value 
	@param v On return, the values of the widget */ 
    virtual void get_multiple_value(std::vector<unsigned int>& v) const {}
    /** Clear the widget */ 
    virtual void clear() {}
    /** Whether this is a single value widget or multiple value.
	Default is to return @c true (single value). */
    virtual bool is_single() const { return true; }
    /** Get the parameter name that this widget corresponds to. 
	@return The name (a C string) of the parameter this widget 
	corresponds to */
    virtual const char* name() const { return ""; }
    /** Validate the value of the widget. 
	@return @c true if the data is valid, false otherwise */
    virtual bool validate() const { return true; }
    /** Set the mask used by the parameter 
	@param mask */ 
    virtual void set_mask(unsigned int mask) {}
  };

  //__________________________________________________________________
  /** @class SingleValueWidget 
      @brief This is a generic single value widget. This is a
      singleton so that we may reuse the object over and over again. 
      @ingroup values_stuff 
      @ingroup widgets
      @ingroup editors */
  class SingleValueWidget : public ValueWidget
  {
  public:
    /** Destructor */
    virtual ~SingleValueWidget() {}
    /** Set the widgets single value.  Derived code should over load
	this member function to do more compliated stuff. 
	@param v Value to set. */
    virtual void set_single_value(unsigned int v) { _value.set_value(v); }
    /** Get the widgets single value 
	@return the widgets single value */ 
    virtual unsigned int get_single_value() const { return _value.get_value();}
    /** Clear the widget */ 
    virtual void clear() { _value.set_value(0x0); }
    /** Validate the value of the widget. 
	@return @c true if the data is valid, false otherwise */
    virtual bool validate() const { return (get_single_value() & ~_mask)==0; }
    /** Set the mask used by the parameter 
	@param mask */ 
    virtual void set_mask(unsigned int mask) { _mask = mask; }
    /** Get the static instance */ 
    static SingleValueWidget* instance();
  protected:
    /** Constructor */
    SingleValueWidget();
    /** Instance member variable */ 
    static SingleValueWidget* _instance;
    /** Mask value */
    unsigned int _mask;
    
    /** @{ 
	@name Value widgets */
    /** Value field */
    Gtk::HexSpinButton _value;
    /** Alignment */
    Gtk::Alignment _alignment;
    /** Value frame */
    Gtk::Frame _frame;
    /** @} */
  };
  //__________________________________________________________________
  /** @class MultipleValueWidget 
      @brief This is a generic multi-valued widget. This is a
      singleton so that we may reuse the object over and over again. 
      @ingroup values_stuff 
      @ingroup widgets
      @ingroup editors */
  class MultiValueWidget : public ValueWidget
  {
  public:
    /** Destructor */
    virtual ~MultiValueWidget() {}
    /** Set the widgets multiple value. Derived code should over load
	this member function to do more compliated stuff. 
	@param v Values to set. */
    virtual void set_multiple_value(const std::vector<unsigned int>& v);
    /** Get the widgets multiple value 
	@param v On return, the values of the widget */ 
    virtual void get_multiple_value(std::vector<unsigned int>& v) const;
    /** Validate the value of the widget. 
	@return @c true if the data is valid, false otherwise */
    virtual bool validate() const;
    /** Clear the widget */ 
    virtual void clear() { _view.clear(); }
    /** Whether this is a single value widget or multiple value.
	Default is to return @c true (single value). */
    virtual bool is_single() const { return false; }
    /** Set the mask used by the parameter 
	@param mask */ 
    virtual void set_mask(unsigned int mask) { _mask = mask; }
    /** Called on add */
    virtual void on_add() { _view.insert_data_after_selected(0x0); }
    /** Get the static instance */ 
    static MultiValueWidget* instance();
  protected:
    /** Constructor */
    MultiValueWidget();
    /** Instance member variable */ 
    static MultiValueWidget* _instance;
    /** Mask value */
    unsigned int _mask;
    /** @{ 
	@name Value widgets */
    /** Horizontal container */
    Gtk::HBox        _hbox;
    /** Hex view */
    Gtk::HexView     _view;
    /** Button box */ 
    Gtk::VButtonBox  _buttons;
    /** Add button */
    Gtk::Button      _add;     
    /** Remove button */
    Gtk::Button      _remove;  
    /** Up button */
    Gtk::Button      _up;      
    /** Down button */
    Gtk::Button      _down;   
    /** Alignment */
    Gtk::Alignment _alignment;
    /** Value frame */
    Gtk::Frame      _frame;
    /** @} */
  };
  //__________________________________________________________________
  /** @class CommandValueWidget 
      @brief This is a generic command value widget. This is a
      singleton so that we may reuse the object over and over again.
      The widget is empty, since commands take no arguments. 
      @ingroup values_stuff 
      @ingroup widgets
      @ingroup editors */
  class CommandValueWidget : public ValueWidget
  {
  public:
    /** Destructor */
    virtual ~CommandValueWidget() {}
    /** Get the static instance */ 
    static CommandValueWidget* instance();
  protected:
    /** Constructor */
    CommandValueWidget() {}
    /** Instance member variable */ 
    static CommandValueWidget* _instance;
  };

  //__________________________________________________________________
  /** @class WidgetFactory 
      @brief This is a factory of widgets. 
      It creates widgets according to the parameter proxy passed into
      the create member function.  It provides a fall back for the
      most common cases. 
      @ingroup widgets 
      @ingroup editors */
  class WidgetFactory 
  {
  public:
    /** Constructor */
    WidgetFactory();
    /** Destructor */
    virtual ~WidgetFactory();

    /** Regsiter a widget with the factory. 
	@param w Widget object to register */ 
    void register_widget(ValueWidget* w);
    
    /** Get a value widget from the factory that corresponds to the
	parameter of the proxy passed in.  This may fall back to one
	of SingleValueWidget, MultiValueWidget, or CommandValueWidget
	in case no widget was registered for this parameter. 
	@param p Parameter proxy */
    ValueWidget* create(ParameterProxy* p);
    ValueWidget* create(ParameterProxy* p, ValueWidget*& w);
  protected:
    /** Type of map of widgets */
    typedef std::map<std::string,ValueWidget*> WidgetMap;
    /** Map of value widgets */
    WidgetMap _map;
  };
  //__________________________________________________________________
  struct to_upper
  {
    char operator()(const char& c) { return ::toupper(c); } 
  };
}
#endif
//
// EOF
//
