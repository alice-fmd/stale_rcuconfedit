#ifndef RCUCONFEDIT_Util
#define RCUCONFEDIT_Util
#include <gtkmm/box.h>
#include <gtkmm/entry.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/treeview.h>
#include <gtkmm/liststore.h>
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/cellrenderertext.h>
#ifdef GTKMM_2_0_0
#include <gtkmm/combo.h>
#endif
namespace Gtk
{
  // Forward decls 
  class Adjustment;

  /** @defgroup utils Some additional widgets not in Gtk-- (sigh!) */

#ifdef GTKMM_2_0_0
  /** A combosition text box */
  class ComboBoxText : public Combo
  {
  public:
    ComboBoxText();
    virtual ~ComboBoxText() {}
    void append_text(const Glib::ustring& s);
    void set_active(int i);
    void set_active_text(const Glib::ustring& t);
    int  get_active_row_number() const;
    const Glib::ustring&  get_active_text() const;
    Glib::SignalProxy0<void> signal_changed() { return get_entry()->signal_changed(); }
  protected:
    std::vector<Glib::ustring> _text;
    void on_show();
  };
#endif    

  //__________________________________________________________________
  /** @class HexEntry 
      @brief a text entry to edit hexadecimal strings 
      @ingroup utils */
  class HexEntry : public Entry 
  {
  public:
    /** Constructor */
    HexEntry(unsigned int lower=0x0, unsigned int upper=0xFFFFFFFF);
    /** Destructor */
    virtual ~HexEntry() {}
    /** Get the data */
    unsigned int get_value() const { return _data; }
    /** Set the value */
    void set_value(unsigned int);
    /** Hide set text */ 
    virtual void set_text(const Glib::ustring& s) { update_text(s); }
  protected:
    /** Called when the value is changed */
    virtual void on_changed();
    /** Called when the data is set */
    virtual void on_activate() { set_value(_lower); }
    /** Hide set text */ 
    virtual void update_text(const Glib::ustring& s);
    /** The data */
    unsigned int _data;
    /** Lower bound */
    unsigned int _lower;
    /** Upper bound */
    unsigned int _upper;
    /** Width in characters */
    unsigned int _w;
  };

  //__________________________________________________________________
  /** @class HexSpinButton
      @brief a spin button that only has hexadecimal numbers 
      @ingroup utils */
  class HexSpinButton : public SpinButton 
  {
  public:
    /** Constructor 
	@param climb_rate Climb rate */
    HexSpinButton(int climb_rate=0);
    /** Constructor 
	@param adj Adjustment. 
	@param climb_rate Climb rate 
	@note The range specified by @a adj may be modified by this
	member function, in case it is outside the range of an @c
	unsigned @c int (0 to @c UINT_MAX) */
    HexSpinButton(Adjustment& adj, int climb_rate=0);
    /** Destructor */
    virtual ~HexSpinButton() {}
    /** "overload" the set_adjustment member function 
	@param a New adjustment. 
	@note The range specified by @a a may be modified by this
	member function, in case it is outside the range of an @c
	unsigned @c int (0 to @c UINT_MAX) */
    virtual void set_adjustment(Adjustment& a);
    /** "overload" the set_range member function 
	@param lower Lower limit of the range 
	@param uppper Upper limit of the range */
    virtual void set_range(unsigned int lower, unsigned int upper);
    /** "overload" the set_increment member function 
	@param step Change of value in a single @e step
	increment/decrement. 
	@param page Change of value in a single @e page
	increment/decrement. */
    virtual void set_increments(unsigned int step, unsigned int page);
    /** "overload" the get value member function 
	@param v The new value */
    virtual void set_value(unsigned int v);
    /** "overload" the get value member function 
	@return The current value */
    virtual unsigned int get_value() const;
    /** Get the range 
	@param lower Lower bound 
	@param upper Upper bound */
    virtual void get_range(unsigned int& lower, unsigned int& upper) const;
  protected:
    /** @{ 
	@name Hide the SpinButton member functions we don't want */
    void set_numeric(bool) {}
    void set_digits(guint) {}
    // double get_value() const { return 0; }
    int get_value_as_int() const { return 0; }
    /** @} */
    /** Get the upper bound */
    virtual unsigned int get_upper() const;
    /** Update the maximum number of characters allowed */
    virtual void update_max_length();
    /** Called when the widget needs to turn the text of the field
	into a number for the adjustment. 
	@param value Return value to the adjustment. 
	@param int always 1 */
    virtual int on_input(double* value); 
    /** Called when the widget needs to render the adjustments value
	into text in the field 
	@return always @c true */
    virtual bool on_output();
    /** Called when the user types in text.  Checks if the inserted
	text is valid hexadecimal character, and if not, ignores it. */
    virtual void on_changed();
  };  

  //__________________________________________________________________
  /** @class CellRendererHex 
      @brief A cell renderer that shows the text as hexadecimal
      numbers only */
  class CellRendererHex : public CellRendererText
  {
  public:
    /** Constructor 
	@param lower Lower bound 
	@param upper Upper bound 
	@param step  Single step size 
	@param page  Page step size */
    CellRendererHex(unsigned int lower=0x0, unsigned int upper=0xFFFFFFFF);
    /** Destructor */
    virtual ~CellRendererHex() {}
    /** Append a column to the passed view that uses this renderer. 
	@param tree  Tree to append to 
	@param name  Name of the column 
	@param field Field. 
	@return */
    template <typename T>
    bool append_to_view(Gtk::TreeView& tree, const Glib::ustring& name,
			const TreeModelColumn<T>& field);
  protected:
    /** Append a column to the passed view that uses this renderer. 
	@param tree  Tree to append to 
	@param name  Name of the column 
	@param field Field. 
	@return */
    bool do_append(Gtk::TreeView& tree, const Glib::ustring& name,
		   const TreeModelColumnBase& field);
    /** Called when we are editing or creating a cell. 
	@param event   Event 
	@param widget  Our widget 
	@param path    Path in tree 
	@param bg_area Background area 
	@param area    Cell area
	@param flags   Flags 
	@return the edited cell */
    CellEditable* start_editing_vfunc(GdkEvent*             event, 
				      Widget&               widget, 
				      const Glib::ustring&  path, 
				      const Gdk::Rectangle& bg_area, 
				      const Gdk::Rectangle& cell, 
				      CellRendererState     flags);
    /** Called when we are done editing a cell. 
	@param entry The entry 
	@param path Path to the entry */
    void on_edited(Entry* entry, Glib::ustring path);
    /** Called when we render a row 
	@param r Pointer to this 
	@param i iterator */
    void on_render(CellRenderer* r, const TreeModel::iterator& i);
    /** Default lower bound */
    unsigned int _lower;
    /** Default upper bound */
    unsigned int _upper;
    /** Column number */
    int _n;
    /** Width in characters */
    unsigned int _w;
  };
  
  //__________________________________________________________________
  template <typename T>
  inline bool
  Gtk::CellRendererHex::append_to_view(TreeView& tree, 
				       const Glib::ustring& name,
				       const TreeModelColumn<T>& field) 
  {
    if (_n >= 0) return false;
    TreeView_Private::_connect_auto_store_editable_signal_handler(&tree, 
								  this,
								  field);
    return do_append(tree, name, field);
  }

  //__________________________________________________________________
  /** @class HexView
      @brief a table view to edit hexadecimal strings 
      @ingroup utils */
  class HexView : public ScrolledWindow 
  {
  public:
    /** Column record */
    struct HexColumns : public TreeModel::ColumnRecord 
    {
      /** Constructor */
      HexColumns() { add(_idx); add(_number); }
      /** Number column */
      Gtk::TreeModelColumn<unsigned int> _idx;
      /** Number column */
      Gtk::TreeModelColumn<unsigned int> _number;
    };
    /** Constructor */
    HexView();
    /** Destrcutor */ 
    virtual ~HexView() {}

    /** Append a value
	@param value Value to append */
    virtual void append_data(const unsigned int& value);
    /** Prepend a value
	@param value Value to append */
    virtual void prepend_data(const unsigned int& value);
    /** Inserts a value before selection, or at the bottom if there's
	no selection
	@param value Value to append */
    virtual void insert_data_before_selected(const unsigned int& value);
    /** Inserts a value after selection, at at the top if there's no
	selection 
	@param value Value to append */
    virtual void insert_data_after_selected(const unsigned int& value);
    /** Remove the selected entry */
    virtual void remove_selected();
    /** Move the selected item up */
    virtual void move_selected_up();
    /** Move the selected item down */
    virtual void move_selected_down();
    /** Set the data 
	@param values Array of values */
    virtual void set_data(const std::vector<unsigned int>& values);
    /** Get the data 
	@param values On return, the values in the table */ 
    virtual void get_data(std::vector<unsigned int>& values) const;
    /** Called when selection changes */
    virtual void on_selection();
    /** Called when a value is changed */
    virtual void on_change(const Glib::ustring& path, const Glib::ustring&);
    /** Clear this */ 
    virtual void clear() { _tree_model->clear(); }
  protected:
    void do_insert(Gtk::TreeModel::iterator i, const unsigned int& value);
    void render_hex(CellRenderer* r, const TreeModel::iterator& i);
    /** The tree view */
    TreeView * _view;
    /** The selection */
    Glib::RefPtr<Gtk::TreeSelection> _tree_selection;
    /** Model used */
    Glib::RefPtr<Gtk::ListStore>  _tree_model;
    /** Columns */
    HexColumns _columns;
  };

  //__________________________________________________________________
  /** @brief A labeled spin button. 
      All data members are public, since this is a convinence class.
      @ingroup utils */
  struct HLabeledSpinButton : public HBox
  {
    /** Constructor 
	@param label The label 
	@param lower The lower bound 
	@param upper The upper bound 
	@param step  Single step size 
	@param page  Page step size */
    HLabeledSpinButton(const Glib::ustring label, unsigned int lower, 
		       unsigned int upper, unsigned int step=1, 
		       unsigned int page=10);
    /** Destructor */
    virtual ~HLabeledSpinButton() {}
    /** Get the value as an @c unsigned @c int
	@return value */
    virtual unsigned int get_value() const;
    /** Set the value (as an unsigned int) 
	@param v New value */
    virtual void set_value(unsigned int v);
    /** The label */
    Label _label;
    /** The adjustment - sigh! we must have this */
    Adjustment _adjustment;
    /** The spin button */
    SpinButton _value;
  };
  //__________________________________________________________________
  /** @brief A labeled spin button. 
      All data members are public, since this is a convinence class.
      @ingroup utils */
  struct VLabeledSpinButton : public HBox
  {
    /** Constructor 
	@param label The label 
	@param lower The lower bound 
	@param upper The upper bound 
	@param step  Single step size 
	@param page  Page step size */
    VLabeledSpinButton(const Glib::ustring label, unsigned int lower, 
		       unsigned int upper, unsigned int step=1, 
		       unsigned int page=10);
    /** Destructor */
    virtual ~VLabeledSpinButton() {}
    /** Get the value as an @c unsigned @c int
	@return value */
    virtual unsigned int get_value() const;
    /** Set the value (as an unsigned int) 
	@param v New value */
    virtual void set_value(unsigned int v);
    /** The label */
    Label _label;
    /** The adjustment - sigh! we must have this */
    Adjustment _adjustment;
    /** The spin button */
    SpinButton _value;
  };
    
  //__________________________________________________________________
  /** copy a cell from one row to another 
      @param i 
      @param src 
      @param dest 
      @ingroup utils */
  template <typename T>
  inline void
  copy_cell(TreeModelColumn<T> i, const TreeModel::Row& src,
	    TreeModel::Row& dest)
  {
    T  temp = src[i];
    dest[i] = temp;
  }


}
#endif
