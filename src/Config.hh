// -*- mode: C++ -*-
#ifndef RCUCONFEDIT_Config
#define RCUCONFEDIT_Config
# include "ScrolledView.hh"
# include "EditDialog.hh"
# include "Handler.hh"

# include <gtkmm/spinbutton.h>
# include <gtkmm/adjustment.h>
# include <gtkmm/entry.h>
# include <gtkmm/box.h>
# include <gtkmm/textview.h>
# include <gtkmm/scrolledwindow.h>

// Forward declarations 
namespace Gtk 
{
  class Alignment;
  class Frame;
}

namespace RcuConf
{
  class Config;
  class Priority;
}

namespace RcuConfEdit
{
  // Forward decls 
  class ConfigView;
  class ConfigHandler;
  class PriorityHandler;
  class PriorityProxy;
  
  /** @defgroup config_stuff Configurations

      This module contains code relevant to the configurations in the
      database. 
   */
  //__________________________________________________________________
  /** @class ConfigProxy 
      @brief Proxy of a config entry 
      @ingroup proxies
      @ingroup config_stuff */
  class ConfigProxy 
  {
  public:
    /** User constructor 
	@param id 
	@param tag 
	@param x
	@param y
	@param z
	@param p Priority proxy
	@param v Version
	@param d Description */
    ConfigProxy(int id, int tag, int x, int y, int z, 
		PriorityProxy* p, int v, const std::string& d) 
      : _id(id), _tag(tag), _x(x), _y(y), _z(z), _priority(p), 
	_version(v), _description(d), _obj(0)
    {}
    /** DB constructor 
	@param o DB Object 
	@param p Priority proxy */
    ConfigProxy(RcuConf::Config* o, PriorityProxy* p) 
      : _obj(o), _priority(p)
    {}
    /** Destructor */
    virtual ~ConfigProxy();
    /** @return Identifier */
    int id() const;
    /** @return Tag */
    int tag() const;
    /** @return X-coordinate */
    int x() const;
    /** @return Y-coordinate */
    int y() const;
    /** @return Z-coordinate */
    int z() const;
    /** @return priority proxy */
    PriorityProxy* priority() const { return _priority; }
    /** @return version */
    int version() const;
    /** @return description */
    const std::string& description() const;
    /** @param (user) id */
    void set_id(int id) { _id = id; }
    /** @return (new) DB object */
    RcuConf::Config* obj();
  protected:
    /** (user) id */
    int _id;
    /** (user) tag */
    int _tag;
    /** (user) x  */
    int _x;
    /** (user) y */
    int _y;
    /** (user) z */
    int _z;
    /** Priority proxy */
    PriorityProxy* _priority;
    /** (user) version */
    int _version;
    /** (user) description */
    std::string _description;
    /** DB object */
    RcuConf::Config* _obj;
  };
  typedef std::vector<ConfigProxy*> ConfigList;
  
  //__________________________________________________________________
  /** @class ConfigColumns ConfigView.h 
      @brief Structure that defines the columns in a configuration view
      @ingroup config_stuff 
      @ingroup view
  */ 
  struct ConfigColumns : public ViewColumns
  {
  public:
    /** Constructor */
    ConfigColumns()
    { 
      add(_tag); 
      add(_x); 
      add(_y); 
      add(_z); 
      add(_priority); 
      add(_version); 
      add(_description); 
      add(_obj);
      add(_prior_obj);
    }
    /** Tag column */
    Gtk::TreeModelColumn<int> _tag;
    /** X coordinate column */
    Gtk::TreeModelColumn<int> _x;
    /** Y coordinate column */
    Gtk::TreeModelColumn<int> _y;
    /** Z coordinate column */
    Gtk::TreeModelColumn<int> _z;
    /** Priority reference column */
    Gtk::TreeModelColumn<Glib::ustring> _priority;
    /** Version number */
    Gtk::TreeModelColumn<int> _version;
    /** Description column */
    Gtk::TreeModelColumn<Glib::ustring> _description;
    /** Object pointer */ 
    Gtk::TreeModelColumn<ConfigProxy*> _obj;
    /** Object priority pointer */
    Gtk::TreeModelColumn<PriorityProxy*> _prior_obj;
    
  };

  //__________________________________________________________________
  /** @class ConfigView 
      @brief View of configurations available 
      @ingroup view
      @ingroup config_stuff */
  class ConfigView : public ScrolledListView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    ConfigView();
    /** Called when the selection is changed */
    void on_selection();
    /** Member function to add an entry to the view 
	@param c Configuration DB object to add
	@param p Priority DB object to add */
    void add_entry(ConfigProxy* c);
    /** Get the DB object and priority corresponding to the current
	object. 
	@param c  On return, the selected object, or null if no row
	was selected. 
	@param p The priority corresponding to @a p */
    void get_entry(ConfigProxy*& c) const;
    /** Find an entry that has ID field @a id, and return a pointer to
	the DB object in @a p 
	@param id Identifier to look for 
	@param  p On exit a pointer to the DB object, or null if the
	identifier wasn't found in the list. */
    void find_entry(int id, ConfigProxy*& p) const;
    /** Copy the entries from @a other into this view.  This is a
	shallow copy. 
	@param other View to topy from */
    void copy(const ConfigView& other);
    /** Get all entries from the table. 
	@param l On return, contains the entries */
    void get_all(ConfigList& l) const;
    /** Get all new entries from the table. 
	@param l On return, contains the new entries */
    void get_new(ConfigList& l) const;
    /** Get the name */ 
    const char* name() const { return "Config"; }
  protected:
    /** Delete user data associated with a row 
	@param row Row to delete user data for */
    void delete_entry(Gtk::TreeModel::Row& row);
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    ConfigColumns _columns;
  };

  //__________________________________________________________________
  /** @brief Configuration edit dialog 
      @ingroup editors
      @ingroup config_stuff */
  class ConfigDialog : public EditDialog
  {  
  public:
    /** Member function to pop-up a dialog, and then remove it when
	the user hits OK or cancel 
	@param h Reference to handler (used by pop-ups) 
	@param c DB object to edit.  On return a possible new object
	@param p Priority associated with @a c
	@return @c true if the values where changed and the user
	pressed OK, @c false otherwise */
    static bool create(ConfigHandler& h, ConfigProxy*& c);
    /** Called on browse */
    void on_browse();
    /** Get the priority id 
	@returns the priority identifier */
    int get_priority();
    /** Set the priority id 
	@param id Set the priority identifier */
    void set_priority(int id);
    /** Validate the input */
    bool validate();
    /** Destructor */
    ~ConfigDialog() {}
  private:
    /** Constructor */
    ConfigDialog(ConfigHandler& h);
    /** Static instance */
    static ConfigDialog* _instance;
    /** The configuration handler */
    ConfigHandler& _handler;

    /** @{ 
	@name Tag and priority stuff */
    /** Tag label */
    Gtk::Label      _tag_label;
    /** Adjustment for the tag */
    Gtk::Adjustment _tag_adjustment;
    /** Value of the tag */
    Gtk::SpinButton _tag_spin;
    /** Priority label */
    Gtk::Label      _priority_label;
    /** The priority view */
    Gtk::Entry      _priority_entry;
    /** Button to browse available priorities */
    Gtk::Button     _priority_browse;
    /** Horizontal boz */
    Gtk::HBox       _tagprior_hbox;
    /** Frame for the tag and priority list */
    Gtk::Frame*     _tagprior_frame;
    /** @} */

    /** @{
	@name *Coordiante stuff */
    /** X label */
    Gtk::Label      _x_label;
    /** X range */
    Gtk::Adjustment _x_adjustment;
    /** X value */
    Gtk::SpinButton _x_spin;
    /** Y label */
    Gtk::Label      _y_label;
    /** Y range */
    Gtk::Adjustment _y_adjustment;
    /** Y value */
    Gtk::SpinButton _y_spin;
    /** Z label */
    Gtk::Label      _z_label;
    /** Z range */
    Gtk::Adjustment _z_adjustment;
    /** Z value */
    Gtk::SpinButton _z_spin;
    /** Horizontal box */
    Gtk::HBox       _xyz_hbox;
    /** Frame for coordinates */
    Gtk::Frame*     _xyz_frame;
    /** @} */

    /** @{
	@name Description stuff */
    /** View of the description */
    Gtk::TextView       _description_entry;
    /** Scroll for the description */
    Gtk::ScrolledWindow _description_scroll;
    /** Frame for the description */
    Gtk::Frame*         _description_frame;
    /** @} */
  };

  //__________________________________________________________________
  /** @class ConfigHandler 
      @brief Handles Config table 
      @ingroup handler
      @ingroup config_stuff */
  class ConfigHandler : public Handler 
  {
  public:
    /** Constructor */
    ConfigHandler(PriorityHandler& handler) 
      : _priority_handler(handler)
    {}
    /** Destructor */
    ~ConfigHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit();
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update();
    /** Create an edit dialog 
	@param copy If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return @c true on edit */
    bool edit(bool copy);
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    bool pick(int& id) const;
    /** @} */

    /** @{ 
	@name Specific member functions */
    /** Find a config object 
	@param id Id of the object 
	@return Pointer to the configuration found */
    ConfigProxy* find(int id) const;    
    /** Pick a priority 
	@param ret on return, the chosen ID 
	@return true if a priority was selected */
    bool pick_priority(int& ret) const;
    /** Find a priority for us. */
    PriorityProxy* find_priority(int ret) const;
    /** @} */
  protected:
    /** Our view. */
    ConfigView _view;
    /** Reference to priority handler */
    PriorityHandler& _priority_handler;
  };
}
#endif
//
// EOF
//

