#ifndef RCUCONFEDIT_Altro_TrCfg
#define RCUCONFEDIT_Altro_TrCfg
#include "Widget.hh"
#include "Util.hh"

namespace RcuConfEdit
{
  struct TrCfg : public ValueWidget
  {
    
    TrCfg()
      : _start("Start", 0, 0x3ff),
	_end("End",0,0x3ff)
    {
      pack_start(_start, Gtk::PACK_SHRINK,5);
      pack_start(_end, Gtk::PACK_SHRINK,5);
    }
    const char* name() const { return "TRCFG"; }
    void set_single_value(unsigned int v) 
    {
      _start.set_value((v >> 10) & 0x3ff);
      _end.set_value((v >> 0) & 0x3ff);
    }
    unsigned int get_single_value() const 
    {
      return (((_start.get_value() & 0x3ff) << 10) | 
	      ((_end.get_value()   & 0x3ff) <<  0));
    }
    Gtk::HLabeledSpinButton _start;
    Gtk::HLabeledSpinButton _end;
    
  };
}
#endif
//
// EOF
//
