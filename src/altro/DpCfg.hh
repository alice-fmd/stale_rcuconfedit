#ifndef RCUCONFEDIT_Altro_DpCfg
#define RCUCONFEDIT_Altro_DpCfg
#include "Widget.hh"
#include <gtkmm/checkbutton.h>
#include <gtkmm/spinbutton.h>
#ifndef GTKMM_2_0_0
# include <gtkmm/comboboxtext.h>
#endif
#include <gtkmm/label.h>

namespace RcuConfEdit
{
  //__________________________________________________________________
  struct DpCfg : public ValueWidget
  {
    DpCfg() 
      : _1st_label("1<sup>st</sup> baseline"),
	_1st_frame(),
	_1st_hbox(),
	_1st_mode_label("Mode"),
	_1st_mode(),
	_1st_pol("1's compliment"),
	_2nd_label("2<span size='x-small' rise='5000'>nd</span> baseline"),
	_2nd_frame(),
	_2nd_vbox(),
	_2nd_enable("Enable"),
	_2nd_pre("Pre", 0, 0x3),
	_2nd_post("Post", 0, 0xf),
	_zs_frame("Zero suppression"),
	_zs_table(2,5),
	_zs_enable("Enable"),
	_zs_pre("Pre", 0, 0x3),
	_zs_post("Post", 0, 0x7),
	_zs_glitch_label("Glitch"),
	_zs_glitch()
    {
      _1st_label.set_use_markup(true);
      _1st_frame.set_label_widget(_1st_label);
      _1st_mode.append_text("din-fpd");		// 0x0
      _1st_mode.append_text("din-f(t)");	// 0x1
      _1st_mode.append_text("din-f(din)");	// 0x2
      _1st_mode.append_text("din-f(din-vpd)");	// 0x3
      _1st_mode.append_text("din-vpd-fpd");	// 0x4
      _1st_mode.append_text("din-vpd-f(t)");	// 0x5
      _1st_mode.append_text("din-vpd-f(din)");	// 0x6
      _1st_mode.append_text("din-vpd-f(din-vpd)");	// 0x7
      _1st_mode.append_text("f(din)-fpd");	// 0x8
      _1st_mode.append_text("f(din-vpd)-fpd");	// 0x9
      _1st_mode.append_text("f(t)-fpd");	// 0xA
      _1st_mode.append_text("f(t)-f(t)");	// 0xB
      _1st_mode.append_text("f(din)-f(din)");	// 0xC
      _1st_mode.append_text("f(din-vpd)-f(din-vpd)");	// 0xD
      _1st_mode.append_text("din-fpd");		// 0xE
      _1st_mode.append_text("din-fpd");		// 0xF
      _2nd_label.set_use_markup(true);
      _2nd_frame.set_label_widget(_2nd_label);
      _zs_glitch.append_text("din-fpd");	// 0x0
      _zs_glitch.append_text("din-f(t)");	// 0x1
      _zs_glitch.append_text("din-f(din)");	// 0x2
      _zs_glitch.append_text("din-f(din-vpd)");	// 0x3
      _1st_hbox.pack_start(_1st_mode_label,Gtk::PACK_SHRINK,3);
      _1st_hbox.pack_start(_1st_mode,Gtk::PACK_EXPAND_WIDGET,3);
      _1st_hbox.pack_start(_1st_pol,Gtk::PACK_SHRINK,3);
      _1st_frame.add(_1st_hbox);
      _2nd_vbox.pack_start(_2nd_enable);
      _2nd_vbox.pack_start(_2nd_pre);
      _2nd_vbox.pack_start(_2nd_post);
      _2nd_frame.add(_2nd_vbox);
      _zs_table.attach(_zs_enable,       0,2,0,1);
      _zs_table.attach(_zs_pre,          0,2,1,2);
      _zs_table.attach(_zs_post,         0,2,2,3);
      _zs_table.attach(_zs_glitch_label, 0,1,3,4);
      _zs_table.attach(_zs_glitch,       1,2,3,4);
      _zs_frame.add(_zs_table);
      pack_start(_1st_frame, Gtk::PACK_EXPAND_WIDGET,3);
      pack_start(_2nd_frame, Gtk::PACK_EXPAND_WIDGET,3);
      pack_start(_zs_frame,  Gtk::PACK_EXPAND_WIDGET,3);
      _2nd_enable.signal_toggled()
	.connect(SigC::slot(*this,&DpCfg::on_2nd));
      _zs_enable.signal_toggled().connect(SigC::slot(*this,&DpCfg::on_zs));
    }
    const char* name() const { return "DPCFG"; }
    void on_2nd() 
    {
      _2nd_pre._value.set_sensitive(_2nd_enable.get_active());
      _2nd_post._value.set_sensitive(_2nd_enable.get_active());
    }
    void on_zs() 
    {
      _zs_pre._value.set_sensitive(_zs_enable.get_active());
      _zs_post._value.set_sensitive(_zs_enable.get_active());
      _zs_glitch.set_sensitive(_zs_enable.get_active());
    }
    void set_single_value(unsigned int v)
    {
      _1st_mode.set_active(v & 0xf);
      _1st_pol.set_active(v & (1 << 4));
      _2nd_enable.set_active(v & (1 << 11));
      _2nd_pre.set_value((v >> 5) & 0x3);
      _2nd_post.set_value((v >> 5) & 0xf);
      _zs_enable.set_active(v & (1 << 19));
      _zs_pre.set_value((v >> 17) & 0x3);
      _zs_post.set_value((v >> 14) & 0xf);
      _zs_glitch.set_active((v >> 12) & 0x3);
      on_2nd();
      on_zs();
    }
    unsigned get_single_value() const
    {
      unsigned int ret = ((_1st_mode.get_active_row_number() & 0xf) | 
			  (_1st_pol.get_active() ? (1<<4) : 0));
      if (_2nd_enable.get_active()) 
	ret |= (((_2nd_pre.get_value()  & 0x3) << 5) | 
		((_2nd_post.get_value() & 0xf) << 7) | (1 << 11));
      if (_zs_enable.get_active()) 
	ret |= (((_zs_pre.get_value()  & 0x3) << 14) | 
		((_zs_post.get_value() & 0x7) << 17) | 
		((_zs_glitch.get_active_row_number() & 0x3) << 12) |
		(1 << 19));
      return ret;
    }
    Gtk::Label          	_1st_label;
    Gtk::Frame			_1st_frame;
    Gtk::HBox			_1st_hbox;
    Gtk::Label			_1st_mode_label;
    Gtk::ComboBoxText		_1st_mode;
    Gtk::CheckButton		_1st_pol;
    Gtk::Label          	_2nd_label;
    Gtk::Frame			_2nd_frame;
    Gtk::VBox			_2nd_vbox;
    Gtk::CheckButton		_2nd_enable;
    Gtk::HLabeledSpinButton	_2nd_pre;
    Gtk::HLabeledSpinButton	_2nd_post;
    Gtk::Frame			_zs_frame;
    Gtk::Table			_zs_table;
    Gtk::CheckButton		_zs_enable;
    Gtk::HLabeledSpinButton	_zs_pre;
    Gtk::HLabeledSpinButton	_zs_post;
    Gtk::Label			_zs_glitch_label;
    Gtk::ComboBoxText		_zs_glitch;
  };
}
#endif
//
// EOF
//
