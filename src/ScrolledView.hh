#ifndef RCUCONFEDIT_ScrolledView_hh
#define RCUCONFEDIT_ScrolledView_hh
#include <gtkmm/scrolledwindow.h>
#include <gtkmm/treeview.h>
#include <gtkmm/treestore.h>
#include <gtkmm/liststore.h>
#include <map>

namespace RcuConfEdit
{
  /** @defgroup view View stuff 
      The classes specify an interface for views of the data.  They
      also provide application storage of the data */

  //__________________________________________________________________
  /** @class ViewColumns 
      @brief class for view column definitions 
      @ingroup view */
  struct ViewColumns : public Gtk::TreeModel::ColumnRecord
  {
    /** Constructor - Adds the id column */ 
    ViewColumns() { add(_id); }
    /** Id column */
    Gtk::TreeModelColumn<int> _id;
  };

  //__________________________________________________________________
  /** @class ScrolledView 
      @brief base class for scrolled tree views 
      @ingroup view */
  class ScrolledView : public Gtk::ScrolledWindow
  {
  public:
    /** Constructor 
	@param data    The Glade-- data */
    ScrolledView();
    /** Destructor */
    virtual ~ScrolledView();
    /** Virtual function called when the selection is changed in the
	tree view */
    virtual void on_selection();
    /** Add a row with the specified ID 
	@param id Identifier of newly added row. 
	@param row Newly made row */
    virtual void add_row(int id, Gtk::TreeModel::Row& row) = 0;
    /** Find a row with ID column equal to @a id 
	@param id Id to search for 
	@param row On return, the row
	@return true if the row was found */
    virtual bool find_row(int id, Gtk::TreeModel::Row& row) const;
    /** Remove a selected row from the table  
	@note Only rows with id less than 0 can be removed */
    virtual bool remove_entry() {}
    /** Clear the table 
	@param also_new If true, also delete newly created rows. */
    virtual void clear(bool also_new=false);
    /** Select a row that corresponds to an id 
	@param id The identifier to look for */
    virtual bool select_row(int id);
    /** Get the name of this scrolled view */ 
    virtual const char* name() const { return ""; }
    /** Get the selection object */
    Glib::RefPtr<Gtk::TreeSelection> get_selector() { return _tree_selection; }
  protected:
    /** Set up 
	@param columns Column record descriptor 
	@param model   Tree view model */
    void setup(const ViewColumns&  columns, 
	       const Glib::RefPtr<Gtk::TreeModel>& model);
    /** set up sorting */
    void setup_sort(const ViewColumns& columns);
    /** Member function to delete a row 
	Users should override this if they store user data with the
	rows */ 
    virtual void delete_entry(Gtk::TreeModel::Row& row) {}
    /** Member function to delete a row 
	Users should override this if they store user data with the
	rows */ 
    virtual void delete_entry(Gtk::TreeModel::iterator& row) {}
    /** Ask tree model to remove these entries 
	@param rows Rows to remove */
    virtual void erase_these(std::vector<Gtk::TreeModel::Row>& rows) {}
    /** Ask tree model to erase all rows */ 
    virtual void erase_all() {}
    /** Get the column descriptor 
	@return The column descriptor */
    virtual const ViewColumns& view_columns() const = 0;
    /** The tree view */
    mutable Gtk::TreeView _view;
    /** The selection */
    Glib::RefPtr<Gtk::TreeSelection> _tree_selection;
    /** Id counter for  new entries */
    int _new_cnt;
  };
  //__________________________________________________________________
  /** @class ScrolledTreeView 
      @brief Base class for tree views 
      @ingroup view */
  class ScrolledTreeView : public ScrolledView 
  {
  public:
    /** Constructor 
	@param data    The Glade-- data */
    ScrolledTreeView();
    /** Destructor */
    virtual ~ScrolledTreeView() {}
    /** Virtual function called when the selection is changed in the
	tree view */
    virtual void on_selection();
    /** Add a row with the specified ID 
	@param id 
	@param row Newly made row */
    virtual void add_row(int id, Gtk::TreeModel::Row& row);
    /** Remove a selected row from the table  
	@note Only rows with id less than 0 can be removed */
    virtual bool remove_entry();
  protected:
    /** Set up 
	@param columns Column record descriptor */
    void setup(const ViewColumns& columns);
    /** Ask tree model to remove these entries 
	@param rows Rows to remove */
    void erase_these(std::vector<Gtk::TreeModel::Row>& rows);
    /** Ask tree model to erase all rows */ 
    void erase_all();
    /** Model used */
    Glib::RefPtr<Gtk::TreeStore> _tree_model;
  };    

  //__________________________________________________________________
  /** @class ScrolledListView 
      @brief Base class for list views 
      @ingroup view */
  class ScrolledListView : public ScrolledView 
  {
  public:
    /** Constructor 
	@param data    The Glade-- data */
    ScrolledListView();
    /** Destructor */
    virtual ~ScrolledListView() {}
    /** Add a row with the specified ID 
	@param id 
	@param row Newly made row */
    virtual void add_row(int id, Gtk::TreeModel::Row& row);
    /** Remove a selected row from the table  
	@note Only rows with id less than 0 can be removed */
    virtual bool remove_entry();
  protected:
    /** Set up 
	@param columns Column record descriptor */
    void setup(const ViewColumns& columns, bool sort);
    /** Ask tree model to remove these entries 
	@param rows Rows to remove */
    void erase_these(std::vector<Gtk::TreeModel::Row>& rows);
    /** Ask tree model to erase all rows */ 
      void erase_all();
    /** Model used */
    Glib::RefPtr<Gtk::ListStore>  _tree_model;
  };    
}

#endif
//
// EOF
//

