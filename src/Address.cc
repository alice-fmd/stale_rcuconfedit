// 
//
//
#include "config.h"
#include "Address.hh"
#include "PickDialog.hh"
#include "ErrorDialog.hh"
#include "Util.hh"

//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>

#include <rcuconf/Address.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>

#include <sstream>
#include <iomanip>
#include <iostream>

//====================================================================
RcuConfEdit::AddressProxy::~AddressProxy() 
{
  if (_obj) delete _obj;
}
//____________________________________________________________________
int
RcuConfEdit::AddressProxy::id() const
{
  return (_obj ? _obj->Id() : _id );
}
//____________________________________________________________________
unsigned int
RcuConfEdit::AddressProxy::raw() const
{
  return (_obj ? _obj->RawValue() : _raw );
}
//____________________________________________________________________
unsigned int
RcuConfEdit::AddressProxy::board() const
{
  return (_obj ? _obj->Board() : (_raw >> 7) & 0x1F );
}
//____________________________________________________________________
unsigned int
RcuConfEdit::AddressProxy::chip() const
{
  return (_obj ? _obj->Chip() : (_raw >> 4) & 0x7 );
}
//____________________________________________________________________
unsigned int
RcuConfEdit::AddressProxy::channel() const
{
  return (_obj ? _obj->Channel() : _raw & 0xf );
}

//____________________________________________________________________
RcuConf::Address*
RcuConfEdit::AddressProxy::obj()
{
  if (_obj) return _obj;
  return _obj = new RcuConf::Address(board(),chip(),channel());
}

//====================================================================
bool
RcuConfEdit::AddressHandler::commit()
{
  if (!_server) return false;

  AddressList l;
  _view.get_new(l);
  for (AddressList::iterator i = l.begin(); i != l.end(); ++i) {
    if (!(*i)->obj()->Insert(*_server)) {
      ErrorDialog::run_it("Failed to insert Address into database",
                          _server->ErrorString());
      return false;
    }
  }
  return true;    
}
//____________________________________________________________________
bool
RcuConfEdit::AddressHandler::update()
{
  if (!_server) return false;
  
  RcuConf::Address::List l;
  if (!RcuConf::Address::Select(l, *_server, "")) {
    ErrorDialog::run_it("Failed to update address list", 
			_server->ErrorString());
    return false;
  }
  for (RcuConf::Address::List::iterator i = l.begin(); i != l.end(); ++i) 
    _view.add_entry(new AddressProxy(*i));
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::AddressHandler::edit(bool copy)
{
  AddressProxy* p = 0;
  if (copy) _view.get_entry(p);

  bool ret = AddressDialog::create(*this,p);
  if (!ret || !p)  return false;

  _view.add_entry(p);
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::AddressHandler::pick(int& ret) const
{
  AddressView v;
  v.copy(_view);
  return PickDialog::run_it("Address", v, ret);
}

//____________________________________________________________________
RcuConfEdit::AddressProxy*
RcuConfEdit::AddressHandler::find(int address) const
{
  AddressProxy* p = 0;
  _view.find_entry(address, p);
  return p;
}

//====================================================================
RcuConfEdit::AddressView::AddressView() 
{  
  setup(_columns,true);
  //Add the TreeView's view columns:
#if !(GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION <= 4)
  _view.append_column_numeric("Board",   _columns._board,   "0x%02x");
  _view.append_column_numeric("Chip",    _columns._chip,    "0x%01x");
  _view.append_column_numeric("Channel", _columns._channel, "0x%01x");
#else
  _view.append_column("Board",   _columns._board);
  _view.append_column("Chip",    _columns._chip);
  _view.append_column("Channel", _columns._channel);
#endif
  // Initial sorting column
  //_tree_model->set_sort_column(_columns._id, Gtk::SORT_ASCENDING ); 
  setup_sort(_columns);
}

//____________________________________________________________________
void
RcuConfEdit::AddressView::add_entry(AddressProxy* p)
{
  
  if (!p) return;
  AddressList l;
  get_all(l);
  for (size_t i = 0; i < l.size(); i++) {
    if (l[i]->raw() == p->raw()) {
      std::stringstream s;
      s << "Address <tt>0x" << std::hex << std::setw(3) << p->raw() 
	<< "</tt> already exists in table";
      ErrorDialog::run_it("Invalid value", s.str());
      return;
    }
  }
  Gtk::TreeModel::Row row;
  add_row(p->id(), row);
  p->set_id(row[_columns._id]);
  row[_columns._board]       = p->board();
  row[_columns._chip]        = p->chip();
  row[_columns._channel]     = p->channel();
  row[_columns._obj]         = p;
}

//____________________________________________________________________
void
RcuConfEdit::AddressView::on_selection()
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  AddressProxy* p = row[_columns._obj];
  // if (!p) return;
  // p->Print();
}

//____________________________________________________________________
void
RcuConfEdit::AddressView::get_entry(AddressProxy*& p) const
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  p = row[_columns._obj];
}
//____________________________________________________________________
void
RcuConfEdit::AddressView::delete_entry(Gtk::TreeModel::Row& row)
{
  AddressProxy* p = row[_columns._obj];
  if (!p) return;
  delete p;
} 
//____________________________________________________________________
void
RcuConfEdit::AddressView::find_entry(int id, AddressProxy*&   p) const
{
  Gtk::TreeModel::Row row;
  if (!find_row(id, row)) return;
  p = row[_columns._obj];
}
//____________________________________________________________________
void
RcuConfEdit::AddressView::copy(const AddressView& other)
{
  Gtk::TreeModel::Children children = other._tree_model->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin();
       i != children.end(); ++i) {
    Gtk::TreeModel::Row row     = *i;
    Gtk::TreeModel::Row new_row = *(_tree_model->append());
    Gtk::copy_cell(_columns._id, row, new_row);
    Gtk::copy_cell(_columns._board,   row, new_row);
    Gtk::copy_cell(_columns._chip,    row, new_row);
    Gtk::copy_cell(_columns._channel, row, new_row);
    Gtk::copy_cell(_columns._obj,     row, new_row);
  }
}
//____________________________________________________________________
void
RcuConfEdit::AddressView::get_all(AddressList& l) const
{
  Gtk::TreeModel::Children c = _tree_model->children();
  l.resize(c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    Gtk::TreeModel::Row r = c[i];
    AddressProxy*   p = r[_columns._obj];
    l[i]                  = p;
  }
}
//____________________________________________________________________
void
RcuConfEdit::AddressView::get_new(AddressList& l) const
{
  l.clear();
  Gtk::TreeModel::Children c = _tree_model->children();
  for (size_t i = 0; i < c.size(); i++) { 
    Gtk::TreeModel::Row r = c[i];
    int id = r[_columns._id];
    if (id >= 0) continue;
    AddressProxy* p = r[_columns._obj];
    l.push_back(p);
  }
}



//====================================================================
RcuConfEdit::AddressDialog* RcuConfEdit::AddressDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::AddressDialog::AddressDialog(AddressHandler& h)
  : _handler(h), 
    _board_label("Board"),
    _chip_label("Chip"),
    _channel_label("Channel"),
    _table(2, 2, false)
{
  _board_label.set_alignment(0,0.5);
  _board_label.set_padding(0,0);
  _board_label.set_justify(Gtk::JUSTIFY_LEFT);
  _board_label.set_line_wrap(false);
  _board_label.set_use_markup(false);
  _board_label.set_selectable(false);
  _board_entry.set_flags(Gtk::CAN_FOCUS);
  _board_entry.set_range(0x0, 0x1f);

  _chip_label.set_alignment(0,0.5);
  _chip_label.set_padding(0,0);
  _chip_label.set_justify(Gtk::JUSTIFY_LEFT);
  _chip_label.set_line_wrap(false);
  _chip_label.set_use_markup(false);
  _chip_label.set_selectable(false);
  _chip_entry.set_flags(Gtk::CAN_FOCUS);
  _chip_entry.set_range(0x0, 0x7);
  //_chip_entry.set_increments(0x0, 0x4);

  _channel_label.set_alignment(0,0.5);
  _channel_label.set_padding(0,0);
  _channel_label.set_justify(Gtk::JUSTIFY_LEFT);
  _channel_label.set_line_wrap(false);
  _channel_label.set_use_markup(false);
  _channel_label.set_selectable(false);
  _channel_entry.set_flags(Gtk::CAN_FOCUS);
  _channel_entry.set_range(0x0, 0xf);
  //_channel_entry.set_increments(0x0, 0x7);

  Gtk::AttachOptions a1 = Gtk::EXPAND|Gtk::FILL;
  Gtk::AttachOptions a2 = Gtk::AttachOptions();
  _table.set_row_spacings(0);
  _table.set_col_spacings(0);
  _table.attach(_board_label,   0, 1, 0, 1, Gtk::FILL);
  _table.attach(_chip_label,    0, 1, 1, 2, Gtk::FILL);
  _table.attach(_channel_label, 0, 1, 2, 3, Gtk::FILL);
  _table.attach(_board_entry,   1, 2, 0, 1, a1, a2, 3, 0);
  _table.attach(_chip_entry,    1, 2, 1, 2, a1, a2, 3, 0);
  _table.attach(_channel_entry, 1, 2, 2, 3, a1, a2, 3, 0);
  
  _inner_vbox.pack_start(_table);
}
  
//____________________________________________________________________
bool
RcuConfEdit::AddressDialog::validate()
{
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::AddressDialog::create(AddressHandler& h, AddressProxy*& p)
{
  if (!_instance) _instance = new AddressDialog(h);
  _instance->set_top("Address", (p ? p->id() : -1), "");
  _instance->_board_entry.set_value(p ? p->board() : 0);
  _instance->_chip_entry.set_value(p ? p->chip() : 0);
  _instance->_channel_entry.set_value(p ? p->channel() : 0);

  if (_instance->run_it() != OK) return false;

  int nboard    = int(_instance->_board_entry.get_value());
  int nchip     = int(_instance->_chip_entry.get_value());
  int nchannel  = int(_instance->_channel_entry.get_value());
  
  if (p && (nchannel == p->channel() && 
	    nchip    == p->chip()    && 
	    nboard   == p->board())) return false;
  
  p = new AddressProxy(-1, nboard, nchip, nchannel);
  return true;
}
//____________________________________________________________________
//
// EOF
//
