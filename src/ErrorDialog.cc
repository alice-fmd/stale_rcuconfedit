//
//
// 
#include "config.h"
#include "ErrorDialog.hh"

//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>

//____________________________________________________________________
RcuConfEdit::ErrorDialog* RcuConfEdit::ErrorDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::ErrorDialog::ErrorDialog()
{  
  _ok_button = Gtk::manage(new Gtk::Button(Gtk::StockID("gtk-ok")));
  _ok_button->set_flags(Gtk::CAN_FOCUS);
  _ok_button->set_flags(Gtk::CAN_DEFAULT);
  _ok_button->set_relief(Gtk::RELIEF_NORMAL);

  _image = Gtk::manage(new Gtk::Image(Gtk::StockID("gtk-dialog-error"), 
				      Gtk::IconSize(6)));
  _image->set_alignment(0.5,0.5);
  _image->set_padding(4,0);

  _title_label = Gtk::manage(new Gtk::Label("<b>Title</b>"));
  _title_label->set_alignment(0.5,0.5);
  _title_label->set_padding(0,0);
  _title_label->set_justify(Gtk::JUSTIFY_CENTER);
  _title_label->set_line_wrap(true);
  _title_label->set_use_markup(true);
  _title_label->set_selectable(false);

  _message_label = Gtk::manage(new Gtk::Label("<i>Message</i>"));
  _message_label->set_alignment(0.5,0.5);
  _message_label->set_padding(0,0);
  _message_label->set_justify(Gtk::JUSTIFY_LEFT);
  _message_label->set_line_wrap(true);
  _message_label->set_use_markup(true);
  _message_label->set_selectable(false);

  _vbox = Gtk::manage(new Gtk::VBox(false, 0));
  _vbox->pack_start(*_title_label, Gtk::PACK_SHRINK, 0);
  _vbox->pack_start(*_message_label, Gtk::PACK_EXPAND_WIDGET, 3);

  _hbox = Gtk::manage(new Gtk::HBox(false, 0));
  _hbox->pack_start(*_image, Gtk::PACK_SHRINK, 0);
  _hbox->pack_start(*_vbox);

  get_action_area()->property_layout_style().set_value(Gtk::BUTTONBOX_END);
  get_vbox()->set_homogeneous(false);
  get_vbox()->set_spacing(0);
  get_vbox()->pack_start(*_hbox);
  set_title("Error ...");
  set_modal(false);
  property_window_position().set_value(Gtk::WIN_POS_NONE);
  set_resizable(true);
  property_destroy_with_parent().set_value(false);
  set_has_separator(true);
  add_action_widget(*_ok_button, -5);

  _ok_button->show();
  _image->show();
  _title_label->show();
  _message_label->show();
  _vbox->show();
  _hbox->show();
}

//____________________________________________________________________
RcuConfEdit::ErrorDialog::~ErrorDialog()
{  
}

//____________________________________________________________________
void
RcuConfEdit::ErrorDialog::run_it(const std::string& title, 
				 const std::string& message) 
{
  if (!_instance) _instance = new ErrorDialog;
  _instance->_title_label->set_label(title);
  _instance->_message_label->set_label(message);
  _instance->show_all();
  _instance->run();
  _instance->hide_all();
}

//____________________________________________________________________
//
// EOF
//
