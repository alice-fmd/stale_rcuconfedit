#ifndef RCUCONFEDIT_Handler
#define RCUCONFEDIT_Handler

namespace RcuDb
{
  class Server;
}

namespace RcuConfEdit
{
  // Forward declaration 
  class ScrolledView;
  class EditDialog;
  class GlademmData;

  /** @defgroup handler Database access and data handlers.  

      These classes handles the data retrieved and put to the
      database.  They provide a uniform interface for the rest of the
      application. */

  /** Handler of the various tables in the database 
      @ingroup handler */
  class Handler 
  {
  public:
    /** Constructor */
    Handler() : _server(0) {}
    /** Destructor */ 
    ~Handler() {}
    /** Set the server */ 
    bool set_server(RcuDb::Server* server);
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    virtual ScrolledView& get_view()  = 0;
    /** Commit new values to the database 
	@return true on success, false otherwise */
    virtual bool commit() = 0;
    /** Update a view from the data base 
	@return true on success, false otherwise */
    virtual bool update() = 0;
    /** Create an edit dialog 
	@param edit If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return Newly created edit dialog */
    virtual bool edit(bool copy) = 0;
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    virtual bool pick(int& id) const = 0;
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    virtual bool remove() { return get_view().remove_entry(); }
    /** Refresh the view */ 
    virtual bool refresh(bool all=true);
  protected:
    /** Reference to DB server */
    RcuDb::Server* _server;
  };
  //__________________________________________________________________
  inline bool
  Handler::set_server(RcuDb::Server* server) 
  {
    _server = server;
    return refresh(true);
  }
  //__________________________________________________________________
  inline bool
  Handler::refresh(bool all) 
  {
    get_view().clear(all);
    if (!_server) return true;
    return update();
  }
  
}

#endif
//
// EOF
//

    
