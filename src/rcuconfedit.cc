// generated 2007/7/2 22:00:26 CEST by cholm@cholm.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to project1.cc_new

// This file is for your program, I won't touch it again!

#include <config.h>
#include <gtkmm/main.h>
// #include <glib/gi18n.h>

#include <rcudb/Server.h>
#include "MainWindow.hh"
#include "ErrorDialog.hh"

int main(int argc, char **argv)
{  
   Gtk::Main m(argc, argv);

   RcuConfEdit::MainWindow* w = new RcuConfEdit::MainWindow;
   m.run(*w);
   delete w;
   return 0;
}
