#ifndef RCUCONFEDIT_MenuBar
#define RCUCONFEDIT_MenuBar
#include <gtkmm/menubar.h>
#include <gtkmm/menu.h>

namespace RcuConfEdit
{
  // Forward declaration 
  class MainWindow;
  
  /** @class MenuBar 
      @brief Our menubar 
      @ingroup core */
  class MenuBar : public Gtk::MenuBar 
  {
  public:
    /** Constructor 
	@param m The main window */
    MenuBar(MainWindow& m);
    /** Destructor */
    virtual ~MenuBar() {}
    /** Signal whether we're connected or not 
	@param con If @c true, we're connected */
    void connected(bool con);
    /** Signal whether we're changed 
	@param chan If @c true, we're changed */ 
    void changed(bool chan);
  protected:
    /** @{ 
	@name File menu */
    /** File menu */
    Gtk::Menu _file;
    /** File-Open */
    Gtk::ImageMenuItem* _open;
    /** File-Close */
    Gtk::ImageMenuItem* _close;
    /** File-Save */
    Gtk::ImageMenuItem* _save;
    /** File-separator */
    Gtk::ImageMenuItem* _quit;
    /** @} */

    /** @{ 
	@name Edit menu */
    /** Edit menu */
    Gtk::Menu _edit;
    /** Edit-Copy */
    Gtk::ImageMenuItem* _copy;
    /** Edit-Paste */
    Gtk::ImageMenuItem* _paste;
    /** Edit-Clear */
    Gtk::ImageMenuItem* _clear;
    /** @} */
    
    /** @{ 
	@name Help menu */
    /** Edit menu */
    Gtk::Menu _help;
    /** Help-About */
    Gtk::ImageMenuItem* _about;
    /** @} */
  };
}

#endif

    
