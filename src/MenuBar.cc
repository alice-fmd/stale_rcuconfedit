#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gtk/gtkimagemenuitem.h>
#include <gtkmm/imagemenuitem.h>
#include <gtkmm/menuitem.h>
#include "MenuBar.hh"
#include "MainWindow.hh"

//____________________________________________________________________
RcuConfEdit::MenuBar::MenuBar(MainWindow& m)
  : _file(),
    _open(0),
    _close(0),
    _save(0),
    _quit(0),
    _edit(),
    _copy(0),
    _paste(0),
    _clear(0),
    _help(),
    _about(0)
{
  using namespace Gtk::Menu_Helpers;
  items().push_back(MenuElem("File", _file));
  _file.items().push_back(StockMenuElem(Gtk::StockID("gtk-open")));
  _open = static_cast<Gtk::ImageMenuItem*>(&_file.items().back());
  _file.items().push_back(StockMenuElem(Gtk::StockID("gtk-close")));
  _close = static_cast<Gtk::ImageMenuItem*>(&_file.items().back());
  _file.items().push_back(StockMenuElem(Gtk::StockID("gtk-save")));
  _save = static_cast<Gtk::ImageMenuItem*>(&_file.items().back());
  _file.items().push_back(SeparatorElem());
  _file.items().push_back(StockMenuElem(Gtk::StockID("gtk-quit")));
  _quit = static_cast<Gtk::ImageMenuItem*>(&_file.items().back());

  items().push_back(MenuElem("Edit", _edit));
  _edit.items().push_back(StockMenuElem(Gtk::StockID("gtk-copy")));
  _copy = static_cast<Gtk::ImageMenuItem*>(&_edit.items().back());
  _edit.items().push_back(StockMenuElem(Gtk::StockID("gtk-new")));
  _paste = static_cast<Gtk::ImageMenuItem*>(&_edit.items().back());
  _edit.items().push_back(StockMenuElem(Gtk::StockID("gtk-remove")));
  _clear = static_cast<Gtk::ImageMenuItem*>(&_edit.items().back());

  items().push_back(MenuElem("Help", _help));
  _help.items().push_back(StockMenuElem(Gtk::StockID("gtk-about")));
  _about = static_cast<Gtk::ImageMenuItem*>(&_help.items().back());
  
  
  show_all();
  connected(false);
  changed(false);
  
  _open->signal_activate().connect(SigC::slot(m,&MainWindow::on_open),false);
  _close->signal_activate().connect(SigC::slot(m,&MainWindow::on_close),false);
  _save->signal_activate().connect(SigC::slot(m,&MainWindow::on_save),false);
  _quit->signal_activate().connect(SigC::slot(m,&MainWindow::on_quit),false);

  _copy->signal_activate().connect(SigC::slot(m,&MainWindow::on_copy),false);
  _paste->signal_activate().connect(SigC::slot(m,&MainWindow::on_paste),false);
  _clear->signal_activate().connect(SigC::slot(m,&MainWindow::on_clear),false);

  _about->signal_activate().connect(SigC::slot(m,&MainWindow::on_about),false);
}

//____________________________________________________________________
void
RcuConfEdit::MenuBar::connected(bool con)
{
  _open->set_sensitive(!con);
  _save->set_sensitive(false);
  _close->set_sensitive(con);
  _copy->set_sensitive(con);
  _paste->set_sensitive(con);
  _clear->set_sensitive(con);
}

//____________________________________________________________________
void
RcuConfEdit::MenuBar::changed(bool chan)
{
  _save->set_sensitive(chan);
}

#if 0
// Below follows an alternate implementation that uses a UIManager 
//____________________________________________________________________
RcuConfEdit::MenuBar::MenuBar(MainWindow& m)
  : _file(),
    _open(0),
    _close(0),
    _save(0),
    _quit(0),
    _edit(),
    _copy(0),
    _paste(0),
    _clear(0),
    _help(),
    _about(0)
{
  Glib::ustring info = 
    "<ui>"
    "  <menubar name='Menu'>"
    "    <menu action='File'>"
    "      <menuitem action='Open'/>"
    "      <menuitem action='Save'/>"
    "      <menuitem action='Close'/>"
    "      <separator/>"
    "      <menuitem action='Quit'/>"
    "    </menu>"
    "    <menu action='Edit'>"
    "      <menuitem action='Copy'/>"
    "      <menuitem action='Paste'/>"
    "      <menuitem action='Clear'/>"
    "    </menu>"
    "    <menu action='Help'>"
#if !(GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION <= 4)
    "      <menuitem action='About'/>"
#endif
    "    </menu>"
    "  </menubar>"
    "</ui>";
  _actions = Gtk::ActionGroup::create();
  _actions->add(Gtk::Action::create("File","File"));
  _actions->add(Gtk::Action::create("Open", Gtk::Stock::OPEN));
  _actions->add(Gtk::Action::create("Save", Gtk::Stock::SAVE));
  _actions->add(Gtk::Action::create("Close",Gtk::Stock::CLOSE));
  _actions->add(Gtk::Action::create("Quit", Gtk::Stock::QUIT));
  _actions->add(Gtk::Action::create("Edit","Edit"));
  _actions->add(Gtk::Action::create("Copy" ,Gtk::Stock::COPY));
  _actions->add(Gtk::Action::create("Paste",Gtk::Stock::PASTE));
  _actions->add(Gtk::Action::create("Clear",Gtk::Stock::CLEAR));
  _actions->add(Gtk::Action::create("Help","Help"));
#if !(GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION <= 4)
  _actions->add(Gtk::Action::create("About",Gtk::Stock::ABOUT));
#endif

  _ui      = Gtk::UIManager::create();
  _ui->insert_action_group(_actions);
  add_accel_group(_ui->get_accel_group());
  _ui->add_ui_from_string(info);
  

  //gnome_app_fill_menu(GTK_MENU_SHELL(_menu_bar.gobj()), 
  // _menu_bar_uiinfo, _gmm_data->getAccelGroup()->gobj(), 
  // 		      true, 0);
  _menu_bar     = (Gtk::MenuBar*)(_ui->get_widget("/Menu"));
  //_toolbar    = (Gtk::ToolBar*)(_ui->get_widget("/ToolBar"));
  _open	        = (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/File/Open"));
  _save	        = (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/File/Save"));
  _close	= (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/File/Close"));
  _quit	        = (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/File/Quit"));
  _copy	 	= (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/Edit/Copy"));
  _paste	= (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/Edit/Paste"));
  _clear	= (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/Edit/Clear"));
#if !(GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION <= 4)
  _about	= (Gtk::ImageMenuItem*)(_ui->get_widget("/Menu/Help/About"));
#endif
  _open->signal_activate().connect(SigC::slot(m,&MainWindow::on_open),false);
  _close->signal_activate().connect(SigC::slot(m,&MainWindow::on_close),false);
  _save->signal_activate().connect(SigC::slot(m,&MainWindow::on_save),false);
  _quit->signal_activate().connect(SigC::slot(m,&MainWindow::on_quit),false);

  _copy->signal_activate().connect(SigC::slot(m,&MainWindow::on_copy),false);
  _paste->signal_activate().connect(SigC::slot(m,&MainWindow::on_paste),false);
  _clear->signal_activate().connect(SigC::slot(m,&MainWindow::on_clear),false);

  _about->signal_activate().connect(SigC::slot(m,&MainWindow::on_about),false);
}
#endif
//
// EOF
//

