#ifndef RCUCONFEDIT_Rcu_TrCfg1
#define RCUCONFEDIT_Rcu_TrCfg1
#include "Widget.hh"
#include "Util.hh"
#ifndef GTKMM_2_0_0
# include <gtkmm/comboboxtext.h>
#endif
#include <gtkmm/label.h>
#include <sstream>
#include <iostream>
#include <iomanip>
namespace RcuConfEdit
{
    
  //__________________________________________________________________
  struct TrCfg1 : public ValueWidget 
  {
    TrCfg1()
      : _trigger_label("Trigger type"),
	_trigger(),
	_tws("Internal L2 delay", 0, 0x3FFF,1,1024),
	_mode_label("Read-out mode"),
	_mode(),
	_buffers_label("Number of buffers"),
	_buffers(),
	_opt("BC optimized readout"),
	_table(2, 5)
    {
      _trigger.append_text("Internal L1");
      _trigger.append_text("External L1, Internal L2");
      _trigger.append_text("External L1 and L2");
      _trigger.set_active(2);
      _mode.append_text("Push");
      _mode.append_text("Pop");
      _mode.set_active(0);
      _buffers.append_text("4 buffers");
      _buffers.append_text("8 buffers");
      _buffers.set_active(0);
      _table.set_col_spacings(5);
      _table.attach(_trigger_label, 0, 1, 0, 1,Gtk::SHRINK|Gtk::FILL);
      _table.attach(_trigger,       1, 2, 0, 1,Gtk::EXPAND|Gtk::FILL);
      _table.attach(_tws,           0, 2, 1, 2,Gtk::EXPAND|Gtk::FILL);
      _table.attach(_buffers_label, 0, 1, 2, 3,Gtk::SHRINK|Gtk::FILL);
      _table.attach(_buffers,       1, 2, 2, 3,Gtk::EXPAND|Gtk::FILL);
      _table.attach(_mode_label,    0, 1, 3, 4,Gtk::SHRINK|Gtk::FILL);
      _table.attach(_mode,          1, 2, 3, 4,Gtk::EXPAND|Gtk::FILL);
      _table.attach(_opt,           0, 2, 4, 5,Gtk::EXPAND);
      pack_start(_table, Gtk::PACK_EXPAND_WIDGET, 3);
      _trigger.signal_changed()
	.connect(SigC::slot(*this,&TrCfg1::on_trigger));
    }
    const char* name() const { return "TRCFG1"; }
    void on_trigger()
    {
      _tws.set_sensitive(_trigger.get_active_row_number()==1);
    }
    void set_single_value(unsigned int v) 
    {
      _tws.set_value(0x3fff & v);
      _opt.set_active((v & (1 << 17)));
      _buffers.set_active((v & (1 << 14)) ? 1 : 0);
      std::cout << "Got mode " << ((v >> 15) & 0x3) << std::endl;
      switch ((v >> 15) & 0x3) {
      case 0: _trigger.set_active(0); break;
      case 2: _trigger.set_active(1); break;
      case 3: _trigger.set_active(2); break;
      }
      _mode.set_active((v & (1 << 18)) ? 1 : 0);
    }
    unsigned int get_single_value() const 
    {
      int ret = 0;
      ret |= _tws.get_value() & 0x3fff;
      ret |= (_opt.get_active()  ? 1 << 17 : 0);
      ret |= (_buffers.get_active_row_number() == 1 ? 1 << 14 : 0);
      std::cout << _trigger.get_active_row_number() << std::endl;
      switch (_trigger.get_active_row_number()) {
      case 0: ret |= (0 & 0x3) << 15; break;
      case 1: ret |= (2 & 0x3) << 15; break;
      case 2: ret |= (3 & 0x3) << 15; break;
      }
      ret |= (_mode.get_active_row_number() ? 1 << 18 : 0);
      return ret;
    }
    Gtk::Label              _trigger_label;
    Gtk::ComboBoxText       _trigger;
    Gtk::HLabeledSpinButton _tws;
    Gtk::Label              _mode_label;
    Gtk::ComboBoxText       _mode;
    Gtk::Label              _buffers_label;
    Gtk::ComboBoxText	    _buffers;
    Gtk::CheckButton        _opt;
    Gtk::Table              _table;
  };
  /** @} */
}
#endif 
//
// EOF
//
