#ifndef RCUCONFEDIT_Rcu_ActFec
#define RCUCONFEDIT_Rcu_ActFec
#include "Widget.hh"
//#include "Util.hh"
//#include <gtkmm/checkbutton.h>
//#include <gtkmm/spinbutton.h>
//#include <gtkmm/comboboxtext.h>
//#include <gtkmm/label.h>
//#include <gtkmm/treestore.h>
#include <sstream>
//#include <iostream>
#include <iomanip>
namespace RcuConfEdit
{
  /** @{
      @name Rcu Widgets */
  //__________________________________________________________________
  struct ActFec : public ValueWidget 
  {
    ActFec()
      : _branch_a("Branch A"), 
	_branch_b("Branch B")
    {
      Gtk::HBox* a = &_hbox_a;
      for (size_t i = 0; i < 32; i++) {
	std::stringstream s; s << std::setw(2) << (i % 16);
	_bits[i].set_label(s.str());
	_bits[i].set_mode(false); // < Normal button 
	if (i == 16) a = &_hbox_b;
	a->pack_start(_bits[i]);
      }
      _hbox_a.set_homogeneous(true);
      _hbox_b.set_homogeneous(true);
      _branch_a.add(_hbox_a);
      _branch_b.add(_hbox_b);
      pack_start(_branch_a);
      pack_start(_branch_b);
    }
    void set_single_value(unsigned int v)
    {
      for (size_t i = 0; i < 32; i++) _bits[i].set_active(v & (1 << i));
    }
    unsigned int get_single_value() const
    {
      unsigned int ret = 0;
      for (size_t i = 0; i < 32; i++) 
	if (_bits[i].get_active()) ret |= (1 << i);
      return ret;
    }
    const char* name() const { return "ACTFEC"; }
    Gtk::Frame       _branch_a;
    Gtk::HBox        _hbox_a;
    Gtk::Frame       _branch_b;
    Gtk::HBox        _hbox_b;
    Gtk::CheckButton _bits[32];
  };
}
#endif
//
// EOF
//
