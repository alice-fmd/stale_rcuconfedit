#ifndef RCUCONFEDIT_Rcu_Acl
#define RCUCONFEDIT_Rcu_Acl
#include "Widget.hh"
#include "Util.hh"
//#include <gtkmm/checkbutton.h>
//#include <gtkmm/spinbutton.h>
//#include <gtkmm/comboboxtext.h>
#include <gtkmm/label.h>
#include <gtkmm/treestore.h>
#include <sstream>
#include <iomanip>
namespace RcuConfEdit
{
  //__________________________________________________________________
  struct Acl : public ValueWidget 
  {
    struct Columns : public Gtk::TreeModelColumnRecord 
    {
      Columns() { 
	add(_where);
	add(_value);
	for (size_t i = 0; i < 16; i++) add(_bit[i]);
      }
      Gtk::TreeModelColumn<Glib::ustring> _where;
      Gtk::TreeModelColumn<int>           _value;
      Gtk::TreeModelColumn<bool>          _bit[16];
    } _columns;
    Acl()
      : _renderer(0x0,0xFFFF),
	_tree(), 
	_scroll()
    {
      _model = Gtk::TreeStore::create(_columns);
      _tree.set_model(_model);
      _tree.append_column("Altro/Channel",_columns._where);
      _renderer.append_to_view(_tree, "Value", _columns._value);
      _renderer.signal_edited()
	.connect(SigC::slot(*this,&Acl::on_edited));
      for (size_t i = 0; i < 16; i++) {
	std::stringstream s; s << i;
	_tree.append_column_editable(s.str(), _columns._bit[i]);
	Gtk::CellRendererToggle* renderer = 
	  ((Gtk::CellRendererToggle*)_tree.get_column_cell_renderer(i+2));
	renderer->signal_toggled()
	  .connect(SigC::slot(*this,&Acl::on_toggle));
#ifndef GTKMM_2_0_0
	Gtk::TreeView::Column* column = _tree.get_column(i+2);
	column->set_cell_data_func(*renderer,
				   SigC::slot(*this,&Acl::on_render));
	if (i == 0) _def_size = renderer->property_indicator_size();
#endif
      }
      _scroll.add(_tree);
      pack_start(_scroll);
      for (size_t b = 0; b < 32; b++) {
	std::stringstream bs;
	bs << "Board 0x" << std::hex << std::setfill('0') << std::setw(2) << b;
	Gtk::TreeModel::Row row = *(_model->append());
	row[_columns._where] = bs.str();
	row[_columns._value] = -1;
	for (size_t a = 0; a < 8; a++) {
	  Gtk::TreeModel::Row sub = *(_model->append(row.children()));
	  std::stringstream as;
	  as << "Altro 0x" << std::hex << a;
	  sub[_columns._where] = as.str();
	  sub[_columns._value] = 0x0;
	  for (size_t c = 0; c < 16; c++) 
	    sub[_columns._bit[c]] = false;
	}
      }
    }
    const char* name() const { return "Acl"; }
    bool is_single() const { return false; }
    void set_multiple_value(const std::vector<unsigned int>& v) 
    {
      Gtk::TreeModel::Children cl = _model->children();
      size_t k = 0;
      for (size_t b = 0; b < cl.size(); b++) {
	Gtk::TreeModel::Row      r  = cl[b];
	Gtk::TreeModel::Children sc = r.children();
	for (size_t a = 0; a < sc.size(); a++) {
	  Gtk::TreeModel::Row sr = sc[a];
	  sr[_columns._value]    = v[k];
	  for (size_t c = 0; c < 16; c++)
	    sr[_columns._bit[c]] = (v[k] & (1 << c));
	  k++;
	  if (k >= v.size()) break;
	}
	if (k >= v.size()) break;
      }
    }
    void get_multiple_value(std::vector<unsigned int>& v) const
    {
      Gtk::TreeModel::Children cl = _model->children();
      v.resize(cl.size()*8);
      size_t k = 0;
      for (size_t b = 0; b < cl.size(); b++) {
	Gtk::TreeModel::Row      r  = cl[b];
	Gtk::TreeModel::Children sc = r.children();
	for (size_t a = 0; a < sc.size(); a++) {
	  Gtk::TreeModel::Row sr = sc[a];
	  v[k]                   = sr[_columns._value];
	  k++;
	}
      }
    }
  protected:
    void on_render(Gtk::CellRenderer* r, const Gtk::TreeModel::iterator& i)
    {
#ifndef GTKMM_2_0_0
      Gtk::TreeModel::Row row = *i;
      Glib::ustring       w   = row[_columns._where];
      Gtk::CellRendererToggle* t = (Gtk::CellRendererToggle*)r;
      t->property_indicator_size() = (w[0] == 'B' ? 0 : _def_size);
      // if (w[0] != 'B') return;
#endif
    }
    void on_toggle(const Glib::ustring& path) 
    {
      size_t colon = path.find(':');
      if (colon == std::string::npos) return;
      Gtk::TreeModel::Row r = *(_model->get_iter(path));     
      unsigned int        v = r[_columns._value];
      for (size_t c = 0; c < 16; c++) {
	bool on =  r[_columns._bit[c]];
	v       =  (on ? v | (1 << c) : v & ~(1 << c));
      }
      r[_columns._value] = v;
    }
    void on_edited(const Glib::ustring& path,const Glib::ustring&) 
    {
      size_t colon = path.find(':');
      if (colon == std::string::npos) return;
      Gtk::TreeModel::Row r = *(_model->get_iter(path));     
      for (size_t c = 0; c < 16; c++) {
	unsigned int val    = r[_columns._value];
	r[_columns._bit[c]] = (val & (1 << c));
      }
    }
    Gtk::CellRendererHex        _renderer;
    Gtk::TreeView                _tree;
    Glib::RefPtr<Gtk::TreeStore> _model;
    Gtk::ScrolledWindow          _scroll;
    int _def_size;
  };
}
#endif
//
// EOF
//
