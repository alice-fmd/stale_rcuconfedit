// generated 2007/7/2 22:00:26 CEST by cholm@cholm.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to SequenceView.cc_new

// This file is for your program, I won't touch it again!

#include "config.h"
#include "Sequence.hh"
#include <sstream>
#include <iomanip>
#include <iostream>

#include <rcuconf/Sequence.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>

//====================================================================
RcuConfEdit::SequenceView::SequenceView() 
{  
  setup(_columns,true);
  // Make our sole row
  _row    = *(_tree_model->append());
  _row[_columns._id] = 0;
}

//____________________________________________________________________
void
RcuConfEdit::SequenceView::set_entry(int id)
{
  _row[_columns._id] = id;
}

//____________________________________________________________________
//
// EOF
//
