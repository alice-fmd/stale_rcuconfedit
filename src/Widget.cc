//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/frame.h>

#include "config.h"
#include "Widget.hh"
#include "Parameter.hh"
#include "rcu/ActFec.hh"
#include "rcu/Acl.hh"
#include "rcu/TrCfg1.hh"
#include "altro/DpCfg.hh"
#include "altro/TrCfg.hh"


#include <iostream>

//====================================================================
RcuConfEdit::SingleValueWidget* RcuConfEdit::SingleValueWidget::_instance = 0;
//____________________________________________________________________
RcuConfEdit::SingleValueWidget*
RcuConfEdit::SingleValueWidget::instance()
{
  if (!_instance) _instance = new SingleValueWidget;
  return _instance;
}
//____________________________________________________________________
RcuConfEdit::SingleValueWidget::SingleValueWidget()
  : _mask(0xFFFFFFFF), 
    _alignment(0.5, 0.5, 1, 1),
    _frame("Value")
{
  _alignment.add(_value);
  _frame.add(_alignment);
  pack_start(_frame,  Gtk::PACK_SHRINK, 3);
}

//====================================================================
RcuConfEdit::MultiValueWidget* RcuConfEdit::MultiValueWidget::_instance = 0;
//____________________________________________________________________
RcuConfEdit::MultiValueWidget*
RcuConfEdit::MultiValueWidget::instance()
{
  if (!_instance) _instance = new MultiValueWidget;
  return _instance;
}
//____________________________________________________________________
RcuConfEdit::MultiValueWidget::MultiValueWidget()
  : _mask(0xFFFFFFFF),
    _add(Gtk::StockID("gtk-add")),
    _remove(Gtk::StockID("gtk-remove")),
    _up(Gtk::StockID("gtk-go-up")),
    _down(Gtk::StockID("gtk-go-down")),
    _buttons(Gtk::BUTTONBOX_START, 0),
    _alignment(0.5, 0.5, 1, 1),
    _frame("Value")
{
  _add.signal_clicked()
    .connect(SigC::slot(*this,&MultiValueWidget::on_add));
  _remove.signal_clicked()
    .connect(SigC::slot(_view,&Gtk::HexView::remove_selected));
  _up.signal_clicked()
    .connect(SigC::slot(_view,&Gtk::HexView::move_selected_up));
  _down.signal_clicked()
    .connect(SigC::slot(_view,&Gtk::HexView::move_selected_down));
  
  _buttons.pack_start(_add);
  _buttons.pack_start(_remove);
  _buttons.pack_start(_up);
  _buttons.pack_start(_down);

  _hbox.pack_start(_view);
  _hbox.pack_start(_buttons, Gtk::PACK_SHRINK, 0);

  _alignment.add(_hbox);
  _frame.add(_alignment);
  pack_start(_frame,Gtk::PACK_EXPAND_WIDGET, 3);
}

//____________________________________________________________________
void
RcuConfEdit::MultiValueWidget::get_multiple_value(std::vector<unsigned int>& v)
  const
{
  _view.get_data(v);
}
//____________________________________________________________________
void
RcuConfEdit::MultiValueWidget::set_multiple_value(const 
						  std::vector<unsigned int>& v)
{
  _view.set_data(v);
}
//____________________________________________________________________
bool
RcuConfEdit::MultiValueWidget::validate() const
{
  std::vector<unsigned int> v;
  _view.get_data(v);
  for (size_t i = 0; i < v.size(); i++) 
    if (v[i] & ~_mask != 0) return false;
  return true;
}

//====================================================================
RcuConfEdit::CommandValueWidget* RcuConfEdit::CommandValueWidget::_instance =0;
//____________________________________________________________________
RcuConfEdit::CommandValueWidget*
RcuConfEdit::CommandValueWidget::instance()
{
  if (!_instance) _instance = new CommandValueWidget;
  return _instance;
}

//====================================================================
RcuConfEdit::WidgetFactory::WidgetFactory()
{
  // Rcu registers 
  register_widget(new ActFec);
  register_widget(new Acl);
  register_widget(new TrCfg1);
  // BC registers 
  // Altro registers 
  register_widget(new TrCfg);
  register_widget(new DpCfg);
}

//____________________________________________________________________
RcuConfEdit::WidgetFactory::~WidgetFactory()
{
  for (WidgetMap::iterator i = _map.begin(); i!= _map.end(); ++i) {
    if (!i->second) continue;
    delete i->second;
    i->second = 0;
  }
  _map.clear();
}

//____________________________________________________________________
void
RcuConfEdit::WidgetFactory::register_widget(ValueWidget* w)
{
  if (!w) return;
  std::string name = w->name();
  std::string n(name);
  std::transform(name.begin(),name.end(),n.begin(),to_upper());
  WidgetMap::iterator i = _map.find(n);
  if (i != _map.end()) {
    std::cerr << "Widget for " << name << " already exists " 
	      << i->second << std::endl;
    return;
  }
  _map[n] = w;
}

//____________________________________________________________________
RcuConfEdit::ValueWidget*
RcuConfEdit::WidgetFactory::create(ParameterProxy* p)
{
  if (!p) return 0;
  ValueWidget*        w    = 0;
  std::string         name = p->name();
  std::string         n(name);
  std::transform(name.begin(),name.end(),n.begin(),to_upper());
  WidgetMap::iterator i    = _map.find(n);
  if (i != _map.end()) {
    w = i->second;
    std::cout << "Found widget " << w << " for parameter " 
	      << name << std::endl;
    return w;
  }
  
  std::cerr << "Widget for " << name 
	    << " not found, returning a generic widget instead" 
	    << std::endl;
  if (p->blob()) return MultiValueWidget::instance();
  else           return SingleValueWidget::instance();
  return 0;
}
  
//____________________________________________________________________
//
// EOF
//
