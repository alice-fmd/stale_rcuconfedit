//
// 
//
#ifndef GTKMM_2_0_0
#include "config.h"
#include <gtkmmconfig.h>
#include "AboutDialog.hh"

#include <sigc++/compatibility.h>

//____________________________________________________________________
RcuConfEdit::AboutDialog* RcuConfEdit::AboutDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::AboutDialog::AboutDialog()
{  
  set_name(PACKAGE_NAME);
  set_version(PACKAGE_VERSION);
  std::vector<std::string> authors;
  authors.push_back("Christian Holm Christensen <cholm@nbi.dk>");
  set_authors(authors);
  set_copyright("Copyright (C) 2007 Christian Holm Christensen");
  set_website("http://fmd.nbi.dk/fmd/fee/");
  set_website_label("NBI FMD FEE pages");
  set_wrap_license(true);
}

//____________________________________________________________________
RcuConfEdit::AboutDialog::~AboutDialog()
{  
}

//____________________________________________________________________
void 
RcuConfEdit::AboutDialog::run_it() 
{
  if (!_instance) _instance = new AboutDialog;
  _instance->show_all();
  _instance->run();
  _instance->hide_all();
}
#endif
//____________________________________________________________________
//
// EOF
//
