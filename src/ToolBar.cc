#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
# include <gtkmm/toolitem.h>
#endif
// #include <gtk/gtkimagetoolitem.h>
// #include <gtkmm/imagetoolitem.h>
#include "ToolBar.hh"
#include "MainWindow.hh"

//____________________________________________________________________
RcuConfEdit::ToolBar::ToolBar(MainWindow& m)
  : _open(Gtk::StockID("gtk-open")),
    _close(Gtk::StockID("gtk-close")),
    _save(Gtk::StockID("gtk-save")),
    _add(Gtk::StockID("gtk-add")),
    _clear(Gtk::StockID("gtk-remove")),
    _copy(Gtk::StockID("gtk-copy"))

{
  set_tooltips(true);
  set_toolbar_style(Gtk::TOOLBAR_ICONS);
  set_orientation(Gtk::ORIENTATION_HORIZONTAL);
#ifndef GTKMM_2_0_0
  set_show_arrow(true);
  append(_open);
  append(_close);
  append(_save);
  append(_add);
  append(_clear);
  append(_copy);
#else
  using namespace Gtk::Toolbar_Helpers;
  tools().push_back(Element(_open));
  tools().push_back(Element(_close));
  tools().push_back(Element(_save));
  tools().push_back(Element(_add));
  tools().push_back(Element(_clear));
  tools().push_back(Element(_copy));
#endif
  
  connected(false);
  show_all();

  _open.signal_clicked().connect(SigC::slot(m,&MainWindow::on_open),false);
  _close.signal_clicked().connect(SigC::slot(m,&MainWindow::on_close),false);
  _save.signal_clicked().connect(SigC::slot(m,&MainWindow::on_save),false);
  _add.signal_clicked().connect(SigC::slot(m,&MainWindow::on_paste),false);
  _clear.signal_clicked().connect(SigC::slot(m,&MainWindow::on_clear),false);
  _copy.signal_clicked().connect(SigC::slot(m,&MainWindow::on_copy),false);
}

//____________________________________________________________________
void
RcuConfEdit::ToolBar::connected(bool con)
{
  _open.set_sensitive(!con);
  _close.set_sensitive(con);
  _save.set_sensitive(false);
  _add.set_sensitive(con);
  _clear.set_sensitive(con);
  _copy.set_sensitive(con);
}
//____________________________________________________________________
void
RcuConfEdit::ToolBar::changed(bool chan)
{
  _save.set_sensitive(chan);
}

//
// EOF
//

