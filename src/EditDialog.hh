#ifndef RCUDATAEDIT_EditDialog
#define RCUDATAEDIT_EditDialog
#include <gtkmm/dialog.h>
#include <gtkmm/tooltips.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>
#include <gtkmm/box.h>
#include <gtkmm/alignment.h>
#include <gtkmm/frame.h>

namespace RcuConfEdit
{
  /** @defgroup dialog Dialogs */
  /** @defgroup editors Entry editors 
      These classes provide editors for the database table entries. 
  */

  /** @class EditDialog 
      @brief Base class for edit dialogs 
      @ingroup dialog 
      @ingroup editors */
  class EditDialog : public Gtk::Dialog 
  {
  public:
    /** Response types */
    enum { 
      OK = 5, 
      Cancel = 6,
      Help = 11
    };

    /** Constructor  */
    EditDialog();
    /** Destrucotr */
    virtual ~EditDialog() {}
    /** Display the dialog */ 
    virtual int run_it();
    /** Display the dialog */ 
    virtual void stop_it();
    /** Called when user hits OK */
    virtual void on_ok();
    /** Called when user hits Cancel */
    virtual void on_cancel();
    /** Called when user hits Help */
    virtual void on_help();
    /** Validate the input */ 
    virtual bool validate() { return true; }
  protected:
    /** Set the title
	@param type   The stuff to edit 
	@param id     The identifier 
	@param extra  Extra stuff to put behind the id */
    virtual void set_top(const std::string& type, 
			 int id, const std::string& extra="");
    /** Make a grouped frame 
	@param title The title of the frame 
	@param widget Widget to put in the frame 
	@return reference to frame */
    Gtk::Frame* make_frame(const std::string& title, Gtk::Widget& widget);

    /** Tool tips */
    Gtk::Tooltips _tooltips;
    /** OK button */
    Gtk::Button _ok_button;
    /** Cancel button */
    Gtk::Button _cancel_button;
    /** Help button */
    Gtk::Button _help_button;
    /** The title */
    Gtk::Label  _title;
    /** The frame */
    Gtk::Frame _frame;
    /** The alignment */
    Gtk::Alignment _alignment;
    /** The inner vertical frame */
    Gtk::VBox _inner_vbox;
  };
}

#endif
//
// EOF
//

