//
//
//
#include "config.h"
#include "Parameter.hh"
#include "ErrorDialog.hh"
#include "PickDialog.hh"

//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>

#include <rcuconf/Parameter.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>

#include <sstream>
#include <iomanip>
#include <iostream>

//====================================================================
RcuConfEdit::Where to_where(RcuConf::Parameter::Where w) 
{
  switch (w) {
  case RcuConf::Parameter::kRcu:   return RcuConfEdit::Rcu;
  case RcuConf::Parameter::kBc:    return RcuConfEdit::Bc;  
  case RcuConf::Parameter::kAltro: return RcuConfEdit::Altro;
  };
  return RcuConfEdit::Rcu;
}

//____________________________________________________________________
RcuConf::Parameter::Where from_where(RcuConfEdit::Where w) 
{
  switch (w) {
  case RcuConfEdit::Rcu:   return RcuConf::Parameter::kRcu;
  case RcuConfEdit::Bc:    return RcuConf::Parameter::kBc;
  case RcuConfEdit::Altro: return RcuConf::Parameter::kAltro;
  }
  return RcuConf::Parameter::kRcu;
}
//====================================================================
RcuConfEdit::ParameterProxy::ParameterProxy(RcuConf::Parameter* p) 
  : _obj(p) 
{}
//____________________________________________________________________
RcuConfEdit::ParameterProxy::~ParameterProxy()
{ 
  if (_obj) delete _obj;
}
//____________________________________________________________________
int 
RcuConfEdit::ParameterProxy::id() const 
{ 
  return _obj ? _obj->Id() : _id; 
}
//____________________________________________________________________
const std::string&  
RcuConfEdit::ParameterProxy::name() const 
{ 
  return _obj ? _obj->Name() : _name; 
}
//____________________________________________________________________
RcuConfEdit::Where
RcuConfEdit::ParameterProxy::where() const 
{ 
  return _obj ? to_where(_obj->Destination()): _where; 
}
//____________________________________________________________________
bool
RcuConfEdit::ParameterProxy::blob() const 
{ 
  return _obj ? _obj->IsBlob() : _blob; 
}
//____________________________________________________________________
unsigned int 
RcuConfEdit::ParameterProxy::mask() const 
{ 
  return _obj ? _obj->Mask() : _mask; 
}

//____________________________________________________________________
RcuConf::Parameter*
RcuConfEdit::ParameterProxy::obj()
{
  if (_obj) return _obj;
  return _obj = new RcuConf::Parameter(_name, from_where(_where), 
				       _blob, _mask);
}

//====================================================================
bool
RcuConfEdit::ParameterHandler::commit()
{
  if (!_server) return false;

  ParameterList l;
  _view.get_new(l);
  for (ParameterList::iterator i = l.begin(); i != l.end(); ++i) {
    if (!(*i)->obj()->Insert(*_server)) {
      ErrorDialog::run_it("Failed to insert Parameter into database",
                          _server->ErrorString());
      return false;
    }
  }
  return true;    
}
//____________________________________________________________________
bool
RcuConfEdit::ParameterHandler::update()
{
  if (!_server) return false;
  
  RcuConf::Parameter::List l;
  if (!RcuConf::Parameter::Select(l, *_server, "")) {
    ErrorDialog::run_it("Failed to update parameter list", 
			_server->ErrorString());
    return false;
  }
  
  for (RcuConf::Parameter::List::iterator i = l.begin(); i != l.end(); ++i)
    _view.add_entry(new ParameterProxy(*i));
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::ParameterHandler::edit(bool copy)
{
  ParameterProxy* p = 0;
  if (copy) _view.get_entry(p);

  bool ret = ParameterDialog::create(*this,p);

  if (!ret || !p) return false;
  
  _view.add_entry(p);
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::ParameterHandler::pick(int& id) const
{
  ParameterView v;
  v.copy(_view);
  return PickDialog::run_it("Parameter", v, id);
}

//____________________________________________________________________
RcuConfEdit::ParameterProxy*
RcuConfEdit::ParameterHandler::find(int parameter) const
{
  ParameterProxy* p = 0;
  _view.find_entry(parameter, p);
  return p;
}

//____________________________________________________________________
std::string
RcuConfEdit::ParameterHandler::where_to_string(Where where) 
{
  if (where == Rcu) return "RCU";
  if (where == Bc)  return "BC";
  return "ALTRO";
}

//____________________________________________________________________
RcuConfEdit::Where
RcuConfEdit::ParameterHandler::string_to_where(const std::string& s) 
{
  if (s == "RCU") return Rcu;
  if (s == "BC")  return Bc;
  return Altro;
}

//====================================================================
RcuConfEdit::ParameterView::ParameterView() 
{  
  setup(_columns,true);
  //Add the TreeView's view columns:
  _view.append_column("Name",         _columns._name);
  _view.append_column("Where",        _columns._where);
  _view.append_column("Blob",         _columns._blob);
#if !(GTKMM_MAJOR_VERSION == 2 && GTKMM_MINOR_VERSION <= 4)
  _view.append_column_numeric("Mask", _columns._mask, "0x%08x");
#else
  _view.append_column("Mask", _columns._mask);
#endif
  // Set up sort columns 
  setup_sort(_columns);
}

//____________________________________________________________________
void
RcuConfEdit::ParameterView::on_selection()
{
  // ScrolledListView::on_selection();
#if 0
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  RcuConf::Parameter* p = row[_columns._obj];
  // if (p) p->Print();
#endif
}

//____________________________________________________________________
void
RcuConfEdit::ParameterView::add_entry(ParameterProxy* p)
{
  if (!p) return;
  ParameterList l;
  get_all(l);
  for (size_t i = 0; i < l.size(); i++) {
    if (l[i]->name()  == p->name() && 
	l[i]->where() == p->where()) {
      std::stringstream s;
      s << "Parameter <i>" << p->name() << "</i> for <i>"
	<< ParameterHandler::where_to_string(p->where()) 
	<< "</i> already exists!" ;
      ErrorDialog::run_it("Invalid value", s.str());
      return;
    }
  }
  Gtk::TreeModel::Row row;
  add_row(p->id(), row);
  p->set_id(row[_columns._id]);
  row[_columns._name]   = p->name();
  row[_columns._where]  = ParameterHandler::where_to_string(p->where());
  row[_columns._blob]   = p->blob();
  row[_columns._mask]   = p->mask();
  row[_columns._obj]    = p;
}

//____________________________________________________________________
void
RcuConfEdit::ParameterView::get_entry(ParameterProxy*& p) const
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  const Gtk::TreeModel::Row& row = *(_tree_selection->get_selected());
  p    = row[_columns._obj];
}
//____________________________________________________________________
void
RcuConfEdit::ParameterView::delete_entry(Gtk::TreeModel::Row& row)
{
  ParameterProxy* p = row[_columns._obj];
  if (!p) return;
  delete p;
}
//____________________________________________________________________
void
RcuConfEdit::ParameterView::find_entry(int id, ParameterProxy*&   p) const
{
  Gtk::TreeModel::Row row;
  p = 0;
  if (find_row(id, row)) p = row[_columns._obj];
}
//____________________________________________________________________
void
RcuConfEdit::ParameterView::copy(const ParameterView& other)
{
  Gtk::TreeModel::Children c = other._tree_model->children();
  for (Gtk::TreeModel::Children::iterator i = c.begin();i != c.end(); ++i) {
    Gtk::TreeModel::Row   row     = *i;
    Gtk::TreeModel::Row   new_row = *(_tree_model->append());
    Gtk::copy_cell(_columns._id,    row, new_row);
    Gtk::copy_cell(_columns._name,  row, new_row);
    Gtk::copy_cell(_columns._where, row, new_row);
    Gtk::copy_cell(_columns._blob,  row, new_row);
    Gtk::copy_cell(_columns._mask,  row, new_row);
    Gtk::copy_cell(_columns._obj,   row, new_row);
  }
}
//____________________________________________________________________
void
RcuConfEdit::ParameterView::get_all(ParameterList& l) const
{
  Gtk::TreeModel::Children c = _tree_model->children();
  l.resize(c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    Gtk::TreeModel::Row r = c[i];
    ParameterProxy*     p = r[_columns._obj];
    l[i]                  = p;
  }
}
//____________________________________________________________________
void
RcuConfEdit::ParameterView::get_new(ParameterList& l) const
{
  l.clear();
  Gtk::TreeModel::Children c = _tree_model->children();
  size_t j = 0;
  for (size_t i = 0; i < c.size(); i++) { 
    Gtk::TreeModel::Row r = c[i];
    int id = r[_columns._id];
    if (id >= 0) continue;
    ParameterProxy* p = r[_columns._obj];
    l.push_back(p);
  }
}


//====================================================================
RcuConfEdit::ParameterDialog* RcuConfEdit::ParameterDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::ParameterDialog::ParameterDialog(ParameterHandler& h)
  : _handler(h),
    _name_label("Name"),
    _where_label("Destination"),
    _type_label("Value type"),
    _single_button(_type_group,"Single value"),
    _array_button(_type_group,"Array of values"),
    _type_vbox(false, 0),
    _mask_label("Mask"),
    _table(2, 2, false)
{
  _name_label.set_alignment(0,0.5);
  _name_label.set_padding(0,0);
  _name_label.set_justify(Gtk::JUSTIFY_LEFT);
  _name_label.set_line_wrap(false);
  _name_label.set_use_markup(false);
  _name_label.set_selectable(false);
  _name_entry.set_flags(Gtk::CAN_FOCUS);
  _name_entry.set_visibility(true);
  _name_entry.set_editable(true);
  _name_entry.set_max_length(0);
  _name_entry.set_text("");
  _name_entry.set_has_frame(true);
  _name_entry.set_activates_default(false);

  _where_label.set_alignment(0,0.5);
  _where_label.set_padding(0,0);
  _where_label.set_justify(Gtk::JUSTIFY_LEFT);
  _where_label.set_line_wrap(false);
  _where_label.set_use_markup(false);
  _where_label.set_selectable(false);
// #ifdef GTKMM_2_0_0
//   _where_combo.get_entry()->set_flags(Gtk::CAN_FOCUS);
//   _where_combo.get_entry()->set_visibility(true);
//   _where_combo.get_entry()->set_editable(true);
//   _where_combo.get_entry()->set_max_length(0);
//   _where_combo.get_entry()->set_text(ParameterHandler::where_to_string(Rcu));
//   _where_combo.get_entry()->set_has_frame(true);
//   _where_combo.get_entry()->set_activates_default(false);
//   _where_combo.set_case_sensitive(false);
//   const char * const items[] = { 
//     ParameterHandler::where_to_string(Rcu).c_str(),
//     ParameterHandler::where_to_string(Bc).c_str(),
//     ParameterHandler::where_to_string(Altro).c_str(),
//     0 };
//   _where_combo.set_popdown_strings(items);
// #else
  _where_combo.append_text(ParameterHandler::where_to_string(Rcu));
  _where_combo.append_text(ParameterHandler::where_to_string(Bc));
  _where_combo.append_text(ParameterHandler::where_to_string(Altro));
  // #endif

  _type_label.set_alignment(0,0.5);
  _type_label.set_padding(0,0);
  _type_label.set_justify(Gtk::JUSTIFY_LEFT);
  _type_label.set_line_wrap(false);
  _type_label.set_use_markup(false);
  _type_label.set_selectable(false);
  _single_button.set_flags(Gtk::CAN_FOCUS);
  _single_button.set_relief(Gtk::RELIEF_NORMAL);
  _single_button.set_mode(true);
  _single_button.set_active(false);
  _array_button.set_flags(Gtk::CAN_FOCUS);
  _array_button.set_relief(Gtk::RELIEF_NORMAL);
  _array_button.set_mode(true);
  _array_button.set_active(false);
  
  _type_vbox.pack_start(_single_button, Gtk::PACK_SHRINK, 0);
  _type_vbox.pack_start(_array_button, Gtk::PACK_SHRINK, 0);

  _mask_label.set_alignment(0,0.5);
  _mask_label.set_padding(0,0);
  _mask_label.set_justify(Gtk::JUSTIFY_LEFT);
  _mask_label.set_line_wrap(false);
  _mask_label.set_use_markup(false);
  _mask_label.set_selectable(false);
  _mask_value.set_flags(Gtk::CAN_FOCUS);

  
  Gtk::AttachOptions a1 = Gtk::EXPAND|Gtk::FILL;
  Gtk::AttachOptions a2 = Gtk::AttachOptions();

  _table.set_row_spacings(0);
  _table.set_col_spacings(6);
  _table.attach(_name_label,  0, 1, 0, 1, Gtk::FILL);
  _table.attach(_where_label, 0, 1, 1, 2, Gtk::FILL);
  _table.attach(_name_entry,  1, 2, 0, 1, a1, a2, 3);
  _table.attach(_where_combo, 1, 2, 1, 2, a1, a2, 3);
  _table.attach(_type_label,  0, 1, 2, 3, Gtk::FILL);
  _table.attach(_mask_label,  0, 1, 3, 4, Gtk::FILL);
  _table.attach(_type_vbox,   1, 2, 2, 3, Gtk::FILL, Gtk::FILL, 3, 0);
  _table.attach(_mask_value,  1, 2, 3, 4, a1, a2, 3);
  _inner_vbox.pack_start(_table);
}

//____________________________________________________________________
bool
RcuConfEdit::ParameterDialog::validate()
{
  return (!_name_entry.get_text().empty() && _mask_value.get_value() != 0);
}

//____________________________________________________________________
void
RcuConfEdit::ParameterDialog::set_destination(Where w) 
{
  // #ifndef GTKMM_2_0_0
  _where_combo.set_active(w);
  // #else
  // _where_combo.get_entry()->set_text(ParameterHandler::where_to_string(w));
  // #endif
}

//____________________________________________________________________
RcuConfEdit::Where
RcuConfEdit::ParameterDialog::get_destination() const
{
  // #ifndef GTKMM_2_0_0
  return ParameterHandler::string_to_where(_where_combo.get_active_text());
  // #else
  // return 
  // ParameterHandler::string_to_where(_where_combo.get_entry()->get_text());
  // #endif
}


//____________________________________________________________________
bool
RcuConfEdit::ParameterDialog::create(ParameterHandler& h, 
				     ParameterProxy*& p)
{
  // Make the instance 
  if (!_instance) _instance = new ParameterDialog(h);

  // Set fields 
  _instance->set_top("Parameter", (p ? p->id() : 0), "");
  _instance->_name_entry.set_text(p ? p->name() : "");
  _instance->set_destination(p ? p->where() : Rcu);
  _instance->_array_button.set_active(p ? p->blob() : false);
  _instance->_mask_value.set_value(p ? p->mask() : 0xFFFFFFFF);
  
  // Run the dialog 
  if (_instance->run_it() != OK) return false;

  // Get the fields 
  std::string nname  = _instance->_name_entry.get_text();
  Where       nwhere = _instance->get_destination();
  bool        nblob  = _instance->_array_button.get_active();
  int         nmask  = int(_instance->_mask_value.get_value());
  if (p && (nname  == p->name()   && 
	    nwhere == p->where() && 
	    nblob  == p->blob()  && 
	    nmask  == p->mask()))
    return false;
  
  // make new object.
  p = new ParameterProxy(-1, nname, nwhere, nblob, nmask);
  return true;
}

//____________________________________________________________________
//
// EOF
//
