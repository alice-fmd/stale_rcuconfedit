#include "Util.hh"

#include <pangomm/fontdescription.h>
#include <gtkmm/adjustment.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif

#include <sstream>
#include <iomanip>
#include <iostream>

//==================================================================
Gtk::HexEntry::HexEntry(unsigned int lower, unsigned int upper)
  : _lower(lower),
    _upper(upper),
    _w(10)
{
  _data = lower;
  modify_font(Pango::FontDescription("Monospace 10"));
  _w = std::max(int(log2(_upper)/4+1+2),3);
  set_width_chars(_w);
  set_max_length(_w);
}

//__________________________________________________________________
void
Gtk::HexEntry::on_changed()
{
  update_text(Entry::get_text());
}
#define IS_HEX_PREFIX(T) (T[0] == '0' && (T[1] == 'x' || T[1] == 'X'))
//__________________________________________________________________
void
Gtk::HexEntry::update_text(const Glib::ustring& str)
{
  if (str.empty() || (str.size() == 2 && IS_HEX_PREFIX(str))) {
    _data = _lower;
    return;
  }
  unsigned int i = 0;
  Glib::ustring t(str);
  if (t[0] == '0' && IS_HEX_PREFIX(t)) i += 2;
  for (;i < t.length(); i++) if (!isxdigit(t[i])) t.erase(i,1);
  std::stringstream s(t);
  i = 0;
  s >> std::hex >> i;
  set_value(i);
  // Entry::set_text(t);
}

//__________________________________________________________________
void
Gtk::HexEntry::set_value(unsigned int v)
{
  //_data = std::max(_lower,std::min(_upper,v));
  std::stringstream s; 
  s << "0x" << std::hex << std::setfill('0') << v;
  Entry::set_text(s.str());
}

//==================================================================
Gtk::HexSpinButton::HexSpinButton(int climb_rate)
  : SpinButton(climb_rate,0)
{
  modify_font(Pango::FontDescription("Monospace 10"));
  set_range(0, UINT_MAX);
  set_increments(1,0x10);
  SpinButton::set_numeric(false);
}
  
//__________________________________________________________________
Gtk::HexSpinButton::HexSpinButton(Adjustment& adj, int climb_rate)
  : SpinButton(adj, climb_rate, 0) 
{
  modify_font(Pango::FontDescription("Monospace 10"));
  set_adjustment(adj);
  SpinButton::set_numeric(false);
}

//__________________________________________________________________
void 
Gtk::HexSpinButton::get_range(unsigned int& lower, unsigned int& upper) const
{
  double min, max;
  SpinButton::get_range(min,max);
  lower = (unsigned int)min;
  upper = (unsigned int)max;
}

//__________________________________________________________________
unsigned int
Gtk::HexSpinButton::get_upper() const
{
  unsigned int ret, dummy;
  get_range(dummy, ret);
  return ret;
}

//__________________________________________________________________
void 
Gtk::HexSpinButton::update_max_length()
{
  int w = std::max(int(log2(get_upper())/4+1+2),3);
  set_width_chars(w);
  set_max_length(w);
}
//__________________________________________________________________
void 
Gtk::HexSpinButton::set_adjustment(Adjustment& adj)
{
  guint imax = std::min((guint)(adj.get_upper()),UINT_MAX);
  guint imin = std::max((guint)(adj.get_lower()),0u);
  adj.set_lower(imin);
  adj.set_upper(imax);
  update_max_length();
}

//__________________________________________________________________
void 
Gtk::HexSpinButton::set_range(unsigned int lower, unsigned int upper) 
{
  SpinButton::set_range(lower,upper);
  get_range(lower, upper);
  update_max_length();
}
//__________________________________________________________________
void 
Gtk::HexSpinButton::set_increments(unsigned int step, unsigned int page) 
{
  SpinButton::set_increments(step,page);
}
//__________________________________________________________________
void 
Gtk::HexSpinButton::set_value(unsigned int v) 
{
  SpinButton::set_value(v);
}
//__________________________________________________________________
unsigned int
Gtk::HexSpinButton::get_value() const
{
  return (guint)SpinButton::get_value();
}
//__________________________________________________________________
int 
Gtk::HexSpinButton::on_input(double* value) 
{
  Glib::ustring t = get_text();
  if (t.empty()) {
    *value = 0;
    return true;
  }
  unsigned int lower, upper, hex;
  std::stringstream s(t);
  s >> std::hex >> hex;
  get_range(lower, upper);
  *value = std::max(std::min(hex,upper), lower);
  return true;
}
//__________________________________________________________________
bool
Gtk::HexSpinButton::on_output() 
{
  guint val = (guint)(get_adjustment()->get_value());
  std::stringstream s;
  s << "0x" << std::hex << val;
  set_text(s.str());
  return true;
}
//__________________________________________________________________
void
Gtk::HexSpinButton::on_changed() 
{
  Glib::ustring t(get_text());
  size_t i = 0;
  if (t[0] == '0' && (t[1] == 'x' || t[1] == 'X')) i += 2;
  for (; i < t.size(); i++) if (!isxdigit(t[i])) t.erase(i,1);
  set_text(t);
}

//==================================================================
Gtk::CellRendererHex::CellRendererHex(unsigned int lower, 
				      unsigned int upper)
  : _lower(lower), _upper(upper), _n(-1)
{
  _w = std::max(int(log2(_upper)/4+1+2),3);
  property_font() = "Monospace";
}

//____________________________________________________________________
Gtk::CellEditable*
Gtk::CellRendererHex::start_editing_vfunc(GdkEvent*             event, 
					  Widget&               widget, 
					  const Glib::ustring&  path, 
					  const Gdk::Rectangle& bg_area, 
					  const Gdk::Rectangle& cell, 
					  CellRendererState     flags)
{
  if (!property_editable()) { return 0; }
  
  // Get the text, and transform to unsigned int
  Glib::ustring str = property_text();
  std::stringstream s(str);
  unsigned int v;
  s >> std::hex >> v;

  // Make our entry 
  HexEntry*   e = manage(new HexEntry(_lower,_upper));
  e->set_value(v);
  e->set_has_frame(false);
  e->select_region(0,-1); // What's this?
  e->show();
  e->signal_editing_done()
    .connect(bind(SigC::slot(*this,&CellRendererHex::on_edited),e,path));
  return e;
}

//__________________________________________________________________
void
Gtk::CellRendererHex::on_edited(Entry* entry, Glib::ustring path)
{
  // Get the entry 
  HexEntry* e = (HexEntry*)(entry);
  if (!e) return;
  
  // Get the old and new value 
  Glib::ustring old_val = property_text();
  Glib::ustring new_val = e->get_text();
  
  if (old_val == new_val) return;

  // Singal that we're done 
  edited(path,new_val);
}

//__________________________________________________________________
bool
Gtk::CellRendererHex::do_append(TreeView& tree, 
				const Glib::ustring& name,
				const TreeModelColumnBase& field) 
{
  // Append the column 
  _n                           = tree.append_column(name, *this)-1;
  Gtk::TreeViewColumn* column  = tree.get_column(_n);
  // Set attributes, and connect our slot, binding the first argument
  // to the column number 
  // column->add_attribute(property_text(), field);
  column->set_cell_data_func(*this, 
			     SigC::slot(*this,&CellRendererHex::on_render));
  return true;
}


//__________________________________________________________________
void
Gtk::CellRendererHex::on_render(CellRenderer*, const TreeModel::iterator& i) 
{
  // Get the row and the valie 
  TreeModel::Row row = *i;
  int v; 
  row.get_value(_n,v);
  if (v < 0) {
    property_text() = "";
    return;
  }
  // Transform the value into text. 
  std::stringstream s;
  s << "0x" << std::hex << std::setfill('0') << std::setw(_w-2) << v;
  property_text() = s.str(); 
}

//==================================================================
Gtk::HexView::HexView()
{ 
  // Make the view 
  _view = Gtk::manage(new TreeView());
  _view->modify_font(Pango::FontDescription("Monospace 10"));

  // Handle tree selections
  _tree_selection = _view->get_selection();
  _tree_selection->signal_changed().
    connect(SigC::slot(*this, &HexView::on_selection));

  // Make the model 
  _tree_model = ListStore::create(_columns);
  _view->set_model(_tree_model);

  // Append a column 
#ifndef GTKMM_2_0_0
  _view->append_column_numeric("#", _columns._idx, "%4d");
  _view->append_column_numeric_editable("Value", _columns._number, "0x%08x");
#else
  _view->append_column("#", _columns._idx);
  _view->append_column_editable("Value", _columns._number);
  TreeView::Column* column = _view->get_column(1); // _columns._number);
  CellRenderer*     render = _view->get_column_cell_renderer(1);
  column->set_cell_data_func(*render,SigC::slot(*this,&HexView::render_hex));  
#endif
  add(*_view);

  // Get the cell renderer so we can attach a handler to it. 
  CellRenderer*      bare_cr = _view->get_column_cell_renderer(0);
  CellRendererText*  text_cr = dynamic_cast<CellRendererText*>(bare_cr);
  if (!text_cr) return;
  text_cr->signal_edited().connect(SigC::slot(*this, &HexView::on_change));
}

//__________________________________________________________________
void 
Gtk::HexView::do_insert(Gtk::TreeModel::iterator i, const unsigned int& value)
{
  int n;
  std::stringstream s(_tree_model->get_string(i));
  s >> n;
  (*i)[_columns._idx]    = n;
  (*i)[_columns._number] = value;
  i++;
  for (; i != _tree_model->children().end(); ++i) (*i)[_columns._idx] = ++n;
}

//__________________________________________________________________
void 
Gtk::HexView::append_data(const unsigned int& value)
{
  do_insert(_tree_model->append(), value);
}  

//__________________________________________________________________
void 
Gtk::HexView::prepend_data(const unsigned int& value)
{
  do_insert(_tree_model->prepend(), value);
}  

//__________________________________________________________________
void 
Gtk::HexView::insert_data_before_selected(const unsigned int& value)
{
  TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) {
    prepend_data(value);
    return;
  }
  do_insert(_tree_model->insert(iter), value);
}  

//__________________________________________________________________
void 
Gtk::HexView::insert_data_after_selected(const unsigned int& value)
{
  TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) {
    append_data(value);
    return;
  }
#ifndef GTKMM_2_0_0
  do_insert(_tree_model->insert_after(iter), value);
#else
  TreeModel::iterator keep = iter;
  if (++iter == _tree_model->children().end()) iter = keep;
  do_insert(_tree_model->insert(iter), value);
#endif  
}  

//__________________________________________________________________
void 
Gtk::HexView::remove_selected()
{
  TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) return;
  TreeModel::iterator next = iter;
  next++;
  int n;
  std::stringstream s(_tree_model->get_string(iter));
  s >> n;
  _tree_model->erase(iter);
  for (; next != _tree_model->children().end(); ++next) 
    (*next)[_columns._idx] = n++;
}  

//__________________________________________________________________
void 
Gtk::HexView::move_selected_up()
{
  TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter)  return;
  TreeModel::Children::iterator top = _tree_model->children().begin();
  if (iter == top)  return;
#ifndef GTKMM_2_0_0
  TreeModel::iterator up   = iter;
  up--;
#else
  TreeModel::iterator up   = top;
  TreeModel::iterator i    = top;
  while ((i != iter && i != _tree_model->children().end())) {
    up = i;
    i++;
  }
#endif  
  _tree_model->move(iter,up);
  (*iter)[_columns._idx] = (*iter)[_columns._idx] - 1;
  (*up)[_columns._idx]   = (*up)[_columns._idx] + 1;
}  

//__________________________________________________________________
void 
Gtk::HexView::move_selected_down()
{
  TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) return;
#ifndef GTKMM_2_0_0
  TreeModel::Children::iterator bottom=_tree_model->children().end();
  bottom--;
#else
  TreeModel::iterator bottom = _tree_model->children().begin();
  TreeModel::iterator i      = bottom;
  while ((i != _tree_model->children().end())) {
    bottom = i;
    i++;
  }
#endif  
  if (iter.equal(bottom)) return;
  TreeModel::iterator down   = iter++;
  _tree_model->move(iter,down);
  (*iter)[_columns._idx] = (*iter)[_columns._idx] - 1;
  (*down)[_columns._idx] = (*down)[_columns._idx] + 1;
}  

//__________________________________________________________________
void 
Gtk::HexView::set_data(const std::vector<unsigned int>& values) 
{
  _tree_model->clear();
  for (std::vector<unsigned int>::const_iterator i = values.begin(); 
       i != values.end(); ++i) 
    append_data(*i);
}

//__________________________________________________________________
void 
Gtk::HexView::get_data(std::vector<unsigned int>& values) const 
{
  TreeModel::Children children = _tree_model->children();
  values.resize(children.size());
  size_t j = 0;
  for(TreeModel::Children::const_iterator i = children.begin(); 
      i != children.end(); ++i, j++) {
    TreeModel::Row row = *i;
    values[j] = row[_columns._number];
  }
}
//__________________________________________________________________
void 
Gtk::HexView::on_selection()
{
#if 0
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  std::cout << "Selected " << row[_columns._number] << std::endl;
#endif
}

//__________________________________________________________________
void 
Gtk::HexView::on_change(const Glib::ustring& path, const Glib::ustring& s)
{
  TreeModel::iterator i = _tree_model->get_iter(path);
  if (!i) return;
  // TreeView::Row row = *iter;
}
//__________________________________________________________________
void 
Gtk::HexView::render_hex(CellRenderer* r, const TreeModel::iterator& i)
{
  TreeModel::Row     row = *i;
  unsigned int       v   = row[_columns._number];
  CellRendererText*  rt  = (CellRendererText*)(r);
  std::stringstream s;
  s << "0x" << std::hex << std::setfill('0') << std::setw(8) << v;
  rt->property_text() = s.str(); 
}

//==================================================================
Gtk::HLabeledSpinButton::HLabeledSpinButton(const Glib::ustring label, 
					    unsigned int lower, 
					    unsigned int upper,
					    unsigned int step, 
					    unsigned int page)
  : _adjustment(lower, lower, upper, step, page), 
    _label(label), 
    _value(_adjustment)
{
  _value.set_numeric(true);
  pack_start(_label, Gtk::SHRINK, 3);
  pack_start(_value, Gtk::EXPAND|Gtk::FILL, 3);
  _label.show();
  _value.show();
}
//__________________________________________________________________
unsigned int
Gtk::HLabeledSpinButton::get_value() const 
{
  return (unsigned int)_value.get_value();
}
//__________________________________________________________________
void
Gtk::HLabeledSpinButton::set_value(unsigned int v)
{
  _value.set_value(v);
}
//==================================================================
Gtk::VLabeledSpinButton::VLabeledSpinButton(const Glib::ustring label, 
					    unsigned int lower, 
					    unsigned int upper,
					    unsigned int step, 
					    unsigned int page)
  : _adjustment(lower, lower, upper, step, page), 
    _label(label), 
    _value(_adjustment)
{
  _value.set_numeric(true);
  pack_start(_label, Gtk::SHRINK, 3);
  pack_start(_value, Gtk::EXPAND|Gtk::FILL, 3);
  _label.show();
  _value.show();
}
//__________________________________________________________________
unsigned int
Gtk::VLabeledSpinButton::get_value() const 
{
  return (unsigned int)_value.get_value();
}
//__________________________________________________________________
void
Gtk::VLabeledSpinButton::set_value(unsigned int v)
{
  _value.set_value(v);
}


#ifdef GTKMM_2_0_0
//==================================================================
Gtk::ComboBoxText::ComboBoxText()
  : Combo()
{}
void
Gtk::ComboBoxText::append_text(const Glib::ustring& s)
{
  _text.push_back(s);
}
//__________________________________________________________________
void
Gtk::ComboBoxText::on_show()
{
  set_popdown_strings(_text);
  Combo::on_show();
}
//__________________________________________________________________
void
Gtk::ComboBoxText::set_active(int n)
{
  if (n >= _text.size() || n < 0) return;
  get_entry()->set_text(_text[n]);
}
//__________________________________________________________________
void
Gtk::ComboBoxText::set_active_text(const Glib::ustring& t)
{
  size_t i = 0;
  for (; i < _text.size(); i++)  if (t == _text[i]) break;
  if (i >= _text.size()) return;
  get_entry()->set_text(_text[i]);
}
//__________________________________________________________________
const Glib::ustring&
Gtk::ComboBoxText::get_active_text() const
{
  Glib::ustring t = get_entry()->get_text();
  size_t i = 0;
  for (; i < _text.size(); i++)  if (t == _text[i]) break;
  if (i >= _text.size()) return _text[0];
  return _text[i];
}
//__________________________________________________________________
int
Gtk::ComboBoxText::get_active_row_number() const
{
  Glib::ustring t = get_entry()->get_text();
  for (size_t i = 0; i < _text.size(); i++)
    if (t == _text[i]) return i;
  return -1;
}
#endif

//__________________________________________________________________
//
// EOF
//
