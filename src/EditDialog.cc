#include "config.h"
#include "EditDialog.hh"
#include "ErrorDialog.hh"

//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
#include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/buttonbox.h>

#include <sstream>
#include <iostream>

//____________________________________________________________________
RcuConfEdit::EditDialog::EditDialog() 
  : _inner_vbox(false, 0), 
    _alignment(0.5, 0.5, 1, 1),
    _title("EditDialog"),
    _ok_button(Gtk::StockID("gtk-ok")),
    _cancel_button(Gtk::StockID("gtk-cancel")),
    _help_button(Gtk::StockID("gtk-help"))
{
  // Make the alignment
  _alignment.add(_inner_vbox);
  _alignment.set_border_width(3);
  
  // Make the title 
  _title.set_alignment(0.5,0.5);
  _title.set_padding(0,0);
  _title.set_justify(Gtk::JUSTIFY_LEFT);
  _title.set_line_wrap(false);
  _title.set_use_markup(true);
  _title.set_selectable(false);
  
  // Make frame 
  _frame.set_shadow_type(Gtk::SHADOW_NONE);
  _frame.set_label_align(0,0.5);
  _frame.add(_alignment);
  _frame.set_label_widget(_title);
  
  // Add the frame 
  get_vbox()->set_homogeneous(false);
  get_vbox()->set_spacing(0);
  get_vbox()->pack_start(_frame, true,true, 3);
  
  // Set flags, etc.
  set_title("Editor...");
  set_modal(false);
  property_window_position().set_value(Gtk::WIN_POS_NONE);
  set_resizable(true);
  property_destroy_with_parent().set_value(false);
  set_has_separator(true);

  // Make buttons
  get_action_area()->property_layout_style().set_value(Gtk::BUTTONBOX_END);

  _ok_button.set_flags(Gtk::CAN_FOCUS);
  _ok_button.set_flags(Gtk::CAN_DEFAULT);
  _ok_button.set_relief(Gtk::RELIEF_NORMAL);
  _ok_button.signal_clicked()
    .connect(SigC::slot(*this, &EditDialog::on_ok), true);
  _ok_button.show();
  add_action_widget(_ok_button, -OK);
  
  _cancel_button.set_flags(Gtk::CAN_FOCUS);
  _cancel_button.set_flags(Gtk::CAN_DEFAULT);
  _cancel_button.set_relief(Gtk::RELIEF_NORMAL);
  _cancel_button.signal_clicked()
    .connect(SigC::slot(*this, &EditDialog::on_cancel), false);
  _cancel_button.show();
  add_action_widget(_cancel_button, -Cancel);
  
  _help_button.set_flags(Gtk::CAN_FOCUS);
  _help_button.set_flags(Gtk::CAN_DEFAULT);
  _help_button.set_relief(Gtk::RELIEF_NORMAL);
  _help_button.signal_clicked()
    .connect(SigC::slot(*this, &EditDialog::on_help), false);
  _help_button.show();
  add_action_widget(_help_button, -Help);

  resize(400,300);
}

//____________________________________________________________________
Gtk::Frame*
RcuConfEdit::EditDialog::make_frame(const std::string& title,
				    Gtk::Widget& widget) 
{
  // Make the alignment
  Gtk::Alignment* align = Gtk::manage(new Gtk::Alignment(0.5, 0.5, 1, 1));
  align->add(widget);
  
  // Make the title 
  Gtk::Label* label     = Gtk::manage(new Gtk::Label(title.c_str()));
  label->set_alignment(0.5,0.5);
  label->set_padding(0,0);
  label->set_justify(Gtk::JUSTIFY_LEFT);
  label->set_line_wrap(false);
  label->set_use_markup(true);
  label->set_selectable(false);
  
  // Make frame 
  Gtk::Frame* frame     = Gtk::manage(new Gtk::Frame());
  frame->set_shadow_type(Gtk::SHADOW_NONE);
  frame->set_label_align(0,0.5);
  frame->add(*align);
  frame->set_label_widget(*label);

  widget.show();
  align->show();
  label->show();
  frame->show();

  return frame;
}

//____________________________________________________________________
int
RcuConfEdit::EditDialog::run_it()
{
  show_all();
  int response;
  do { 
    response = run();
  } while (response != -OK && response != -Cancel);
  hide_all();
  return -response;
}

//____________________________________________________________________
void
RcuConfEdit::EditDialog::stop_it()
{
  hide_all();
}

//____________________________________________________________________
void
RcuConfEdit::EditDialog::set_top(const std::string& type, 
				 int                id, 
				 const std::string& extra)
{
  std::stringstream s;
  s << type << " (Id # " << id << (extra.size() > 0 ? ", " : "")
    << extra << ")";
  _title.set_label(s.str().c_str());
  std::stringstream t;
  t << type << " editor ...";
  set_title(t.str().c_str());
}

//____________________________________________________________________
void
RcuConfEdit::EditDialog::on_ok()
{
  // The user class should validate the dialog 
  //std::cout << __PRETTY_FUNCTION__ << ": User pressed ok" << std::endl;
  if (!validate()) {
    ErrorDialog::run_it("Invalid input", 
			"The values input for the object is invalid");
    // std::cout << "Entries didn't validate!" << std::endl;
    response(-Cancel);
    return;
  }
}

//____________________________________________________________________
void
RcuConfEdit::EditDialog::on_cancel()
{
}

//____________________________________________________________________
void
RcuConfEdit::EditDialog::on_help()
{
}
//____________________________________________________________________
//
// EOF
//
