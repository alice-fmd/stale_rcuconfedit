//
// 
//
#include "config.h"
#include "Config.hh"
#include "ErrorDialog.hh"
#include "Priority.hh"
#include "PickDialog.hh"
#include "Util.hh"

//#include <libgnome/libgnome.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gtkmmconfig.h>
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/alignment.h>
#include <gtkmm/frame.h>
#include <gtkmm/textbuffer.h>

#include <rcuconf/Config.h>
#include <rcuconf/Priority.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>

#include <sstream>
#include <iomanip>
#include <iostream>

//====================================================================
RcuConfEdit::ConfigProxy::~ConfigProxy()
{
  if (_obj) delete _obj;
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::id() const
{
  return (_obj ? _obj->Id() : _id);
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::tag() const
{
  return (_obj ? _obj->Tag() : _tag);
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::x() const
{
  return (_obj ? _obj->X() : _x);
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::y() const
{
  return (_obj ? _obj->Y() : _y);
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::z() const
{
  return (_obj ? _obj->Z() : _z);
}
//____________________________________________________________________
int
RcuConfEdit::ConfigProxy::version() const
{
  return (_obj ? _obj->Version() : _version);
}
//____________________________________________________________________
const std::string& 
RcuConfEdit::ConfigProxy::description() const
{
  return (_obj ? _obj->Description() : _description);
}
//____________________________________________________________________
RcuConf::Config*
RcuConfEdit::ConfigProxy::obj()
{
  if (_obj) return _obj;
  return _obj = new RcuConf::Config(_tag, _x, _y, _z, _priority->id(), 
				    _description);
}


//====================================================================
bool
RcuConfEdit::ConfigHandler::commit()
{
  if (!_server) return false;

  ConfigList l;
  _view.get_new(l);
  for (ConfigList::iterator i = l.begin(); i != l.end(); ++i) {
    if (!(*i)->obj()->Insert(*_server)) {
      ErrorDialog::run_it("Failed to insert Config into database",
                          _server->ErrorString());
      return false;
    }
  }
  return true;    
}
//____________________________________________________________________
bool
RcuConfEdit::ConfigHandler::update()
{
  if (!_server) return false;
  
  RcuConf::Config::List l;
  if (!RcuConf::Config::Select(l, *_server, "")) {
    ErrorDialog::run_it("Error updating config values",_server->ErrorString());
    return false;
  }
  for (RcuConf::Config::List::iterator i = l.begin(); i != l.end(); ++i) {
    PriorityProxy* p = _priority_handler.find((*i)->PriorityId());
    if (!p) {
      std::cerr << (*i)->Description() << " has invalid priority " 
		<< (*i)->PriorityId() << std::endl;
      continue;
   } 
    _view.add_entry(new ConfigProxy(*i,p));
  }
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::ConfigHandler::edit(bool copy)
{
  ConfigProxy* p = 0;
  if (copy) _view.get_entry(p);
  
  bool ret = ConfigDialog::create(*this, p);
  if (!ret || !p) return ret;
  
  _view.add_entry(p);
  return ret;
}

//____________________________________________________________________
bool
RcuConfEdit::ConfigHandler::pick(int& id) const
{
  ConfigView v;
  v.copy(_view);
  return PickDialog::run_it("Config", v, id);
}

//____________________________________________________________________
RcuConfEdit::ConfigProxy*
RcuConfEdit::ConfigHandler::find(int config) const
{
  ConfigProxy* p = 0;
  _view.find_entry(config, p);
  return p;
}

//____________________________________________________________________
bool
RcuConfEdit::ConfigHandler::pick_priority(int& ret) const
{
  return _priority_handler.pick(ret);
}
//____________________________________________________________________
RcuConfEdit::PriorityProxy*
RcuConfEdit::ConfigHandler::find_priority(int ret) const
{
  return _priority_handler.find(ret);
}

//====================================================================
RcuConfEdit::ConfigView::ConfigView() 
{  
  setup(_columns, true);
  //Add the TreeView's view columns:
  _view.append_column("Tag", _columns._tag);
  _view.append_column("X",   _columns._x);
  _view.append_column("Y",   _columns._y);
  _view.append_column("Z",   _columns._z);
  _view.append_column("Priority",    _columns._priority);
  _view.append_column("Version",_columns._version);
  _view.append_column("Description", _columns._description);
  setup_sort(_columns);
}

//____________________________________________________________________
void
RcuConfEdit::ConfigView::on_selection()
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  // ConfigProxy* p  = row[_columns._obj];
  // p->Print();
}

//____________________________________________________________________
void
RcuConfEdit::ConfigView::add_entry(ConfigProxy* p)
{
  if (!p) return;
  Gtk::TreeModel::Row row;
  add_row(p->id(), row);
  p->set_id(row[_columns._id]);
  std::stringstream s;
  std::string pn = p->priority()->description();
  s << (pn.size() > 20 ? pn.substr(0,20) : pn)
    << (pn.size() > 20 ? "..." : "");
  row[_columns._tag]         = p->tag();
  row[_columns._x]           = p->x();
  row[_columns._y]           = p->y();
  row[_columns._z]           = p->z();
  row[_columns._priority]    = s.str();
  row[_columns._version]     = p->version();
  row[_columns._description] = p->description();
  row[_columns._obj]         = p;
  row[_columns._prior_obj]   = p->priority();
}

//____________________________________________________________________
void
RcuConfEdit::ConfigView::get_entry(ConfigProxy*&   p) const
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  p  = row[_columns._obj];
}
//____________________________________________________________________
void
RcuConfEdit::ConfigView::delete_entry(Gtk::TreeModel::Row& row)
{
  ConfigProxy* p = row[_columns._obj];
  if (!p) return;
  delete p;
}
//____________________________________________________________________
void
RcuConfEdit::ConfigView::find_entry(int id, ConfigProxy*& p) const
{
  Gtk::TreeModel::Row row;
  if (!find_row(id, row)) return;
  p = row[_columns._obj];
}

//____________________________________________________________________
void
RcuConfEdit::ConfigView::copy(const ConfigView& other)
{
  Gtk::TreeModel::Children children = other._tree_model->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin();
       i != children.end(); ++i) {
    Gtk::TreeModel::Row row     = *i;
    Gtk::TreeModel::Row new_row = *(_tree_model->append());
    Gtk::copy_cell(_columns._id,          row, new_row);
    Gtk::copy_cell(_columns._tag,         row, new_row);
    Gtk::copy_cell(_columns._x,           row, new_row);
    Gtk::copy_cell(_columns._y,           row, new_row);
    Gtk::copy_cell(_columns._z,           row, new_row);
    Gtk::copy_cell(_columns._priority,    row, new_row);
    Gtk::copy_cell(_columns._version,     row, new_row);
    Gtk::copy_cell(_columns._description, row, new_row);
    Gtk::copy_cell(_columns._obj,         row, new_row);
  }
}
//____________________________________________________________________
void
RcuConfEdit::ConfigView::get_all(ConfigList& l) const
{
  Gtk::TreeModel::Children c = _tree_model->children();
  l.resize(c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    Gtk::TreeModel::Row r = c[i];
    ConfigProxy*    p = r[_columns._obj];
    l[i]                  = p;
  }
}
//____________________________________________________________________
void
RcuConfEdit::ConfigView::get_new(ConfigList& l) const
{
  l.clear();
  Gtk::TreeModel::Children c = _tree_model->children();
  size_t j = 0;
  for (size_t i = 0; i < c.size(); i++) { 
    Gtk::TreeModel::Row r = c[i];
    int id = r[_columns._id];
    if (id >= 0) continue;
    ConfigProxy* p = r[_columns._obj];
    l.push_back(p);
  }
}

//====================================================================
RcuConfEdit::ConfigDialog* RcuConfEdit::ConfigDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::ConfigDialog::ConfigDialog(ConfigHandler& h)
  : _handler(h),
    _tag_label("Tag "),
    _tag_adjustment(1, 0, 100, 1, 10, 10),
    _tag_spin(_tag_adjustment, 1, 0),
    _priority_label("Priority"),
    _priority_browse("Select..."),
    _tagprior_hbox(false, 0),
    _x_label("X"),
    _x_adjustment(1, 0, 100, 1, 10, 10),
    _x_spin(_x_adjustment, 1, 0),
    _y_label("Y"),
    _y_adjustment(1, 0, 100, 1, 10, 10),
    _y_spin(_y_adjustment, 1, 0),
    _z_label("Z"),
    _z_adjustment(1, 0, 100, 1, 10, 10),
    _z_spin(_z_adjustment, 1, 0),
    _xyz_hbox(false, 0)
{
  _tag_label.set_alignment(0.5,0.5);
  _tag_label.set_padding(0,0);
  _tag_label.set_justify(Gtk::JUSTIFY_LEFT);
  _tag_label.set_line_wrap(false);
  _tag_label.set_use_markup(false);
  _tag_label.set_selectable(false);
  _tag_label.show();
  _tag_spin.set_flags(Gtk::CAN_FOCUS);
  _tag_spin.set_update_policy(Gtk::UPDATE_ALWAYS);
  _tag_spin.set_numeric(true);
  _tag_spin.set_digits(0);
  _tag_spin.set_wrap(false);
  _tag_spin.show();

  _priority_label.set_alignment(0.5,0.5);
  _priority_label.set_padding(0,0);
  _priority_label.set_justify(Gtk::JUSTIFY_LEFT);
  _priority_label.set_line_wrap(false);
  _priority_label.set_use_markup(false);
  _priority_label.set_selectable(false);
  _priority_label.show();
  _priority_entry.set_flags(Gtk::CAN_FOCUS);
  _priority_entry.set_visibility(true);
  _priority_entry.set_editable(false);
  _priority_entry.set_max_length(0);
  _priority_entry.set_text("");
  _priority_entry.set_has_frame(true);
  _priority_entry.set_activates_default(false);
  _priority_entry.show();
  _priority_browse.set_flags(Gtk::CAN_FOCUS);
  _priority_browse.set_relief(Gtk::RELIEF_NORMAL);
  _priority_browse.signal_clicked()
    .connect(SigC::slot(*this, &ConfigDialog::on_browse), false);
  _priority_browse.show();
  _tooltips.set_tip(_priority_browse, 
		    "Select parameter priority from known priorities", "");

  _tagprior_hbox.pack_start(_tag_label, Gtk::PACK_SHRINK, 0);
  _tagprior_hbox.pack_start(_tag_spin, Gtk::PACK_EXPAND_WIDGET, 3);
  _tagprior_hbox.pack_start(_priority_label, Gtk::PACK_SHRINK, 3);
  _tagprior_hbox.pack_start(_priority_entry);
  _tagprior_hbox.pack_start(_priority_browse, Gtk::PACK_SHRINK, 3);
  _tagprior_frame     = make_frame("Tag/Priority", _tagprior_hbox);

  _x_label.set_alignment(0.5,0.5);
  _x_label.set_padding(0,0);
  _x_label.set_justify(Gtk::JUSTIFY_LEFT);
  _x_label.set_line_wrap(false);
  _x_label.set_use_markup(false);
  _x_label.set_selectable(false);
  _x_label.show();
  _x_spin.set_flags(Gtk::CAN_FOCUS);
  _x_spin.set_update_policy(Gtk::UPDATE_IF_VALID);
  _x_spin.set_numeric(false);
  _x_spin.set_digits(0);
  _x_spin.set_wrap(false);
  _x_spin.show();
  _y_label.set_alignment(0.5,0.5);
  _y_label.set_padding(0,0);
  _y_label.set_justify(Gtk::JUSTIFY_LEFT);
  _y_label.set_line_wrap(false);
  _y_label.set_use_markup(false);
  _y_label.set_selectable(false);
  _y_label.show();
  _y_spin.set_flags(Gtk::CAN_FOCUS);
  _y_spin.set_update_policy(Gtk::UPDATE_IF_VALID);
  _y_spin.set_numeric(false);
  _y_spin.set_digits(0);
  _y_spin.set_wrap(false);
  _y_spin.show();
  _z_label.set_alignment(0.5,0.5);
  _z_label.set_padding(0,0);
  _z_label.set_justify(Gtk::JUSTIFY_LEFT);
  _z_label.set_line_wrap(false);
  _z_label.set_use_markup(false);
  _z_label.set_selectable(false);
  _z_label.show();
  _z_spin.set_flags(Gtk::CAN_FOCUS);
  _z_spin.set_update_policy(Gtk::UPDATE_IF_VALID);
  _z_spin.set_numeric(false);
  _z_spin.set_digits(0);
  _z_spin.set_wrap(false);
  _z_spin.show();
  _xyz_hbox.pack_start(_x_label, Gtk::PACK_SHRINK, 0);
  _xyz_hbox.pack_start(_x_spin, Gtk::PACK_EXPAND_WIDGET, 3);
  _xyz_hbox.pack_start(_y_label, Gtk::PACK_SHRINK, 0);
  _xyz_hbox.pack_start(_y_spin, Gtk::PACK_EXPAND_WIDGET, 3);
  _xyz_hbox.pack_start(_z_label, Gtk::PACK_SHRINK, 0);
  _xyz_hbox.pack_start(_z_spin, Gtk::PACK_EXPAND_WIDGET, 3);
  _xyz_frame    = make_frame("Coordinates", _xyz_hbox);


  _description_entry.set_flags(Gtk::CAN_FOCUS);
  _description_entry.set_editable(true);
  _description_entry.set_cursor_visible(true);
  _description_entry.set_pixels_above_lines(0);
  _description_entry.set_pixels_below_lines(0);
  _description_entry.set_pixels_inside_wrap(0);
  _description_entry.set_left_margin(0);
  _description_entry.set_right_margin(0);
  _description_entry.set_indent(0);
  _description_entry.set_wrap_mode(Gtk::WRAP_NONE);
  _description_entry.set_justification(Gtk::JUSTIFY_LEFT);
  _description_entry.show();
  _description_scroll.set_flags(Gtk::CAN_FOCUS);
  _description_scroll.set_shadow_type(Gtk::SHADOW_IN);
  _description_scroll.set_policy(Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS);
  _description_scroll.property_window_placement()
    .set_value(Gtk::CORNER_TOP_LEFT);
  _description_scroll.add(_description_entry);
  // _description_scroll.show();
  _description_frame  = make_frame("Description", _description_scroll);

  _inner_vbox.pack_start(*_tagprior_frame);
  _inner_vbox.pack_start(*_xyz_frame);
  _inner_vbox.pack_start(*_description_frame);
}

//____________________________________________________________________
int
RcuConfEdit::ConfigDialog::get_priority()
{
  std::stringstream np(_priority_entry.get_text());
  int nprior;
  np >> nprior;
  return nprior;
}

//____________________________________________________________________
void
RcuConfEdit::ConfigDialog::set_priority(int id)
{
  std::stringstream np;
  np << id;
  _priority_entry.set_text(np.str());
}


//____________________________________________________________________
void
RcuConfEdit::ConfigDialog::on_browse()
{
  std::cout << "Sorry! not implemented yet" << std::endl;
  int ret = get_priority();
  if (_handler.pick_priority(ret)) set_priority(ret);
}

//____________________________________________________________________
bool
RcuConfEdit::ConfigDialog::validate()
{
  return get_priority() != 0;
}

//____________________________________________________________________
bool
RcuConfEdit::ConfigDialog::create(ConfigHandler& h, 
				  ConfigProxy*&   p)
{
  if (!_instance) _instance = new ConfigDialog(h);
  std::stringstream sv; sv << "version " << (p ? p->version() : -1);
  Glib::RefPtr<Gtk::TextBuffer> buffer = Gtk::TextBuffer::create();
  buffer->set_text(p ? p->description() : "");

  _instance->set_top("Configuration", (p ? p->id() : 0), sv.str());
  _instance->set_priority(p ? p->priority()->id() : 0);
  _instance->_tag_spin.set_value(p ? p->tag() : 0);
  _instance->_x_spin.set_value(p ? p->x() : 0);
  _instance->_y_spin.set_value(p ? p->y() : 0);
  _instance->_z_spin.set_value(p ? p->z() : 0);
  _instance->_description_entry.set_buffer(buffer);
  
  // Now show the dialog 
  if (_instance->run_it() != OK) return false;

  // Now, get the new values; 
  int ntag   = int(_instance->_tag_spin.get_value());
  int nx     = int(_instance->_x_spin.get_value());
  int ny     = int(_instance->_y_spin.get_value());
  int nz     = int(_instance->_z_spin.get_value());
  int nprior = _instance->get_priority();

  buffer = _instance->_description_entry.get_buffer();
  std::string ndesc = buffer->get_text();

  if (p && (ntag   == p->tag()      && 
	    nx     == p->x()        && 
	    ny     == p->y()        && 
	    nz     == p->z()        && 
	    nprior == p->priority()->id() && 
	    ndesc  == p->description()))    return false;
  PriorityProxy* pp = h.find_priority(nprior);
  if (!pp) return false;

  p  = new ConfigProxy(-1, ntag, nx, ny, nz, pp, -1, ndesc);
  return true;
}

//____________________________________________________________________
//
// EOF
//
