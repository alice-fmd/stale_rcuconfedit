#ifndef RCUCONFEDIT_PickDialog
#define RCUCONFEDIT_PickDialog
#include <gtkmm/dialog.h>
#include <gtkmm/tooltips.h>
#include <gtkmm/button.h>
#include <gtkmm/label.h>

namespace Gtk 
{
  class Button;
  class Label;
}

namespace RcuConfEdit 
{
  class ScrolledView;
  
  /** @class PickDialog 
      @ingroup dialog
      @ingroup editors
      @brief Base class for pick dialogs */
  class PickDialog : public Gtk::Dialog 
  {
  public:
    /** Response types */
    enum { 
      OK = 5, 
      Cancel = 6,
      Help = 11
    };

    /** Destructor  */
    virtual ~PickDialog() {}
    /** Called when the user selects an entry */ 
    virtual void on_selection() {}
    /** Called when the user press OK */
    virtual void on_ok();
    /** Called when the user press Cancel */
    virtual void on_cancel();
    /** Called when the user press Cancel */
    virtual void on_help();
    /** Get the return identifier */ 
    virtual int get_id() const { return _id; }
    /** Run the dialog */ 
    static bool run_it(const std::string& t, ScrolledView& view, int& ret);
  protected:
    /** Constructor 
	@param view The view to show */
    PickDialog();
    /** The view */ 
    ScrolledView* _view;
    /** Tool tips */
    Gtk::Tooltips _tooltips;
    /** OK button */
    Gtk::Button _ok_button;
    /** Cancel button */
    Gtk::Button _cancel_button;
    /** Help button */
    Gtk::Button _help_button;
    /** Return identifier */ 
    int _id;
    /** Static instance */ 
    static PickDialog* _instance;
  };
}

#endif
//
// EOF
//

