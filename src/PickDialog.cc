//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>

#include "config.h"
#include "PickDialog.hh"
#include "ScrolledView.hh"

#include <iostream>
#include <sstream>

//____________________________________________________________________
RcuConfEdit::PickDialog* RcuConfEdit::PickDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::PickDialog::PickDialog() 
  : _view(0),
    _ok_button(Gtk::StockID("gtk-ok")),
    _cancel_button(Gtk::StockID("gtk-cancel")),
    _help_button(Gtk::StockID("gtk-help"))
{
  // Add the frame 
  get_vbox()->set_homogeneous(false);
  get_vbox()->set_spacing(0);
  // get_vbox()->pack_start(*_view);
  
  // Set flags, etc.
  set_title("Editor...");
  set_modal(false);
  property_window_position().set_value(Gtk::WIN_POS_NONE);
  set_resizable(true);
  property_destroy_with_parent().set_value(false);
  set_has_separator(true);

  // Make buttons
  get_action_area()->property_layout_style().set_value(Gtk::BUTTONBOX_END);

  _ok_button.set_flags(Gtk::CAN_FOCUS);
  _ok_button.set_flags(Gtk::CAN_DEFAULT);
  _ok_button.set_relief(Gtk::RELIEF_NORMAL);
  _ok_button.signal_clicked()
    .connect(SigC::slot(*this, &PickDialog::on_ok), false);
  _ok_button.show();
  add_action_widget(_ok_button, -OK);
  
  _cancel_button.set_flags(Gtk::CAN_FOCUS);
  _cancel_button.set_flags(Gtk::CAN_DEFAULT);
  _cancel_button.set_relief(Gtk::RELIEF_NORMAL);
  _cancel_button.signal_clicked()
    .connect(SigC::slot(*this, &PickDialog::on_cancel), false);
  _cancel_button.show();
  add_action_widget(_cancel_button, -Cancel);
  
  _help_button.set_flags(Gtk::CAN_FOCUS);
  _help_button.set_flags(Gtk::CAN_DEFAULT);
  _help_button.set_relief(Gtk::RELIEF_NORMAL);
  _help_button.signal_clicked()
    .connect(SigC::slot(*this, &PickDialog::on_help), false);
  _help_button.show();
  add_action_widget(_help_button, -Help);

  resize(400,300);
}

//____________________________________________________________________
bool
RcuConfEdit::PickDialog::run_it(const std::string& t, 
				ScrolledView& view, int& id)
{
  if (!_instance) _instance = new PickDialog;
  std::stringstream s;
  s << "Pick a " << t << " ...";
  _instance->set_title(s.str());
  _instance->_view = &view;
  _instance->get_vbox()->pack_start(view);
  _instance->show_all();
  int response;
  do { 
    response = _instance->run();
  } while (response != -OK && response != -Cancel);
  _instance->hide_all();
  if (response == -Cancel) return false;
  
  id = _instance->_id;
  
  return true;
}


//____________________________________________________________________
void
RcuConfEdit::PickDialog::on_ok()
{
  Glib::RefPtr<Gtk::TreeSelection> ts = _view->get_selector();
  if (ts->count_selected_rows() <= 0) {
    // Whoops, nothing selected - not good - we must return cancel. 
    _id = 0;
    response(-Cancel);
    return;
  }
  Gtk::TreeModel::iterator iter = ts->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  row.get_value(0, _id);
  // std::cout << "Selected # " << _id << std::endl;
}

//____________________________________________________________________
void
RcuConfEdit::PickDialog::on_cancel()
{
  _id = 0;
}

//____________________________________________________________________
void
RcuConfEdit::PickDialog::on_help()
{
}
//____________________________________________________________________
//
// EOF
//
