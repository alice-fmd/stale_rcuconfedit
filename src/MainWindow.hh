#ifndef RCUCONFEDIT_MainWindow
#define RCUCONFEDIT_MainWindow
#include <gtkmm/window.h>
#include <gtkmm/notebook.h>
#include <gtkmm/box.h>
#include "Parameter.hh"
#include "Priority.hh"
#include "Config.hh"
#include "Address.hh"
#include "Value.hh"
#include "Sequence.hh"
#include "MenuBar.hh"
#include "ToolBar.hh"

namespace RcuDb
{
  class Server;
}

namespace Gtk
{
  class VBox;
  class MenuBar;
  class MenuItem;
  class ImageMenuItem;
  class Toolbar;
  class ToolButton;
  class Notebook;
  class ActionGroup;
  class UIManager;
}
struct _GtkNotebookPage;


namespace RcuConfEdit
{
  // Forward decls. 
  class Handler;
  class ScrolledView;

  /** @defgroup main Main window */
  
  /** @class MainWindow 
      @brief Main Window 
      @ingroup main */
  class MainWindow : public Gtk::Window
  {  
  public:
    /** Constructor */
    MainWindow();
    /** Destructor */
    virtual ~MainWindow();

    /** Called on open */
    virtual void on_open();
    /** Called on save */
    virtual void on_save();
    /** Called on close */
    virtual void on_close();
    /** Called on quit */
    virtual void on_quit();
    /** Called on copy */
    virtual void on_copy();
    /** Called on paste */
    virtual void on_paste();
    /** Called on clear */
    virtual void on_clear();
    /** Called on about */
    virtual void on_about();
    /** Called when switchin tabs 
	@param p Not used 
	@param id Id of selected tab */
    virtual void on_page(_GtkNotebookPage* p, guint id);
  protected:
    /** Add a view 
	@param view View to add */
    void add_view(ScrolledView& view);
    /** Check for changes */
    void check_changes();
    /** Get data handler 
	@return the data handler */
    Handler* get_handler();
    /** Pointer to services */
    RcuDb::Server* _server;
    /** Number of changes */
    int _changes;
    
    
    /** @{ 
	@name Note book of views */
    ParameterHandler _parameter_handler;
    PriorityHandler  _priority_handler;
    ConfigHandler    _config_handler;
    AddressHandler   _address_handler;
    ValueHandler     _value_handler;
    /** @} */

    /** @{
	@name Actions an UI */
    // Glib::RefPtr<Gtk::ActionGroup> _actions;
    // Glib::RefPtr<Gtk::UIManager>   _ui;
    /** @} */

    /** @{ 
	name Misc */
    Gtk::VBox      _vbox;
    Gtk::Notebook  _notebook;
    MenuBar        _menu_bar;
    ToolBar        _tool_bar;
    /** @} */
  };
}
#endif
