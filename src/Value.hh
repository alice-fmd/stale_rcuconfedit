// generated 2007/7/2 22:00:26 CEST by cholm@cholm.(none)
// using glademm V2.6.0
//
// newer (non customized) versions of this file go to ValueView.hh_new

// you might replace
//    class foo : public foo_glade { ... };
// by
//    typedef foo_glade foo;
// if you didn't make any modifications to the widget

#ifndef RCUCONFEDIT_Value
#define RCUCONFEDIT_Value
#include "ScrolledView.hh"
#include "EditDialog.hh"
#include "Handler.hh"
#include "Util.hh"
#include <gtkmm/label.h>
#include <gtkmm/table.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/entry.h>

// Forward declarations 
namespace Gtk 
{
  class Frame;
}

namespace RcuConf
{
  class Value;
  class SingleValue;
  class BlobValue;
  class Address;
  class Config;
  class Parameter;
}

namespace RcuConfEdit 
{
  // Forward declaration 
  class ValueView;
  class ConfigHandler;
  class ConfigProxy;
  class ParameterHandler;
  class ParameterProxy;
  class AddressHandler;
  class AddressProxy;
  class ValueHandler;
  class ValueWidget;
  class WidgetFactory;
  
  /** @defgroup value_stuff Value stuff */

  //__________________________________________________________________
  /** @class ValueProxy ValueView.h 
      @brief Structure that defines the columns in a value view. This
      is a base class for the two specialisations
      @ingroup value_stuff 
      @ingroup proxies 
  */ 
  struct ValueProxy
  {
  public:
    /** Constructor from user data 
	@param id Identifier 
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy 
	@param v Version number */
    ValueProxy(int id, ConfigProxy* c, ParameterProxy* p, AddressProxy* a,
	       int v) 
      : _id(id), _config(c), _parameter(p), _address(a), _version(v)
    {}
    /** Constructor from DB data 
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy  */
    ValueProxy(ConfigProxy* c, ParameterProxy* p, AddressProxy* a) 
      : _config(c), _parameter(p), _address(a)
    {}
    /** Destructor */
    virtual ~ValueProxy() {}
    /** @return identifier */
    virtual int id() const = 0;
    /** @return user identifier */
    int my_id() const { return _id; }
    /** @return Config proxy */
    ConfigProxy*    config()    { return _config; }
    /** @return Parameter proxy */
    ParameterProxy* parameter() { return _parameter; }
    /** @return Address proxy */
    AddressProxy*   address()   { return _address; }
    /** @return version number */
    virtual int version() const = 0;
    /** @param new (user) id */
    int set_id(int id) { _id = id; }
    /** @return DB object (possibly made from user data) */
    virtual RcuConf::Value* obj() = 0;
  protected:
    /** Identifier */
    int _id;
    ConfigProxy* _config;
    /**  Parameter proxy */
    ParameterProxy* _parameter;
    /**  Address proxy */
    AddressProxy* _address;
    /**  version number */
    int _version;
  };

  //__________________________________________________________________
  /** @class SingleValueProxy 
      @brief Proxy for single value items 
      @ingroup value_stuff 
      @ingroup proxies 
  */ 
  class SingleValueProxy : public ValueProxy
  {
  public:
    /** Constructor from user data 
	@param id Identifier 
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy 
	@param v Version number 
	@param value Value */
    SingleValueProxy(int id, ConfigProxy* c, ParameterProxy* p, 
		     AddressProxy* a, int v, int value) 
      : ValueProxy(id, c, p, a, v), _value(value), _obj(0) 
    {}
    /** Constructor from DB data 
	@param o DB object
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy  */
    SingleValueProxy(RcuConf::SingleValue* o, 
		     ConfigProxy* c, ParameterProxy* p, AddressProxy* a) 
      : ValueProxy(c,p,a), _obj(o)
    {}
    /** Destructor */
    virtual ~SingleValueProxy();
    /** @return get DB object */
    RcuConf::Value* obj();
    /** @return values */
    int value() const;
    /** @return Version number */
    int version() const;
    /** @return identifier */
    int id() const;
  protected:
    /** get DB object */
    RcuConf::SingleValue* _obj;
    /** values */
    int _value;
  };
  
  //__________________________________________________________________
  /** @class SingleValueProxy 
      @brief Proxy for blob value items 
      @ingroup value_stuff 
      @ingroup proxies 
  */ 
  class BlobValueProxy : public ValueProxy
  {
  public:
    /** Constructor from user data 
	@param id Identifier 
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy 
	@param v Version number 
	@param u Values */
    BlobValueProxy(int id, ConfigProxy* c, ParameterProxy* p, 
		   AddressProxy* a, int v, 
		   const std::vector<unsigned int>& u) 
      : ValueProxy(id, c, p, a, v), _values(u.begin(),u.end()), _obj(0)  
    {}
    /** Constructor from DB data 
	@param o DB object
	@param c Config proxy 
	@param p Parameter proxy 
	@param a Address proxy  */
    BlobValueProxy(RcuConf::BlobValue* o, 
		   ConfigProxy* c, 
		   ParameterProxy* p, 
		   AddressProxy* a);
    /** Destructor */
    virtual ~BlobValueProxy();
    /** @return get DB object */
    RcuConf::Value* obj();
    /** @return Version number */
    int version() const;
    /** @return identifier */
    int id() const;
    /** @return values */
    const std::vector<unsigned int>& values() const { return _values; }
  protected:
    /** DB object */
    RcuConf::BlobValue* _obj;
    /** values */
    std::vector<unsigned int> _values;
  };
  
  //__________________________________________________________________
  /** List of value proxies 
      @ingroup proxies */
  typedef std::vector<ValueProxy*> ValueList;
  
  //__________________________________________________________________
  /** @class ValueColumns ValueView.h 
      @brief Structure that defines the columns in a value view
      @ingroup value_stuff 
      @ingroup view  */ 
  struct ValueColumns : public ViewColumns
  {
  public:
    /** Constructor */
    ValueColumns()
    { 
      add(_config); 
      add(_parameter); 
      add(_address); 
      add(_version); 
      add(_value); 
      add(_obj);
      add(_config_obj);
      add(_parameter_obj);
      add(_address_obj);
    }
    /** Configuration column */
    Gtk::TreeModelColumn<Glib::ustring> _config;
    /** Parameter column */
    Gtk::TreeModelColumn<Glib::ustring> _parameter;
    /** Address column */
    Gtk::TreeModelColumn<Glib::ustring> _address;
    /** Version column */
    Gtk::TreeModelColumn<int> _version;
    /** Channel column */
    Gtk::TreeModelColumn<Glib::ustring>	_value;
    /** Object */
    Gtk::TreeModelColumn<ValueProxy*> _obj;
    /** Reference to config object */
    Gtk::TreeModelColumn<ConfigProxy*> _config_obj;
    /** Reference to parameter object */
    Gtk::TreeModelColumn<ParameterProxy*> _parameter_obj;
    /** Reference to address object */
    Gtk::TreeModelColumn<AddressProxy*> _address_obj;
  };

  /** @class ValueView 
      @brief View of valuees available 
      @ingroup value_stuff 
      @ingroup view */
  class ValueView : public ScrolledListView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    ValueView();
    /** Called when the selection is changed */
    virtual void on_selection();
    /** Member function to add an entry to the view 
	@param v The value to add
	@param c The configuration associated with @a v
	@param p The parameter associated with @a v
	@param a The address associated with @a v */
    virtual void add_entry(SingleValueProxy* v);
    /** Member function to add an entry to the view 
	@param v The value to add
	@param c The configuration associated with @a v
	@param p The parameter associated with @a v
	@param a The address associated with @a v */
    virtual void add_entry(BlobValueProxy* v);
    /** Member function to get currently selected item
	@param v The value to get
	@param c The configuration associated with @a v
	@param p The parameter associated with @a v
	@param a The address associated with @a v */
    virtual void get_entry(ValueProxy*&     v) const;
    /** Copy the entries from @a other into this view.  This is a
	shallow copy. 
	@param other View to topy from */
    void copy(const ValueView& other);
    /** Get all entries from the table. 
	@param l On return, contains the entries */
    void get_all(ValueList& l) const;
    /** Get all new entries from the table. 
	@param l On return, contains the new entries */
    void get_new(ValueList& l) const;
    /** Get the name */
    const char* name() const { return "Value"; }
  protected:
    /** Delete user data associated with a row 
	@param row Row to delete user data for */
    void delete_entry(Gtk::TreeModel::Row& row);
    /** Service Member function to add an entry to the view 
	@param r The added row
	@param v The value to add
	@param c The configuration associated with @a v
	@param p The parameter associated with @a v
	@param a The address associated with @a v */
    virtual void add_entry(Gtk::TreeModel::Row& r,
			   ValueProxy*      v);
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    ValueColumns _columns;
  };

  //__________________________________________________________________
  /** @brief Value edit dialog 
      @ingroup value_stuff 
      @ingroup editors */
  class ValueDialog : public EditDialog
  {  
  public:
    /** Called on browse for a config */
    void on_config();
    /** Called on browse for a parameter */
    void on_parameter();
    /** Called on browse for an address */
    void on_address();

    /** Set the config 
	@param config Config Id */
    void set_config(int config);
    /** Get the config 
	@return the config identifier */
    int get_config() const;
    /** Set the parameter 
	@param parameter Parameter Id */
    void set_parameter(int parameter);
    /** Get the parameter 
	@return the parameter identifier */
    int get_parameter() const;
    /** Set the address 
	@param address Address Id */
    void set_address(int address);
    /** Get the address 
	@return the address identifier */
    int get_address() const;
    
    /** Run when user presses OK. Checks if 
	- we have a valid config, 
	- we have a valid parameter, 
	- and data in widget is OK.
	If any of these fails, this member function will return @c
	false to flag that we should not add the new value. 
	@return boolean @e AND of the above conditions */
    bool validate();
    
    
    /** Handle broadcast */ 
    void on_broadcast();
    /** IS this broadcast */
    bool is_broadcast() const;

    /** Run this dialog with the specified widget 
	@param widget Widget to show 
	@return true in case we got new valid values */
    bool run_with_widget(ValueWidget* widget);
    
    /** Run this dialog 
	@param h Handler 
	@param v Proxy value 
	@param p Parameter proxy */
    static bool create(ValueHandler& h, ValueProxy*& v, ParameterProxy* p);
    

    /** Destructor */
    virtual ~ValueDialog() {}
  protected:
    /** Constructor */
    ValueDialog(ValueHandler& h);
    /** Static singleton instance */
    static ValueDialog* _instance;
    /** The handler */ 
    ValueHandler& _handler;

    /** @{ 
	@name config field */
    /** Label for config */
    Gtk::Label      _config_label;
    /** Entry for config */
    Gtk::Entry      _config_entry;
    /** Button for config */
    Gtk::Button     _config_button;
    /** @} */

    /** @{ 
	@name param field */
    /** Label for parameter */
    Gtk::Label      _param_label;
    /** Entry for parameter */
    Gtk::Entry      _param_entry;
    /** Button for parameter */
    Gtk::Button     _param_button;
    /** @} */

    /** @{ 
	@name address field */
    /** Label for address */
    Gtk::Label      _address_label;
    Gtk::CheckButton _broadcast_check;
    /** Entry for address */
    Gtk::Entry      _address_entry;
    /** Button for address */
    Gtk::Button     _address_button;
    /** @} */

    /** Table layout */
    Gtk::Table _table;

    /** The value widget - note, that this changes around each time we
	edit/create new parameter values */ 
    ValueWidget* _widget;
  };

  //__________________________________________________________________
  /** @class ValueHandler 
      @brief Handles Value table 
      @ingroup value_stuff 
      @ingroup handler */
  class ValueHandler : public Handler 
  {
  public:
    /** Constructor 
	@param ch Reference to config handler 
	@param ph Reference to parameter handler 
	@param ah Reference to address handler */
    ValueHandler(ConfigHandler&    ch,
		 ParameterHandler& ph, 
		 AddressHandler&   ah);
    /** Destructor */
    ~ValueHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit();
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update();
    /** Create an edit dialog 
	@param copy If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return @c true on edit */
    bool edit(bool copy);
    /** Run a dialog to pick an identifier.  This member function does
	nothing, as no other tables reference this table.  
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    bool pick(int& id) const { return true; }
    /** @} */

    /** @{ 
	@name Specific functions */ 
    /** Pick a config 
	@param id On return, the chosen identifier
	@return true if a valid object was chosen */
    bool pick_config(int& id) const;
    /** Pick a parameter 
	@param id On return, the chosen identifier
	@return true if a valid object was chosen */
    bool pick_parameter(int& id) const;
    /** Pick an address 
	@param id On return, the chosen identifier
	@return true if a valid object was chosen */
    bool pick_address(int& id) const;
    /** Find a config 
	@param id Id to look for
	@return Pointer to proxy, or null if @a id is not found */
    ConfigProxy* find_config(int id) const;
    /** Find a parameter 
	@param id Id to look for 
	@return Pointer to proxy, or null if @a id is not found */
    ParameterProxy* find_parameter(int id) const;
    /** Find an address 
	@param id Id to look for
	@return Pointer to proxy, or null if @a id is not found */
    AddressProxy* find_address(int id) const;
    /** Get the widget factory */
    WidgetFactory* factory() { return _factory; }
    /** @} */
  protected:
    /** Our view */
    ValueView _view;
    /** Config handler */
    ConfigHandler&     _config_handler;
    /** Parameter handler */
    ParameterHandler&  _parameter_handler;
    /** Address handler */
    AddressHandler&     _address_handler;
    /** Widget factory */
    WidgetFactory* _factory;
  };

}
#endif
//
// EOF
//

