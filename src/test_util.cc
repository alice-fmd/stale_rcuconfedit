#include <gtkmm/window.h>
#include <gtkmm/main.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/scrolledwindow.h>
#include <pangomm/fontdescription.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
# include <gtkmm/comboboxtext.h>
#endif
#include "Util.hh"
#include <sstream>
#include <iostream>
#include <iomanip>


class MainWindow : public Gtk::Window
{
public:
  MainWindow()
    : Gtk::Window(Gtk::WINDOW_TOPLEVEL),
      _hbox(),
      _vbox(),
      _adjustment(0,0,100),
      _spin(_adjustment),
      _hex_adjustment(0,0,0x10000,0x1,0x10),
      _hex_spin(_hex_adjustment),
      _entry(),
      _combo(),
      _view(),
      _buttons(Gtk::BUTTONBOX_START, 0),
      _up(Gtk::StockID("gtk-go-up")),
      _down(Gtk::StockID("gtk-go-down")),
      _add(Gtk::StockID("gtk-add")),
      _remove(Gtk::StockID("gtk-remove"))
  {
    _model = Gtk::ListStore::create(_columns);
    _tree.set_model(_model);
    _tree.append_column("Int",_columns._int);
    _renderer                   = Gtk::manage(new Gtk::CellRendererHex);
    _renderer->append_to_view(_tree, "Hex", _columns._hex);
    
    _combo.append_text("Foo");
    _combo.append_text("bar");
    _combo.append_text("baz");
    _combo.signal_changed().connect(SigC::slot(*this,&MainWindow::on_combo));

    _up.signal_clicked().connect(SigC::slot(*this,&MainWindow::on_up));
    _down.signal_clicked().connect(SigC::slot(*this,&MainWindow::on_down));
    _add.signal_clicked().connect(SigC::slot(*this,&MainWindow::on_add));
    _remove.signal_clicked().connect(SigC::slot(*this,
						   &MainWindow::on_remove));

    _buttons.pack_start(_add);
    _buttons.pack_start(_remove);
    _buttons.pack_start(_up);
    _buttons.pack_start(_down);
    _hbox.pack_start(_view);
    _hbox.pack_start(_buttons, Gtk::PACK_SHRINK, 0);
    _scroll.add(_tree);
    

    _vbox.pack_start(_entry, Gtk::PACK_SHRINK, 0);
    _vbox.pack_start(_spin, Gtk::PACK_SHRINK, 0);
    _vbox.pack_start(_hex_spin, Gtk::PACK_SHRINK, 0);
    _vbox.pack_start(_combo,Gtk::PACK_SHRINK, 0);
    _vbox.pack_start(_hbox);
    _vbox.pack_start(_scroll);
    add(_vbox);

    _entry.set_value(0xdeadbeaf);
    std::vector<unsigned int> v(16);
    for (size_t i = 0; i < v.size(); i++) {
      v[i] = 2 * i;
      Gtk::TreeModel::Row row = *(_model->append());
      row[_columns._int]   = i;
      row[_columns._hex]   = v[i];
    }
    _view.set_data(v);

    resize(300,400);
    show_all();
  }
  void on_up()   { _view.move_selected_up(); }
  void on_down() { _view.move_selected_down(); }
  void on_add()  { _view.insert_data_after_selected(0x0); }
  void on_remove()  { _view.remove_selected(); }
  void on_combo() { 
    std::cout << "Selected " << _combo.get_active_row_number() 
	      << ": " << _combo.get_active_text() << std::endl;
  }
protected:
  Gtk::HBox                    _hbox;
  Gtk::VBox                    _vbox;
  Gtk::HexEntry                _entry; 
  Gtk::Adjustment              _adjustment;
  Gtk::SpinButton              _spin;
  Gtk::Adjustment              _hex_adjustment;
  Gtk::HexSpinButton           _hex_spin;
  Gtk::HexView                 _view;    
  Gtk::VButtonBox              _buttons;
  Gtk::Button                  _up;      
  Gtk::Button                  _down;   
  Gtk::Button                  _add;     
  Gtk::Button                  _remove;  
  Gtk::CellRendererHex*        _renderer;
  Gtk::ScrolledWindow          _scroll;
  Gtk::TreeView                _tree;
  Glib::RefPtr<Gtk::ListStore> _model;
  Gtk::ComboBoxText            _combo;

  struct Columns : public Gtk::TreeModelColumnRecord 
  {
    Gtk::TreeModelColumn<int> _int;
    Gtk::TreeModelColumn<int> _hex;
    Columns() { add(_int); add(_hex); }
  } _columns;
};

int main(int argc, char **argv)
{  
   Gtk::Main m(argc, argv);
   MainWindow* w = new MainWindow;
   m.run(*w);
   delete w;
   return 0;
}

//
// EOF
//
