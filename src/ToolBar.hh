#ifndef RCUCONFEDIT_ToolBar
#define RCUCONFEDIT_ToolBar
#include <gtkmm/toolbar.h>
#ifndef GTKMM_2_0_0
# include <gtkmm/toolitem.h>
#else
# include <gtkmm/button.h>
#endif //

namespace RcuConfEdit
{
  // Forward declaration 
  class MainWindow;
  
  /** @class ToolBar 
      @brief Our toolbar 
      @ingroup core */
  class ToolBar : public Gtk::Toolbar 
  {
  public:
    /** Constructor 
	@param m The main window */
    ToolBar(MainWindow& m);
    /** Destructor */
    virtual ~ToolBar() {}
    /** Signal whether we're connected or not 
	@param con If @c true, we're connected */
    void connected(bool con);
    /** Signal whether we're changed 
	@param chan If @c true, we're changed */ 
    void changed(bool chan);
  protected:
#if GTKMM_MAJOR_VERSION==2 && GTKMM_MINOR_VERSION>2
    /** @{ 
	@name Tool-bar buttons */
    Gtk::ToolButton 	_open;
    Gtk::ToolButton 	_close;
    Gtk::ToolButton 	_save;
    Gtk::ToolButton 	_add;
    Gtk::ToolButton 	_copy;
    Gtk::ToolButton 	_clear;
    /** @} */
#else 
    Gtk::Button 	_open;
    Gtk::Button 	_close;
    Gtk::Button 	_save;
    Gtk::Button 	_add;
    Gtk::Button 	_copy;
    Gtk::Button 	_clear;
#endif
  };
}

#endif

    
