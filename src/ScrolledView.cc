//
// 
//
//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif

#include "config.h"
#include "ScrolledView.hh"
#include "ErrorDialog.hh"

#include <iostream>
#include <iomanip>

//____________________________________________________________________
RcuConfEdit::ScrolledView::ScrolledView() 
{
  _new_cnt = 0;
  
  // Make the view 
  _view.set_flags(Gtk::CAN_FOCUS);
  _view.set_headers_visible(true);
  _view.set_rules_hint(false);
  _view.set_reorderable(false);
  _view.set_enable_search(true);

  // Handle tree selections
  _tree_selection = _view.get_selection();
  _tree_selection->signal_changed().
    connect(SigC::slot(*this, &ScrolledView::on_selection),false);
  
  // Set flags on this object. 
  set_flags(Gtk::CAN_FOCUS);
  set_shadow_type(Gtk::SHADOW_IN);
  set_policy(Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS);
  property_window_placement().set_value(Gtk::CORNER_TOP_LEFT);

  // Add the view 
  add(_view);
  _view.show();
}
//____________________________________________________________________
void
RcuConfEdit::ScrolledView::setup(const ViewColumns&    	             columns, 
				 const Glib::RefPtr<Gtk::TreeModel>& model)
{
  // Set the model 
  _view.set_model(model);

  // Add the ID column
  _view.append_column("#", columns._id);
}

//____________________________________________________________________
void
RcuConfEdit::ScrolledView::setup_sort(const ViewColumns& columns)
{
#ifndef GTKMM_2_0_0
  // Set up sort columns 
  for (size_t i = 0; i < columns.size(); i++) {
    Gtk::TreeView::Column* column = _view.get_column(i);
    if (!column) continue;
    column->set_sort_column(i);
  }
#endif
}

//____________________________________________________________________
RcuConfEdit::ScrolledView::~ScrolledView()
{}

//____________________________________________________________________
void
RcuConfEdit::ScrolledView::on_selection()
{
#if 0
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  int id;
  row.get_value(0, id);
  std::cout << "Selected # " << id << std::endl;
#endif
}

//____________________________________________________________________
bool
RcuConfEdit::ScrolledView::find_row(int id, Gtk::TreeModel::Row& row) const
{
  const Gtk::TreeModel::Children children = _view.get_model()->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin(); 
       i != children.end(); ++i) {
    // Skip children 
    Gtk::TreeModel::Row r = *i;
    if (!(!r.parent())) continue;
    int rid;
    r.get_value(0, rid);
    if (rid == id) { 
      row = r; 
      return true; 
    }
  }
  return false;
}

//____________________________________________________________________
bool
RcuConfEdit::ScrolledView::select_row(int id) 
{
  Gtk::TreeModel::Children children = _view.get_model()->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin(); 
       i != children.end(); ++i) {
    // Skip children 
    Gtk::TreeModel::Row r = *i;
    if (!(!r.parent())) continue;
    int rid;
    r.get_value(0, rid);
    if (rid == id) { 
      _tree_selection->select(r);
      return true; 
    }
  }
  return false;
}

//____________________________________________________________________
void
RcuConfEdit::ScrolledView::clear(bool also_new)
{
  Gtk::TreeModel::Children children = _view.get_model()->children();
  std::vector<Gtk::TreeModel::Row> to_erase;
  for (Gtk::TreeModel::Children::iterator i = children.begin();
       i != children.end(); ++i) {
    Gtk::TreeModel::Row row = *i;
    if (!also_new) {      
      int rid = row[view_columns()._id];
      if (rid < 0) continue;
    }
    delete_entry(row);
    to_erase.push_back(row);
  }
  if (also_new) _new_cnt = 0;
  if (also_new) erase_all();
  else          erase_these(to_erase);
}

//====================================================================
RcuConfEdit::ScrolledTreeView::ScrolledTreeView() 
  : _tree_model(0)
{}

//____________________________________________________________________
void
RcuConfEdit::ScrolledTreeView::setup(const ViewColumns&    columns)
{
  if (_tree_model) return;
  // Create the model
  _tree_model = Gtk::TreeStore::create(columns);
  // Set the model 
  ScrolledView::setup(columns, _tree_model);
  // Initial sorting column
#ifndef GTKMM_2_0_0
  _tree_model->set_sort_column(columns._id, Gtk::SORT_ASCENDING );   
#endif
}

//____________________________________________________________________
void
RcuConfEdit::ScrolledTreeView::on_selection()
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  int id;
  row.get_value(0, id);
  std::cout << "Selected # " << id << std::endl;
  if (row.children().size() < 0) return;
  std::cout << "Children: " << std::endl;
  for (Gtk::TreeNodeChildren::const_iterator i = row.children().begin();
       i != row.children().end(); ++i) {
    (*i).get_value(0, id);
    std::cout << "\tID: " << id << std::endl;
  }
}

//____________________________________________________________________
void
RcuConfEdit::ScrolledTreeView::add_row(int id, Gtk::TreeModel::Row& row) 
{
  row = *(_tree_model->append());
  if (id < 0) {
    _new_cnt++;
    id = -_new_cnt;
  }
  row.set_value(0, id);
}

//____________________________________________________________________
bool
RcuConfEdit::ScrolledTreeView::remove_entry()
{
  if (_tree_selection->count_selected_rows() <= 0) return false;
  Gtk::TreeModel::iterator i = _tree_selection->get_selected();
  Gtk::TreeModel::Row row = *i;
  int rid;
  row.get_value(0, rid);
  if (rid > 0) {
    ErrorDialog::run_it("Bad request", 
			"Cannot remove entry already in database");
    return false;
  }
  delete_entry(row);
  _tree_model->erase(row);
  return true;
}    
//____________________________________________________________________
void
RcuConfEdit::ScrolledTreeView::erase_these(std::vector<Gtk::TreeModel::Row>& 
					   to_erase)
{
  for (size_t i = 0; i < to_erase.size(); i++) 
    _tree_model->erase(to_erase[i]);
}
//____________________________________________________________________
void
RcuConfEdit::ScrolledTreeView::erase_all()
{
  _tree_model->clear();
}

//====================================================================
RcuConfEdit::ScrolledListView::ScrolledListView() 
  : _tree_model(0)
{}

//____________________________________________________________________
void
RcuConfEdit::ScrolledListView::setup(const ViewColumns& columns, bool sort)
{						
  if (_tree_model) return;
  _tree_model = Gtk::ListStore::create(columns);
  // Set the model 
  ScrolledView::setup(columns, _tree_model);
  // Initial sorting column
#ifndef GTKMM_2_0_0
  if (sort)
    _tree_model->set_sort_column(columns._id, Gtk::SORT_ASCENDING );   
#endif
}
 
//____________________________________________________________________
void
RcuConfEdit::ScrolledListView::add_row(int id, Gtk::TreeModel::Row& row) 
{
  row = *(_tree_model->append());
  if (id < 0) {
    _new_cnt++;
    id = -_new_cnt;
  }
  row.set_value(0, id);
}

//____________________________________________________________________
bool
RcuConfEdit::ScrolledListView::remove_entry()
{
  if (_tree_selection->count_selected_rows() <= 0) return false;
  Gtk::TreeModel::iterator i = _tree_selection->get_selected();
  Gtk::TreeModel::Row row = *i;
  int rid;
  row.get_value(0, rid);
  if (rid > 0) {
    ErrorDialog::run_it("Bad request", 
			"Cannot remove entry already in database");
    return false;
  }
  delete_entry(row);
  _tree_model->erase(row);
  return true;
}    
//____________________________________________________________________
void
RcuConfEdit::ScrolledListView::erase_these(std::vector<Gtk::TreeModel::Row>& 
					   to_erase)
{
  for (size_t i = 0; i < to_erase.size(); i++) 
    _tree_model->erase(to_erase[i]);
}

//____________________________________________________________________
void
RcuConfEdit::ScrolledListView::erase_all()
{
  _tree_model->clear();
}



//____________________________________________________________________
//
// EOF
//
