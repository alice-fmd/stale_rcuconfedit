//
//
//
//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/frame.h>

#include "config.h"
#include "Value.hh"
#include "Parameter.hh"
#include "Config.hh"
#include "Address.hh"
#include "Util.hh"
#include "ErrorDialog.hh"
#include "Widget.hh"

#include <rcuconf/SingleValue.h>
#include <rcuconf/BlobValue.h>
#include <rcuconf/Config.h>
#include <rcuconf/Parameter.h>
#include <rcuconf/Address.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>

#include <sstream>
#include <iomanip>
#include <iostream>

//====================================================================
RcuConfEdit::SingleValueProxy::~SingleValueProxy()
{
  if (_obj) delete _obj;
}
//____________________________________________________________________
int
RcuConfEdit::SingleValueProxy::value() const
{
  return (_obj ? _obj->Values() : _value);
}
//____________________________________________________________________
RcuConf::Value*
RcuConfEdit::SingleValueProxy::obj()
{
  if (_obj) return _obj;
  return _obj = new RcuConf::SingleValue(_config->id(), _parameter->id(), 
					 (_address ? _address->id() : -1),
					 _value);
}
//____________________________________________________________________
int
RcuConfEdit::SingleValueProxy::id() const
{
  return (_obj ? _obj->Id() : _id);
}
//____________________________________________________________________
int
RcuConfEdit::SingleValueProxy::version() const
{
  return (_obj ? _obj->Version() : _version);
}

//====================================================================
RcuConfEdit::BlobValueProxy::BlobValueProxy(RcuConf::BlobValue* o, 
					    ConfigProxy* c, 
					    ParameterProxy* p, 
					    AddressProxy* a)
  : ValueProxy(c,p,a), _obj(o)
{
  std::vector<unsigned int> v;
  _obj->Values(v);
  _values.resize(v.size());
  std::copy(v.begin(),v.end(),_values.begin());
}
//____________________________________________________________________
RcuConfEdit::BlobValueProxy::~BlobValueProxy()
{
  if (_obj) delete _obj;
}
//____________________________________________________________________
RcuConf::Value*
RcuConfEdit::BlobValueProxy::obj()
{
  if (_obj) return _obj;
  return _obj = new RcuConf::BlobValue(_config->id(), _parameter->id(), 
				       (_address ? _address->id() : -1),
				       _values);
}
//____________________________________________________________________
int
RcuConfEdit::BlobValueProxy::id() const
{
  return (_obj ? _obj->Id() : _id);
}
//____________________________________________________________________
int
RcuConfEdit::BlobValueProxy::version() const
{
  return (_obj ? _obj->Version() : _version);
}


//====================================================================
RcuConfEdit::ValueHandler::ValueHandler(ConfigHandler&    ch,
					ParameterHandler& ph, 
					AddressHandler&   ah) 
  : _config_handler(ch),
    _parameter_handler(ph),
    _address_handler(ah)
{
  _factory = new WidgetFactory;
}

//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::commit()
{
  if (!_server) return false;

  ValueList l;
  _view.get_new(l);
  for (ValueList::iterator i = l.begin(); i != l.end(); ++i) {
    if (!(*i)->obj()->Insert(*_server)) {
      ErrorDialog::run_it("Failed to insert Value into database",
			  _server->ErrorString());
      return false;
    }
  }
  return true;    
}
//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::update()
{
  if (!_server) return false;

  RcuConf::SingleValue::List sl;
  if (!RcuConf::SingleValue::Select(sl, *_server, "")) {
    ErrorDialog::run_it("Failed to update single value view from database",
			_server->ErrorString());
    return false;
  }
  for (RcuConf::SingleValue::List::iterator i = sl.begin(); i != sl.end();++i){
    ConfigProxy*    c = _config_handler.find((*i)->ConfigId());
    ParameterProxy* p = _parameter_handler.find((*i)->ParamId());
    AddressProxy*   a = _address_handler.find((*i)->AddressId());
    _view.add_entry(new SingleValueProxy(*i, c, p, a));
  }
  RcuConf::BlobValue::List bl;
  if (!RcuConf::BlobValue::Select(bl, *_server, "")) {
    ErrorDialog::run_it("Failed to update blob value view from database",
			_server->ErrorString());
    return false;
  }
  for (RcuConf::BlobValue::List::iterator i = bl.begin(); i != bl.end(); ++i){
    ConfigProxy*    c = _config_handler.find((*i)->ConfigId());
    ParameterProxy* p = _parameter_handler.find((*i)->ParamId());
    AddressProxy*   a = _address_handler.find((*i)->AddressId());
    _view.add_entry(new BlobValueProxy(*i, c, p, a));
  }
  
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::edit(bool copy)
{
  ValueProxy* v = 0;
  if (copy) _view.get_entry(v);

  // Check whether we have a parameter, and if not, run a dialog to
  // choose our parameter - we need to know the type (at least) of the
  // parameter. 
  ParameterProxy* p = (v ? v->parameter() : 0);
  if (!p) {
    int id;
    if (!pick_parameter(id)) return false;
    p = _parameter_handler.find(id);
    // std::cout << "Found parameter " << p << " for id " << id << std::endl;
  }
  bool ret = ValueDialog::create(*this,v,p);
  
  if (!ret || !v) return false; 
  
  if (v->parameter()->blob()) 
    _view.add_entry(dynamic_cast<BlobValueProxy*>(v));
  else 
    _view.add_entry(dynamic_cast<SingleValueProxy*>(v));

  return ret;
}

//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::pick_config(int& id) const
{
  return _config_handler.pick(id);
}
//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::pick_parameter(int& id) const
{
  return _parameter_handler.pick(id);
}
//____________________________________________________________________
bool
RcuConfEdit::ValueHandler::pick_address(int& id) const
{
  return _address_handler.pick(id);
}
//____________________________________________________________________
RcuConfEdit::ConfigProxy*
RcuConfEdit::ValueHandler::find_config(int id) const
{
  return _config_handler.find(id);
}
//____________________________________________________________________
RcuConfEdit::ParameterProxy*
RcuConfEdit::ValueHandler::find_parameter(int id) const
{
  return _parameter_handler.find(id);
}
//____________________________________________________________________
RcuConfEdit::AddressProxy*
RcuConfEdit::ValueHandler::find_address(int id) const
{
  return _address_handler.find(id);
}


//====================================================================
RcuConfEdit::ValueView::ValueView()
{  
  setup(_columns,true);
  //Add the TreeView's view columns:
  _view.append_column("Config",              _columns._config);
  _view.append_column("Parameter",           _columns._parameter);
  _view.append_column("Address",             _columns._address);
  _view.append_column("Version",             _columns._version);
  _view.append_column("Value",               _columns._value);
  setup_sort(_columns);
}

//____________________________________________________________________
void
RcuConfEdit::ValueView::on_selection()
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  // ValueProxy* p = row[_columns._obj];
  // p->Print();
}

//____________________________________________________________________
void
RcuConfEdit::ValueView::add_entry(Gtk::TreeModel::Row& r, 
				  ValueProxy*      v)
{
  std::stringstream cs;
  std::string name(v->config()->description());
  cs << (name.size() > 20 ? name.substr(0,20) : name) 
     << (name.size() > 20 ? "..." : "");
  std::stringstream as;
  if (!v->address()) 
    as << "broadcast";
  else 
    as << "0x" << std::hex << std::setfill('0') 
       << std::setw(3) << v->address()->raw();
  v->set_id(r[_columns._id]);
  r[_columns._config]        = cs.str();
  r[_columns._parameter]     = v->parameter()->name();
  r[_columns._address]       = as.str();
  r[_columns._version]       = v->version();
  r[_columns._value]         = "";
  r[_columns._obj]           = v;
  r[_columns._config_obj]    = v->config();
  r[_columns._parameter_obj] = v->parameter();
  r[_columns._address_obj]   = v->address();
}

//____________________________________________________________________
void
RcuConfEdit::ValueView::add_entry(SingleValueProxy* v)
{
  if (!v) return;
  Gtk::TreeModel::Row row;
  add_row(v->id(), row);
  add_entry(row, v);
  std::stringstream s;
  s << "0x" << std::setfill('0') << std::setw(8) << std::hex << v->value();
  row[_columns._value]       = s.str();
}
//____________________________________________________________________
void
RcuConfEdit::ValueView::add_entry(BlobValueProxy*   v)
{
  if (!v) return;
  Gtk::TreeModel::Row row;
  add_row(v->id(), row);
  add_entry(row, v);
  std::vector<unsigned int> vs = v->values();
  std::stringstream s;
  s << std::setfill('0') << std::hex;
  for (size_t i = 0; i < std::min(vs.size(),4u); i++) 
    s << (i==0 ? "0x" : ",0x")  << std::setw(8) << (unsigned int)(vs[i]);
  if (vs.size() > 4) s << "...";
  row[_columns._value]       = s.str();
}

//____________________________________________________________________
void
RcuConfEdit::ValueView::get_entry(ValueProxy*& v) const
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  v = row[_columns._obj];
}  

//____________________________________________________________________
void
RcuConfEdit::ValueView::delete_entry(Gtk::TreeModel::Row& row)
{
  ValueProxy*   p  = row[_columns._obj];
  if (!p) return;
  delete p;
}
  
//____________________________________________________________________
void
RcuConfEdit::ValueView::copy(const ValueView& other)
{
  Gtk::TreeModel::Children children = other._tree_model->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin();
       i != children.end(); ++i) {
    Gtk::TreeModel::Row row     = *i;
    Gtk::TreeModel::Row new_row = *(_tree_model->append());
    Gtk::copy_cell(_columns._config,        row, new_row);
    Gtk::copy_cell(_columns._parameter,     row, new_row);
    Gtk::copy_cell(_columns._address,       row, new_row);
    Gtk::copy_cell(_columns._version,       row, new_row);
    Gtk::copy_cell(_columns._value,         row, new_row);
    Gtk::copy_cell(_columns._obj,           row, new_row);
    Gtk::copy_cell(_columns._config_obj,    row, new_row);
    Gtk::copy_cell(_columns._parameter_obj, row, new_row);
    Gtk::copy_cell(_columns._address_obj,   row, new_row);
  }
}
//____________________________________________________________________
void
RcuConfEdit::ValueView::get_all(ValueList& l) const
{
  Gtk::TreeModel::Children c = _tree_model->children();
  l.resize(c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    Gtk::TreeModel::Row r = c[i];
    ValueProxy*     p = r[_columns._obj];
    l[i]                  = p;
  }
}
//____________________________________________________________________
void
RcuConfEdit::ValueView::get_new(ValueList& l) const
{
  l.clear();
  Gtk::TreeModel::Children c = _tree_model->children();
  size_t j = 0;
  for (size_t i = 0; i < c.size(); i++) { 
    Gtk::TreeModel::Row r = c[i];
    int id = r[_columns._id];
    if (id >= 0) continue;
    ValueProxy* p = r[_columns._obj];
    l.push_back(p);
  }
}

    

//====================================================================
RcuConfEdit::ValueDialog* RcuConfEdit::ValueDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::ValueDialog::ValueDialog(ValueHandler& h)
  : _handler(h),
    _config_label("Configuration"),
    _config_button("Select..."),
    _param_label("Parameter"),
    _param_button("Select..."),
    _broadcast_check("Broadcast"),
    _address_label("Address"),
    _address_button("Select..."),
    _table(2, 2, false), 
    _widget(0)
{
  // Tag input 
  
  _config_label.set_alignment(0.5,0.5);
  _config_label.set_padding(0,0);
  _config_label.set_justify(Gtk::JUSTIFY_LEFT);
  _config_label.set_line_wrap(false);
  _config_label.set_use_markup(false);
  _config_label.set_selectable(false);
  _config_entry.set_flags(Gtk::CAN_FOCUS);
  _config_entry.set_visibility(true);
  _config_entry.set_editable(false);
  _config_entry.set_max_length(0);
  _config_entry.set_text("");
  _config_entry.set_has_frame(true);
  _config_entry.set_activates_default(false);
  _config_button.set_flags(Gtk::CAN_FOCUS);
  _config_button.set_relief(Gtk::RELIEF_NORMAL);
  _config_button.signal_clicked()
    .connect(SigC::slot(*this, &ValueDialog::on_config), false);
  _tooltips.set_tip(_config_button, 
		    "Select configuration from known configurations", 
		    "");

  _param_label.set_alignment(0.5,0.5);
  _param_label.set_padding(0,0);
  _param_label.set_justify(Gtk::JUSTIFY_LEFT);
  _param_label.set_line_wrap(false);
  _param_label.set_use_markup(false);
  _param_label.set_selectable(false);
  _param_entry.set_flags(Gtk::CAN_FOCUS);
  _param_entry.set_visibility(true);
  _param_entry.set_editable(false);
  _param_entry.set_max_length(0);
  _param_entry.set_text("");
  _param_entry.set_has_frame(true);
  _param_entry.set_activates_default(false);
  _param_button.set_flags(Gtk::CAN_FOCUS);
  _param_button.set_relief(Gtk::RELIEF_NORMAL);
  _param_button.set_sensitive(false); //<== Disable param select!
  _param_button.signal_clicked()
    .connect(SigC::slot(*this, &ValueDialog::on_parameter), false);
  _tooltips.set_tip(_param_button,"Select parameter from known parameters", "");

  _address_label.set_alignment(0.5,0.5);
  _address_label.set_padding(0,0);
  _address_label.set_justify(Gtk::JUSTIFY_LEFT);
  _address_label.set_line_wrap(false);
  _address_label.set_use_markup(false);
  _address_label.set_selectable(false);
  _address_entry.set_flags(Gtk::CAN_FOCUS);
  _address_entry.set_visibility(true);
  _address_entry.set_editable(false);
  _address_entry.set_max_length(0);
  _address_entry.set_text("");
  _address_entry.set_has_frame(true);
  _address_entry.set_activates_default(false);
  _address_button.set_flags(Gtk::CAN_FOCUS);
  _address_button.set_relief(Gtk::RELIEF_NORMAL);
  _address_button.signal_clicked()
    .connect(SigC::slot(*this, &ValueDialog::on_address), false);
  _tooltips.set_tip(_address_button, "Select address from known addresses", "");

  _broadcast_check.signal_toggled()
    .connect(SigC::slot(*this, &ValueDialog::on_broadcast));
  _tooltips.set_tip(_broadcast_check, 
		    "Broad cast this value to all possible targets");
  
  _table.set_row_spacings(0);
  _table.set_col_spacings(3);
  Gtk::AttachOptions a = Gtk::AttachOptions();
  _table.attach(_config_label,    0, 1, 0, 1, Gtk::FILL, a, 3);
  _table.attach(_config_entry,    1, 3, 0, 1, Gtk::EXPAND|Gtk::FILL);
  _table.attach(_config_button,   3, 4, 0, 1, Gtk::FILL, a, 3);
  
  _table.attach(_param_label,     0, 1, 1, 2, Gtk::FILL, a, 3);
  _table.attach(_param_entry,     1, 3, 1, 2, Gtk::EXPAND|Gtk::FILL);
  _table.attach(_param_button,    3, 4, 1, 2, Gtk::FILL, a, 3);

  _table.attach(_address_label,   0, 1, 2, 3, Gtk::FILL, a, 3);
  _table.attach(_broadcast_check, 1, 2, 2, 3, Gtk::FILL);
  _table.attach(_address_entry,   2, 3, 2, 3, Gtk::EXPAND|Gtk::FILL);
  _table.attach(_address_button,  3, 4, 2, 3, Gtk::FILL, a, 3);

  _inner_vbox.pack_start(_table,  Gtk::PACK_SHRINK, 3);
}

//____________________________________________________________________
void
RcuConfEdit::ValueDialog::on_broadcast()
{
  _address_button.set_sensitive(!_broadcast_check.get_active());
}

//____________________________________________________________________
bool
RcuConfEdit::ValueDialog::is_broadcast() const
{
  return _broadcast_check.get_active();
}


//____________________________________________________________________
bool
RcuConfEdit::ValueDialog::validate()
{
  bool has_config    = get_config() != 0;
  bool has_parameter = get_parameter() != 0;
  bool data_ok       = (_widget ? _widget->validate() : false);
  if (!has_config || !has_parameter || !data_ok) {
    std::stringstream s;
    s << "Invalid " 
      << (!has_config ? "configuration" : "")
      << (!has_parameter ? (has_config ? "" : ", ") : "")
      << (!has_parameter ? (data_ok && !has_parameter ? "& " : "") : "")
      << (!has_parameter ? "parameter" : "")
      << (!data_ok ? (has_config && has_parameter ? "" : ", ") : "")
      << (!data_ok ? (!has_parameter ? " & " : "") : "")
      << (!data_ok ? "data" : "");
    ErrorDialog::run_it("Bad parameters", s.str());
  }
  return (has_config && has_parameter && data_ok);
}

//____________________________________________________________________
void
RcuConfEdit::ValueDialog::set_config(int id)
{
  std::stringstream s; s << id; _config_entry.set_text(s.str());
}
//____________________________________________________________________
int
RcuConfEdit::ValueDialog::get_config() const
{
  std::stringstream s(_config_entry.get_text()); int id; s >> id;  return id;
}
//____________________________________________________________________
void
RcuConfEdit::ValueDialog::set_parameter(int id)
{
  std::stringstream s; s << id; _param_entry.set_text(s.str());
}
//____________________________________________________________________
int
RcuConfEdit::ValueDialog::get_parameter() const
{
  std::stringstream s(_param_entry.get_text()); int id; s >> id;  return id;
}
//____________________________________________________________________
void
RcuConfEdit::ValueDialog::set_address(int id)
{
  std::stringstream s; s << id; _address_entry.set_text(s.str());
  _broadcast_check.set_active(id == 0);
}
//____________________________________________________________________
int
RcuConfEdit::ValueDialog::get_address() const
{
  if (is_broadcast()) return 0;
  std::stringstream s(_address_entry.get_text()); int id; s >> id;  return id;
}
//____________________________________________________________________
void
RcuConfEdit::ValueDialog::on_config()
{
  int ret;
  if (_handler.pick_config(ret)) set_config(ret);
}
//____________________________________________________________________
void
RcuConfEdit::ValueDialog::on_parameter()
{
  int ret;
  if (_handler.pick_parameter(ret)) set_parameter(ret);
}
//____________________________________________________________________
void
RcuConfEdit::ValueDialog::on_address()
{
  int ret;
  if (_handler.pick_address(ret)) set_address(ret);
}

//____________________________________________________________________
bool
RcuConfEdit::ValueDialog::run_with_widget(ValueWidget* w)
{
  if (!w) return false;
  if (_widget) _inner_vbox.remove(*_widget);
  _widget = w;
  _inner_vbox.pack_start(*_widget,Gtk::PACK_EXPAND_WIDGET, 3);
  return (run_it() == OK);
}


  
//____________________________________________________________________
bool
RcuConfEdit::ValueDialog::create(ValueHandler& h, ValueProxy*& v,
				 ParameterProxy* p)
{
  if (!_instance) _instance = new ValueDialog(h);
  // Get some pointers 
  ConfigProxy*  c = (v ? v->config() : 0);
  AddressProxy* a = (v ? v->address() : 0);
  
  
  // Set the basic fields 
  std::stringstream vs; vs << "version " << (v ? v->version() : 0);
  _instance->set_top("Single Value", (v ? v->id() : 0), vs.str());
  _instance->set_config(c ? c->id() : 0);
  _instance->set_parameter(p ? p->id() : 0);
  _instance->set_address(a ? a->id() : 0);

  // Get the appropriate widget.
  ValueWidget*      widget = h.factory()->create(p);
  if (!p) { 
    std::cerr << "No widget created for parameter " << p 
	      << " " << (p ? p->name() : "") << std::endl;
    return false;
  }
  BlobValueProxy*   bv     = dynamic_cast<BlobValueProxy*>(v);
  SingleValueProxy* sv     = dynamic_cast<SingleValueProxy*>(v);
  if (p->blob()) {
    widget->clear();
    if (v) widget->set_multiple_value(bv->values());
  }
  else {
    widget->clear();
    if (v) widget->set_single_value(sv->value());
  }
  
  // Now run 
  if (!_instance->run_with_widget(widget)) {
    std::cout << "Failed in run!" << std::endl;
    return false;
  }
  
  // OK, time to check for changes. 
  int  nconfig  = _instance->get_config();
  int  nparam   = _instance->get_parameter();
  int  naddress = _instance->get_address();
  bool sconfig  = (c ? c->id() == nconfig  : true);
  bool sparam   = (p ? p->id() == nparam   : true);
  bool saddress = (a ? a->id() == naddress : true);
  bool svalue   = true;
  std::vector<unsigned int> nvalues;
  unsigned int              nvalue;
  
  if (!p->blob()) {
    // Check the single value 
    nvalue = widget->get_single_value();
    svalue = (v ? sv->value() == nvalue : false);
  }
  else {
    // Check the multiple value. 
    widget->get_multiple_value(nvalues);
    if (!v) svalue = false;
    else {
      const std::vector<unsigned int> values = bv->values();
      if (values.size() != nvalues.size()) svalue = false;
      else {
	for (size_t i = 0; i < values.size(); i++) {
	  if (values[i] != nvalues[i]) {
	    svalue = false;
	    break;
	  }
	}
      }
    }
  }

  // Now, we can check if there's any difference
  if (sconfig && sparam && saddress && svalue) {
    ErrorDialog::run_it("Duplicate entry", 
			"An entry with the exact same values already "
			"exists in the table");
    return false;
  }
  
  
  if (!(c = h.find_config(nconfig))) {
    std::stringstream s;
    s << "The configuation with id <i>" << nconfig 
      << "</i> could not be found";
    ErrorDialog::run_it("Invalid parameter", s.str());
    return false;
  }
  
  if (!(p = h.find_parameter(nparam))) {
    std::stringstream s;
    s << "The parameter with id <i>" << nparam 
      << "</i> could not be found";
    ErrorDialog::run_it("Invalid parameter", s.str());
    return false;
  }
  a = h.find_address(naddress);
  
  // Now, we can create our return objects. 
  if (p->blob()) 
    v = new BlobValueProxy(-1,c,p,a,-1,nvalues);
  else 
    v = new SingleValueProxy(-1,c,p,a,-1,nvalue);
  return true;
}

//____________________________________________________________________
//
// EOF
//
