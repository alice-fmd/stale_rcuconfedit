//
//
//
//#include <libgnome/libgnome.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gdk/gdkkeysyms.h>
#include <gtkmm/accelgroup.h>
#include <gtkmm/frame.h>
#include <gtkmm/treeview.h>

#include "config.h"
#include "Priority.hh"
#include "Parameter.hh"
#include "PickDialog.hh"
#include "ErrorDialog.hh"

#include <rcuconf/Priority.h>
#include <rcuconf/Parameter.h>
#include <rcudb/Server.h>
#include <rcudb/Sql.h>


#include <sstream>
#include <iomanip>
#include <iostream>

//====================================================================
RcuConfEdit::PriorityProxy::~PriorityProxy()
{
  if (_obj) delete _obj; 
}
//____________________________________________________________________
int
RcuConfEdit::PriorityProxy::id() const 
{
  return _obj ? _obj->Id() : _id;
}
//____________________________________________________________________
const std::string&
RcuConfEdit::PriorityProxy::description() const 
{
  return _obj ? _obj->Description() : _description;
}

//____________________________________________________________________
RcuConf::Priority*
RcuConfEdit::PriorityProxy::obj()
{
  if (_obj) return _obj;
  std::vector<int> pp(_params.size());
  for (size_t i = 0; i < _params.size(); i++) 
    pp[i] = _params[i]->id();
  return _obj = new RcuConf::Priority(_description, pp);
}

//====================================================================
bool
RcuConfEdit::PriorityHandler::commit()
{
  if (!_server) return false;

  PriorityList l;
  _view.get_new(l);
  for (PriorityList::iterator i = l.begin(); i != l.end(); ++i) {
    if (!(*i)->obj()->Insert(*_server)) {
      ErrorDialog::run_it("Failed to insert Priority into database",
                          _server->ErrorString());
      return false;
    }
  }
  return true;    
}
//____________________________________________________________________
bool
RcuConfEdit::PriorityHandler::update()
{
  if (!_server) return false;

  RcuConf::Priority::List l;
  if (!RcuConf::Priority::Select(l, *_server, "")) {
    ErrorDialog::run_it("Failed to update priority lists", 
			_server->ErrorString());
    return false;
  }
  
  for (RcuConf::Priority::List::iterator i = l.begin(); i != l.end(); ++i) {
    std::vector<int> ps;
    (*i)->Params(ps);
    ParameterList pp(ps.size());
    for (size_t j = 0; j < ps.size(); j++) {
      ParameterProxy* ip = _parameter_handler.find(ps[j]);
      if (!ip) {
	std::stringstream s;
	s << "Priority list refers to invalid parameter " << ps[j];
	ErrorDialog::run_it("Invalid value", s.str());
	return false;
      }
      pp[j] = ip;
    }
    _view.add_entry(new PriorityProxy(*i,pp));
  }
  return true;
}

//____________________________________________________________________
bool
RcuConfEdit::PriorityHandler::edit(bool copy)
{
  PriorityProxy* p = 0;
  if (copy) _view.get_entry(p);

  bool ret = PriorityDialog::create(*this, p); 
  if (!ret || !p) return ret;
  
  // Here, we should get the parameters and store pointer in pp 
  _view.add_entry(p);
  return ret;
}

//____________________________________________________________________
RcuConfEdit::PriorityProxy*
RcuConfEdit::PriorityHandler::find(int priority) const
{
  PriorityProxy* p = 0;
  _view.find_entry(priority, p);
  return p;
}

//____________________________________________________________________
bool
RcuConfEdit::PriorityHandler::pick(int& ret) const
{
  PriorityView v; 
  v.copy(_view);
  return PickDialog::run_it("Priority", v, ret);
}

//____________________________________________________________________
bool
RcuConfEdit::PriorityHandler::pick_parameter(ParameterProxy*& p) const
{
  int ret;
  if (!_parameter_handler.pick(ret)) return false;
  p = _parameter_handler.find(ret);
  return p != 0;
}


//====================================================================
RcuConfEdit::PriorityView::PriorityView() 
{  
  setup(_columns);
  _view.append_column("Description", _columns._name);
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::add_entry(PriorityProxy* p)
{
  if (!p) return;
  Gtk::TreeModel::Row row;
  add_row(p->id(), row);
  p->set_id(row[_columns._id]);
  row[_columns._name]        = p->description();
  row[_columns._obj]         = p;
  row[_columns._sub_obj]     = 0;
  const ParameterList& pp    = p->params();
  for (ParameterList::const_iterator i = pp.begin(); i != pp.end(); ++i) {
    Gtk::TreeModel::Row childrow = *(_tree_model->append(row.children()));
    childrow[_columns._id]      = (*i)->id();
    childrow[_columns._name]    = (*i)->name();
    childrow[_columns._obj]     = 0;
    childrow[_columns._sub_obj] = (*i);
  }
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::get_entry(PriorityProxy*& p) const
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  if (!(!row.parent())) return;
  p = row[_columns._obj];
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::delete_entry(Gtk::TreeModel::Row& row)
{
  PriorityProxy* p = row[_columns._obj];
  if (!p) return;
  delete p;
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::find_entry(int id, PriorityProxy*& p) const
{
  Gtk::TreeModel::Row row;
  p = 0;
  if (find_row(id, row)) p = row[_columns._obj];
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::on_selection()
{
  if ( _tree_selection->count_selected_rows() <= 0) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  const Gtk::TreeModel::Row& row = *iter;
  if (row[_columns._sub_obj]) {
    _tree_selection->select(row.parent());
    return;
  }
  // PriorityProxy* p = row[_columns._obj];
  // p->Print();
}

//____________________________________________________________________
void
RcuConfEdit::PriorityView::copy(const PriorityView& other) 
{
  Gtk::TreeModel::Children children = other._tree_model->children();
  for (Gtk::TreeModel::Children::iterator i = children.begin();
       i != children.end(); ++i) {
    Gtk::TreeModel::Row row     = *i;
    Gtk::TreeModel::Row new_row = *(_tree_model->append());
    Gtk::copy_cell(_columns._id,       row, new_row);
    Gtk::copy_cell(_columns._name,     row, new_row);
    Gtk::copy_cell(_columns._obj,      row, new_row);
    Gtk::copy_cell(_columns._sub_obj,  row, new_row);

    Gtk::TreeModel::Children sub_children = row.children();
    for (size_t j = 0; j < sub_children.size(); j++) {
      row                         = sub_children[j];
      Gtk::TreeModel::Row sub_row = *(_tree_model->append(new_row.children()));
      Gtk::copy_cell(_columns._id,       row, sub_row);
      Gtk::copy_cell(_columns._name,     row, sub_row);
      Gtk::copy_cell(_columns._obj,      row, sub_row);
      Gtk::copy_cell(_columns._sub_obj,  row, sub_row);
    }
  }
}
//____________________________________________________________________
void
RcuConfEdit::PriorityView::get_all(PriorityList& l) const
{
  Gtk::TreeModel::Children c = _tree_model->children();
  l.resize(c.size());
  for (size_t i = 0; i < c.size(); ++i) {
    Gtk::TreeModel::Row r = c[i];
    PriorityProxy* p      = r[_columns._obj];
    l[i]                  = p;
  }
}
  
//____________________________________________________________________
void
RcuConfEdit::PriorityView::get_new(PriorityList& l) const
{
  l.clear();
  Gtk::TreeModel::Children c = _tree_model->children();
  size_t j = 0;
  for (size_t i = 0; i < c.size(); i++) { 
    Gtk::TreeModel::Row r = c[i];
    int id = r[_columns._id];
    if (id >= 0) continue;
    PriorityProxy* p = r[_columns._obj];
    l.push_back(p);
  }
}


//====================================================================
RcuConfEdit::ParameterTable::ParameterTable(PriorityDialog& p) 
  : _parent(p)
{  
  setup(_columns, false);
  _view.append_column("Name",  _columns._name);
}

//____________________________________________________________________
void
RcuConfEdit::ParameterTable::add_entry(ParameterProxy* p)
{
  if (!p) return;
  Gtk::TreeModel::Row row    = *(_tree_model->append());
  row[_columns._id]          = p->id();
  row[_columns._name]        = p->name();
  row[_columns._obj]         = p;
  //std::cout << "Added parameter " << p->Name() << std::endl;
  
}

//____________________________________________________________________
void
RcuConfEdit::ParameterTable::set_entries(const ParameterList& pp)
{
  _tree_model->clear();
  for (ParameterList::const_iterator i = pp.begin(); i != pp.end(); ++i) 
    add_entry(*i);
  //std::cout << "Added " << pp.size() << " rows " << std::endl;
}

//____________________________________________________________________
void
RcuConfEdit::ParameterTable::get_entries(ParameterList& pp) const
{
  Gtk::TreeModel::Children children = _tree_model->children();
  pp.resize(children.size());
  for (size_t i = 0; i < children.size(); i++) {
    Gtk::TreeModel::Row row = children[i];
    pp[i] = row[_columns._obj];
  }
}
  
//____________________________________________________________________
void
RcuConfEdit::ParameterTable::on_selection()
{
#if 0
  if (_tree_selection->count_selected_rows() > 0) {
    Gtk::TreeModel::Row row = *(_tree_selection->get_selected());
    RcuConf::Parameter* p = row[_columns._obj];
    p->Print();
  }
#endif
  _parent.on_select(_tree_selection->count_selected_rows() > 0);
}

//____________________________________________________________________
void
RcuConfEdit::ParameterTable::insert_entry(ParameterProxy* p) 
{
  if (!p) return;
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) {
    add_entry(p);
    return;
  }
#ifndef GTKMM_2_0_0
  Gtk::TreeModel::Row row    = *(_tree_model->insert_after(iter));
#else
  Gtk::TreeModel::iterator keep = iter;
  iter++;
  if (iter == _tree_model->children().end()) iter = keep;
  Gtk::TreeModel::Row row    = *(_tree_model->insert(iter));
#endif
  row[_columns._id]          = p->id();
  row[_columns._name]        = p->name();
  row[_columns._obj]         = p;
}
//____________________________________________________________________
void
RcuConfEdit::ParameterTable::remove_selected() 
{
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) return;
  _tree_model->erase(iter);
}

//__________________________________________________________________
void 
RcuConfEdit::ParameterTable::move_up()
{
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter)  return;
  Gtk::TreeModel::Children::iterator top = _tree_model->children().begin();
  if (iter == top)  return;
#ifndef GTKMM_2_0_0
  Gtk::TreeModel::iterator up   = iter;
  up--;
#else
  Gtk::TreeModel::iterator up   = _tree_model->children().begin();
  Gtk::TreeModel::iterator keep = up;
  while ((keep != iter && keep != _tree_model->children().end())) {
    up = keep;
    keep++;
  }
#endif
  _tree_model->move(iter,up);
}  

//__________________________________________________________________
void 
RcuConfEdit::ParameterTable::move_down()
{
  Gtk::TreeModel::iterator iter = _tree_selection->get_selected();
  if (!iter) return;
#ifndef GTKMM_2_0_0
  Gtk::TreeModel::Children::iterator bottom=_tree_model->children().end();
  bottom--;
#else
  Gtk::TreeModel::iterator bottom   = _tree_model->children().begin();
  Gtk::TreeModel::iterator keep     = bottom;
  while ((keep != _tree_model->children().end())) {
    bottom = keep;
    keep++;
  }
#endif
  if (iter.equal(bottom)) return;
  Gtk::TreeModel::iterator down   = iter++;
  _tree_model->move(iter,down);
}  


//====================================================================
RcuConfEdit::PriorityDialog* RcuConfEdit::PriorityDialog::_instance = 0;

//____________________________________________________________________
RcuConfEdit::PriorityDialog::PriorityDialog(PriorityHandler& h)
  : _handler(h), 
    _add_button(Gtk::StockID("gtk-add")),
    _remove_button(Gtk::StockID("gtk-remove")),
    _up_button(Gtk::StockID("gtk-go-up")),
    _down_button(Gtk::StockID("gtk-go-down")),
    _param_buttons(Gtk::BUTTONBOX_START, 0),
    _param_list(*this),
    _param_hbox(false, 0)
{
  _description_view.set_flags(Gtk::CAN_FOCUS);
  _description_view.set_editable(true);
  _description_view.set_cursor_visible(true);
  _description_view.set_pixels_above_lines(0);
  _description_view.set_pixels_below_lines(0);
  _description_view.set_pixels_inside_wrap(0);
  _description_view.set_left_margin(0);
  _description_view.set_right_margin(0);
  _description_view.set_indent(0);
  _description_view.set_wrap_mode(Gtk::WRAP_NONE);
  _description_view.set_justification(Gtk::JUSTIFY_LEFT);
  _description_scroll.set_flags(Gtk::CAN_FOCUS);
  _description_scroll.set_shadow_type(Gtk::SHADOW_IN);
  _description_scroll.set_policy(Gtk::POLICY_ALWAYS, Gtk::POLICY_ALWAYS);
  _description_scroll.property_window_placement()
    .set_value(Gtk::CORNER_TOP_LEFT);
  _description_scroll.add(_description_view);
  _description_frame  = make_frame("Description", _description_scroll);

  _add_button.set_flags(Gtk::CAN_FOCUS);
  _add_button.set_flags(Gtk::CAN_DEFAULT);
  _add_button.set_relief(Gtk::RELIEF_NORMAL);
  _add_button.signal_clicked()
    .connect(SigC::slot(*this, &PriorityDialog::on_add), false);
  _remove_button.set_flags(Gtk::CAN_FOCUS);
  _remove_button.set_flags(Gtk::CAN_DEFAULT);
  _remove_button.set_relief(Gtk::RELIEF_NORMAL);
  _remove_button.signal_clicked()
    .connect(SigC::slot(*this, &PriorityDialog::on_remove), false);
  _up_button.set_flags(Gtk::CAN_FOCUS);
  _up_button.set_flags(Gtk::CAN_DEFAULT);
  _up_button.set_relief(Gtk::RELIEF_NORMAL);
  _up_button.signal_clicked()
    .connect(SigC::slot(*this, &PriorityDialog::on_up), false);
  _down_button.set_flags(Gtk::CAN_FOCUS);
  _down_button.set_flags(Gtk::CAN_DEFAULT);
  _down_button.set_relief(Gtk::RELIEF_NORMAL);
  _down_button.signal_clicked()
    .connect(SigC::slot(*this, &PriorityDialog::on_down), false);
  _param_buttons.pack_start(_add_button);
  _param_buttons.pack_start(_remove_button);
  _param_buttons.pack_start(_up_button);
  _param_buttons.pack_start(_down_button);
  _param_hbox.pack_start(_param_list);
  _param_hbox.pack_start(_param_buttons, Gtk::PACK_SHRINK, 0);
  _param_frame   = make_frame("Parameters", _param_hbox);

  _paned.pack1(*_description_frame, Gtk::SHRINK);
  _paned.pack2(*_param_frame);
  _inner_vbox.pack_start(_paned);
  resize(300,400);
}

  
//____________________________________________________________________
void
RcuConfEdit::PriorityDialog::on_add()
{
  ParameterProxy* p = 0;
  if (_handler.pick_parameter(p)) 
    _param_list.insert_entry(p);
}
//____________________________________________________________________
void
RcuConfEdit::PriorityDialog::on_remove()
{
  _param_list.remove_selected();
}
//____________________________________________________________________
void
RcuConfEdit::PriorityDialog::on_up()
{
  _param_list.move_up();
}
//____________________________________________________________________
void
RcuConfEdit::PriorityDialog::on_down()
{
  _param_list.move_down();
}
//____________________________________________________________________
void
RcuConfEdit::PriorityDialog::on_select(bool ok)
{
  if (!ok) {
    _add_button.set_sensitive(true);
    _up_button.set_sensitive(false);
    _down_button.set_sensitive(false);
    _remove_button.set_sensitive(false);
    return;
  }
  _up_button.set_sensitive(true);
  _down_button.set_sensitive(true);
  _remove_button.set_sensitive(true);
  Gtk::TreeModel::iterator s = _param_list._tree_selection->get_selected();
  Gtk::TreeModel::iterator t = _param_list._tree_model->children().begin();
#ifndef GTKMM_2_0_0
  Gtk::TreeModel::iterator e = _param_list._tree_model->children().end();
  e--;
#else
  Gtk::TreeModel::iterator e    = _param_list._tree_model->children().begin();
  Gtk::TreeModel::iterator keep = e;
  while ((keep != _param_list._tree_model->children().end())) {
    e = keep;
    keep++;
  }
#endif
  _up_button.set_sensitive(s != t);
  _down_button.set_sensitive(s != e);
}

//____________________________________________________________________
bool
RcuConfEdit::PriorityDialog::create(PriorityHandler& h, 
				    PriorityProxy*& p)
{
  if (!_instance) _instance = new PriorityDialog(h);

  Glib::RefPtr<Gtk::TextBuffer> buffer = Gtk::TextBuffer::create();
  buffer->set_text(p ? p->description() : "");
  _instance->set_top("Priority", (p ? p->id() : 0), "");
  _instance->_description_view.set_buffer(buffer);
  _instance->_param_list.clear(true);
  if (p) _instance->_param_list.set_entries(p->params());
  
  // Now show the dialog 
  if (_instance->run_it() != OK) return false;

  // Now, get the new description
  buffer = _instance->_description_view.get_buffer();
  std::string ndesc = buffer->get_text();

  // ... and the parameters - note, order is important. 
  ParameterList npp;
  _instance->_param_list.get_entries(npp);
  
  bool differ = false;
  if (p) {
    const ParameterList& pp = p->params();
    if (pp.size() != npp.size()) differ = true;
    else {
      for (size_t i = 0; i < npp.size(); i++) {
	if (pp[i]->id() == npp[i]->id()) continue;
	differ = true;
	break;
      }
    }
  }
  if (p && ndesc == p->description() && !differ) return true;

  p = new PriorityProxy(-1, ndesc, npp);
  return true;
}
//____________________________________________________________________
//
// EOF
//
