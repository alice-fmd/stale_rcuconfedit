//
//
//
#ifndef RCUCONFEDIT_Address
#define RCUCONFEDIT_Address
#include "EditDialog.hh"
#include "ScrolledView.hh"
#include "Handler.hh"
#include "Util.hh"
#include <gtkmm/label.h>
#include <gtkmm/table.h>

// Forward declaration 
namespace RcuConf
{
  class Address;
}

namespace RcuConfEdit
{
  /** @defgroup proxies Proxies for database table entries. 

      Classes in this group are proxies for the entries retrieved or
      to be stored in the database tables.  They may, or may not,
      contain a pointer to the real data retrieved. These classes are
      used to ensure that, on commit, the proper references is set
      between the rows.  It also makes some of the interfaces slightly
      more generic. */

  /** @defgroup address_stuff Addresses */
  class AddressView;
  class AddressHandler;
  //__________________________________________________________________
  /** @class AddressProxy
      @brief proxy for DB address entries 
      @ingroup address_stuff 
      @ingroup proxies */
  class AddressProxy 
  {
  public:
    /** constructor from user data 
	@param id Identifier 
	@param board Board number 
	@param chip Chip number 
	@param channel Channel number */
    AddressProxy(int id, unsigned int board, unsigned int chip, 
		 unsigned int channel)
      : _id(id),
	_raw(((board & 0x1f)<<7)|((chip & 0x7)<<4)|(channel & 0xf)), 
	_obj(0)
    {}
    /** Constructor from DB data 
	@param obj Database data */
    AddressProxy(RcuConf::Address* obj) : _obj(obj) {}
    /** Destructor */
    virtual ~AddressProxy();
    /** @return the id */
    int id() const;
    /** @return the id */
    int my_id() const { return _id; }
    /** @return board number */
    unsigned int board() const;
    /** @return chip number */
    unsigned int chip() const;
    /** @return channel number */
    unsigned int channel() const;
    /** @return Full address */
    unsigned int raw() const;
    /** @param id New (user) id */
    void set_id(int id) { _id = id; }
    /** Create and get database object */
    RcuConf::Address* obj();
  protected:
    /** (user) id */
    int _id;
    /** (user) full address */
    unsigned int _raw;
    /** database object */
    RcuConf::Address* _obj;
  };
  //__________________________________________________________________
  /** Typedef of address proxy list 
      @ingroup address_stuff
      @ingroup proxies */
  typedef std::vector<AddressProxy*> AddressList;
    
  //__________________________________________________________________
  /** @class AddressColumns AddressView.h 
      @brief Structure that defines the columns in a address view
      @ingroup address_stuff 
      @ingroup view
  */ 
  struct AddressColumns : public ViewColumns
  {
  public:
    /** Constructor */
    AddressColumns()
    { 
      add(_board); 
      add(_chip); 
      add(_channel); 
      add(_obj);
    }
    /** Board column */
    Gtk::TreeModelColumn<unsigned int>	_board;
    /** Chip column */
    Gtk::TreeModelColumn<unsigned int>	_chip;
    /** Channel column */
    Gtk::TreeModelColumn<unsigned int>	_channel;
    /** Pointer to the object */ 
    Gtk::TreeModelColumn<AddressProxy*> _obj;
  };

  //__________________________________________________________________
  /** @class AddressView 
      @brief View of addresses available 
      @ingroup view
      @ingroup address_stuff */
  class AddressView : public ScrolledListView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    AddressView();
    /** Called when the selection is changed */
    void on_selection();
    /** Member function to add an entry to the view 
	@param p Pointer to the entry */
    void add_entry(AddressProxy* p);
    /** Member function to get the selected entry from the view 
	@param p Pointer to the entry */
    void get_entry(AddressProxy*& p) const;
    /** Find an entry that has ID field @a id, and return a pointer to
	the DB object in @a p 
	@param id Identifier to look for 
	@param  p On exit a pointer to the DB object, or null if the
	identifier wasn't found in the list. */
    void find_entry(int id, AddressProxy*& p) const;
    /** Copy the entries from @a other into this view.  This is a
	shallow copy. 
	@param other View to topy from */
    void copy(const AddressView& other);
    /** Get all entries from the table. 
	@param l On return, contains the entries */
    void get_all(AddressList& l) const;
    /** Get all new entries from the table. 
	@param l On return, contains the new entries */
    void get_new(AddressList& l) const;
    /** Get the name */ 
    virtual const char* name() const { return "Address"; }
  protected:
    /** Delete user data associated with a row 
	@param row Row to delete user data for */
    void delete_entry(Gtk::TreeModel::Row& row);
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    AddressColumns _columns;
  };

  //__________________________________________________________________
  /** @class AddressDialog
      @brief Edit address information 
      @ingroup editors
      @ingroup address_stuff */
  class AddressDialog : public EditDialog
  {  
  public:
    /** Member function to pop-up a dialog, and then remove it when
	the user hits OK or cancel 
	@param h Reference to handler (used by pop-ups) 
	@param p DB object to edit.  On return a possible new object
	@return @c true if the values where changed and the user
	pressed OK, @c false otherwise */
    static bool create(AddressHandler& h, AddressProxy*& p);
    /** Validate the input */
    bool validate();
    /** Destructor */
    virtual ~AddressDialog() {}
  protected:
    /** Constructor */
    AddressDialog(AddressHandler& h);
    /** Handler */
    AddressHandler& _handler;
    /** Table */
    Gtk::Table _table;
    
    /** Board label */
    Gtk::Label _board_label;
    /** Board spin */
    Gtk::HexSpinButton _board_entry;
    /** Chip label */
    Gtk::Label _chip_label;
    /** Chip spin */
    Gtk::HexSpinButton _chip_entry;
    /** Channel label */
    Gtk::Label _channel_label;
    /** Channel spin */
    Gtk::HexSpinButton _channel_entry;

    /** Singleton */
    static AddressDialog* _instance;
  };

  //__________________________________________________________________
  /** @class AddressHandler
      @brief Handler of address stuff 
      @ingroup handler
      @ingroup address_stuff */
  class AddressHandler : public Handler 
  {
  public:
    /** Constructor */
    AddressHandler() { }
    /** Destructor */
    ~AddressHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit();
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update();
    /** Create an edit dialog 
	@param copy If false, edit currently selected object, if any.
	Note, a copy will be made and inserted into the view 
	@return @c true on edit */
    bool edit(bool copyx);
    /** Run a dialog to pick an identifier 
	@param id On return, the chosen identifier 
	@return true if an object was selected */ 
    bool pick(int& id) const;
    /** @} */

    /** @{ 
	@name Specific member functions */
    /** Find a address object 
	@param id Id of the object */ 
    virtual AddressProxy* find(int id) const;
    /** @} */
  protected:
    /** View */
    AddressView _view;
  };

}
#endif
//
// EOF
//
