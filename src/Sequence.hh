//
//
#ifndef RCUCONFEDIT_Sequence
#define RCUCONFEDIT_Sequence
#include "ScrolledView.hh"
#include "EditDialog.hh"
#include "Handler.hh"

namespace RcuConfEdit
{
  /** @defgroup sequence_stuff Sequence stuff */

  //__________________________________________________________________
  /** @class SequenceColumns 
      @brief Structure that defines the columns in a sequence view
      @ingroup sequence_stuff 
      @ingroup view
  */ 
  typedef ViewColumns SequenceColumns;

  //__________________________________________________________________
  /** @class SequenceView 
      @brief View of sequencees available 
      @ingroup view
      @ingroup sequence_stuff */
  class SequenceView : public ScrolledListView
  {  
  public:
    /** Constructor 
	@param gmm_data Data */
    SequenceView();
    /** Member function to add an entry to the view 
	@param id The id  */
    virtual void set_entry(int id);
    /** Get the name */
    virtual const char* name() const { return "Sequence"; }
  protected:
    /** Get the column descriptor 
	@return The column descriptor */
    const ViewColumns& view_columns() const { return _columns; }
    /** Columns in this view */
    SequenceColumns _columns;
    /** The only row */
    Gtk::TreeModel::Row _row;
  };

  //__________________________________________________________________
  /** @class SequenceHandler 
      @brief Handles Sequence table 
      @ingroup handler 
      @ingroup sequence_stuff */
  class SequenceHandler : public Handler 
  {
  public:
    /** Constructor */
    SequenceHandler()  {}
    /** Destructor */
    ~SequenceHandler() {};
    
    /** @{ 
	@name Implementation of handler functions */
    /** Create a scrolled view for this table 
	@return Newly created scroll view */
    ScrolledView& get_view() { return _view; }
    /** Commit new values to the database 
	@return true on success, false otherwise */
    bool commit() { return true; }
    /** Update a view from the data base 
	@return true on success, false otherwise */
    bool update() { return true; }
    /** Create an edit dialog.  Note, that the sequence is strictly
	read-only for this client, so we do nothing here. 
	@param copy Ignored
	@return Always @c true */
    bool edit(bool copy) { return true; }
    /** Run a dialog to pick an identifier.  This does not make sense
	for the sequence, so this does absolute nothing. 
	@param id Ignored. 
	@return always @c true */ 
    bool pick(int& id) const { return true; }
    /** @} */
  protected:
    /** Our view */
    SequenceView _view;
  };

}
#endif
//
// EOF
//


