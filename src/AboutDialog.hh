//
// 
//
#ifndef RCUCONFEDIT_AboutDialog
#define RCUCONFEDIT_AboutDialog
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
#include <gtkmm/aboutdialog.h>

namespace RcuConfEdit
{
  //__________________________________________________________________
  /** @class AboutDialog 
      @brief About dialog 
      @ingroup dialog */
  class AboutDialog : public Gtk::AboutDialog
  {  
  public:
    /** Destructor */
    ~AboutDialog();
    /** Show it */
    static void run_it();
  protected:
    /** Constructor */
    AboutDialog();
    /** Singleton */
    static AboutDialog* _instance;
  };
};
#endif
#endif
//
// EOF
//
