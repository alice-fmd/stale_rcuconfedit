//#include "Value.hh"
#include <gtkmm/main.h>
#include <gtkmmconfig.h>
#ifndef GTKMM_2_0_0
# include <sigc++/compatibility.h>
#endif
#include <gtkmm/frame.h>
#include <gtkmm/buttonbox.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include "Widget.hh"
#include "Parameter.hh"
#include <iostream>
#include <sstream>
#include <iomanip>

struct TestWidget : public Gtk::Window
{
  TestWidget(RcuConfEdit::ValueWidget* w)
    : Gtk::Window(Gtk::WINDOW_TOPLEVEL), 
      _vbox(),
      _frame("Widget"),
      _buttons(),
      _ok(Gtk::StockID("gtk-ok")),
      _cancel(Gtk::StockID("gtk-cancel")), 
      _widget(w)
  {    
    _buttons.pack_start(_cancel);
    _buttons.pack_start(_ok);
    _vbox.pack_start(_frame, Gtk::PACK_EXPAND_WIDGET,10);
    _vbox.pack_end(_buttons, Gtk::PACK_SHRINK,10);
    _hbox.pack_start(_vbox,  Gtk::PACK_EXPAND_WIDGET,10);
    add(_hbox);

    _frame.add(*_widget);
    _widget->show_all();
    set_title(_widget->name());

    _ok.show();
    _cancel.show();
    _frame.show();
    _buttons.show();
    _hbox.show();
    _vbox.show();
    show();

    set_modal(false);
    property_window_position().set_value(Gtk::WIN_POS_NONE);
    set_resizable(true);
    property_destroy_with_parent().set_value(false);

    _ok.signal_clicked().connect(SigC::slot(*this,&TestWidget::on_ok));
    _cancel.signal_clicked().connect(SigC::slot(*this,
						   &TestWidget::on_cancel));
  }
  void on_cancel() 
  {
    hide_all();
  }
  void on_ok() 
  {
    if (_widget->is_single()) {
      unsigned int sv = _widget->get_single_value();
      std::cout << "Single value: 0x" << std::hex << std::setfill('0')
		<< std::setw(8) << sv << std::dec << std::setfill(' ')
		<< std::endl;
    }
    else {
      std::vector<unsigned int> mv;
      unsigned int sv;
      _widget->get_multiple_value(mv);
      std::cout << "Multiple values:\n" << std::endl;
      for (size_t i = 0; i < mv.size(); i++) 
	std::cout << "\t" << std::setw(3) << i << ": 0x" << std::hex 
		  << std::setfill('0') << std::setw(8) << mv[i] 
		  << std::dec << std::setfill(' ')
		  << std::endl;
    }
    hide_all();
  }
  Gtk::HBox   _hbox;
  Gtk::VBox   _vbox;
  Gtk::Frame  _frame;
  Gtk::Button _ok;
  Gtk::Button _cancel;
  Gtk::HButtonBox _buttons;
  RcuConfEdit::ValueWidget*  _widget;
};

int 
main(int argc, char** argv) 
{
  std::string        name;
  std::string        value;
  bool               blob;
  RcuConfEdit::Where where = RcuConfEdit::Rcu;
  for (size_t i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      switch (argv[i][1]) {
      case 'h': 
	std::cout << "Usage: " << argv[0] << " [OPTIONS] NAME [VALUE]\n\n" 
		  << "Options:\n"
		  << "\t-b\tBlob valued\n"
		  << "\t-s\tSingle valued\n"
		  << "\t-r\tRCU register\n"
		  << "\t-f\tBC register\n"
		  << "\t-a\tALTRO register" 
		  << std::endl;
	return 0;
      case 'b': blob  = true;               break;
      case 's': blob  = false;              break;
      case 'r': where = RcuConfEdit::Rcu;   break;
      case 'f': where = RcuConfEdit::Bc;    break;
      case 'a': where = RcuConfEdit::Altro; break;
      }
    }
    else if (name.empty()) 
      name = argv[i];
    else 
      value = argv[i];
  }
  if (name.empty()) {
    std::cerr << "No name specified" << std::endl;
    return 1;
  }
  Gtk::Main m(argc, argv);
  
  RcuConfEdit::ParameterProxy  p(-1, name, where, blob, 0xFFFFFFFF);
  RcuConfEdit::WidgetFactory   f;  
  RcuConfEdit::ValueWidget*    w = f.create(&p);
  if (!w) {
    std::cerr << "No widget made" << std::endl;
    return 1;
  }
  
  std::stringstream s(value);
  if (!blob) {
    unsigned int sv;
    s >> std::hex >> sv;
    w->set_single_value(sv);
  }
  else {
    std::vector<unsigned int> mv;
    unsigned int sv;
    while (!s.eof()) {
      s >> std::hex >> sv;
      if (s.fail()) break;
      mv.push_back(sv);
    }
    w->set_multiple_value(mv);
  }

  TestWidget t(w);
  if (blob) t.resize(400,300);
  m.run(t);
    
  return 0;
}


  
  
  
    
      

